﻿#pragma once

#include "../math/collision/box.h"

namespace app {

	namespace area {

		//---------------------------------------------------------------------------------
		/**
		 * @brief
		 * モートン順序
		 */
		class MortonOrder {
		public:
			//---------------------------------------------------------------------------------
			/**
			 * @brief	コンストラクタ
			 */
			MortonOrder() = default;

			//---------------------------------------------------------------------------------
			/**
			 * @brief	デストラクタ
			 */
			~MortonOrder() = default;

		public:
			//---------------------------------------------------------------------------------
			/**
			 * @brief	モートン順序の初期設定
			 * @param	start		開始座標
			 * @param	size		サイズ
			 * @param	hierarchy	階層数
			 * @return
			 */
			static void SetUp(const vec4& start, const vec4& size, int hierarchy);

			//---------------------------------------------------------------------------------
			/**
			 * @brief	モートン番号を取得する
			 * @param	point		ポイント
			 * @return	対応する最下層モートン番号
			 */
			static int MortonNumber(const vec4& point);

			//---------------------------------------------------------------------------------
			/**
			 * @brief	モートン番号を取得する
			 * @param	aabb		境界ボリューム（AABB）
			 * @return	階層と階層におけるモートン番号のペア
			 */
			static std::pair<int, int> MortonNumber(const core::math::collision::AABB& aabb);
		};
	}
}
