﻿#include "sceneTexture3d.h"

#include "../dx11/dx11.h"
#include "../buffer/buffer.h"

namespace app {

	namespace scene {

		//---------------------------------------------------------------------------------
		/**
		 * @brief	シーン初期化
		 * @return
		 */
		void SceneTexture3d::Initialize() {
			m_cube.Initialize();
			m_material.SetData(nullptr);
			m_material.SetShader("resource/hlsl/vertexTexture3d.hlsl", m_cube.GetVertexBuffer()->GetFormat());

			m_cube.SetMaterial(&m_material);
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	シーン更新
		 * @return
		 */
		void SceneTexture3d::Update() {

		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	シーン終了
		 * @return
		 */
		void SceneTexture3d::Finalize() {
		}
	}
}
