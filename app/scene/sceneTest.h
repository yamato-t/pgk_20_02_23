﻿#pragma once

#include "sceneBase.h"

namespace app {

	namespace scene{

		//---------------------------------------------------------------------------------
		/**
		 * @brief
		 * シーン
		 */
		class SceneTest : public SceneBase {
		public:
			//---------------------------------------------------------------------------------
			/**
			 * @brief	コンストラクタ
			 */
			SceneTest() = default;

			//---------------------------------------------------------------------------------
			/**
			 * @brief	デストラクタ
			 */
			~SceneTest() = default;

		public:
			//---------------------------------------------------------------------------------
			/**
			 * @brief	シーン初期化
			 * @return
			 */
			virtual void Initialize() override final;

			//---------------------------------------------------------------------------------
			/**
			 * @brief	シーン更新
			 * @return
			 */
			virtual void Update() override final;

			//---------------------------------------------------------------------------------
			/**
			 * @brief	シーン終了
			 * @return
			 */
			virtual void Finalize() override final;
		};
	}
}
