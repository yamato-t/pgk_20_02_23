﻿#pragma once

#include "singleton.h"
#include "sceneBase.h"

namespace app {

	namespace scene{

		//---------------------------------------------------------------------------------
		/**
		 * @brief
		 * シーン管理
		 */
		class SceneManager : public core::util::Singleton<SceneManager> {
		private:
			friend class core::util::Singleton<SceneManager>;

		private:
			class cImpl;
			std::shared_ptr<cImpl> m_pImpl;	///< インプリメントクラスポインタ

		private:
			//---------------------------------------------------------------------------------
			/**
			 * @brief	コンストラクタ
			 */
			SceneManager();

			//---------------------------------------------------------------------------------
			/**
			 * @brief	デストラクタ
			 */
			virtual ~SceneManager();

		public:

			//---------------------------------------------------------------------------------
			/**
			 * @brief	シーンを更新する
			 */
			void Update();

			//---------------------------------------------------------------------------------
			/**
			 * @brief	シーンを追加する
			 */
			void Add(std::shared_ptr<SceneBase> scene);

			//---------------------------------------------------------------------------------
			/**
			 * @brief	シーンを切り替える
			 */
			void Change(std::weak_ptr<SceneBase> scene);

			//---------------------------------------------------------------------------------
			/**
			 * @brief	新規シーンを追加して切り替える
			 */
			template<class T>
			void ChangeNewScene()
			{
				auto scene = std::make_shared<T>();
				Add(scene);
				Change(scene);
			}
		};
	}
}
