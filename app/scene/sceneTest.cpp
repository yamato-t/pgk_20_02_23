﻿#include "sceneTest.h"

#include "callback.h"

namespace app {

	namespace scene {

		//---------------------------------------------------------------------------------
		template<typename Result, typename ...Arg>
		using Function = std::function<Result(Arg...)>;

		//---------------------------------------------------------------------------------
		bool Function1(int i) {
			return false;
		}
		//---------------------------------------------------------------------------------
		bool Function2(int i, int ii, std::string str) {
			return true;
		}

		//---------------------------------------------------------------------------------
		Function<bool, int>	Function3 = [](int i) {
			return true;
		};

		//---------------------------------------------------------------------------------
		Function<bool, int, std::string> Function4 = [](bool i, std::string str) {
			return true;
		};

		//---------------------------------------------------------------------------------
		core::util::CallBack<bool, int, std::string> Function5 = Function4;

		//---------------------------------------------------------------------------------
		core::util::CallBack<bool, int, std::string> FunctionCallback = [](int i, std::string str) {
			return true;
		};

		//---------------------------------------------------------------------------------
		/**
		 * @brief
		 */
		class Test
		{
		public:
			Test()
			{
				trace(u8"Test()");
			}

			Test(int i) {
				trace(u8"Test(int i)");
				m_value = i;
			}

			Test(const Test& r)
			{
				trace(u8"Test(const Test& r)");
			}

			Test(Test&& r)
			{
				trace(u8"Test(Test&& r)");
			}

			Test& operator=(const Test& r)
			{
				trace(u8"operator=(const Test& r)");
				return *this;
			}

			Test& operator=(Test&& r)
			{
				trace(u8"operator=(Test&& r)");
				return *this;
			}

			~Test()
			{
				trace(u8"  ~Test value [ %d ]", m_value);
			}


		private:
			int m_value = 0;
		};

		//---------------------------------------------------------------------------------
		/**
		 * @brief	シーン初期化
		 * @return
		 */
		void SceneTest::Initialize() {
			//---------------------------------------------------------------------
			Function<bool, int>	func1;
			func1 = std::bind(&Function1, std::placeholders::_1);
			auto b1 = func1(1);

			Function<bool, int, int, std::string> func2;
			func2 = std::bind(&Function2, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3);
			auto b2 = func2(1, 2, "aaa");

			auto b3 = Function5(2, "bbb");

			//---------------------------------------------------------------------
			Test t1 = Test(1);
			Test t2 = Test(2);

			Test t3;
			t3 = t2;
			t3 = Test(3);
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	シーン更新
		 * @return
		 */
		void SceneTest::Update() {

		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	シーン終了
		 * @return
		 */
		void SceneTest::Finalize() {

		}

	}
}
