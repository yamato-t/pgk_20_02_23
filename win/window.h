#pragma once

#include "singleton.h"

/** @def
 * ウィンドウのサイズ
 */
#define WINDOW_WIDTH	(1280) 

/** @def
 * ウィンドウのサイズ
 */
#define WINDOW_HEIGHT	(720) 

/** @def
 * アプリケーション（ウィンドウ）名
 */
#define APP_NAME		("system")

namespace core {

	namespace windows {

		//---------------------------------------------------------------------------------
		/**
		 * @brief	
		 * ウィンドウ制御クラス
		 *
		 * シングルトンによるウィンドウ制御クラス
		 */
		class Window : public util::Singleton<Window> {
		private:
			friend class util::Singleton<Window>;

		private:
			class cImpl;
			std::shared_ptr<cImpl> m_pImpl;	///< インプリメントクラスポインタ

		private:
			//---------------------------------------------------------------------------------
			/**
			 * @brief	コンストラクタ
			 */
			Window();

			//---------------------------------------------------------------------------------
			/**
			 * @brief	デストラクタ
			 */
			~Window();

		public:
			//---------------------------------------------------------------------------------
			/**
			 * @brief	ウィンドウプロシージャ
			 */
			LRESULT		MsgProc(HWND hWnd, UINT iMsg, WPARAM wParam, LPARAM lParam);

			//---------------------------------------------------------------------------------
			/**
			 * @brief	ウィンドウスレッドの生成
			 * @param	hInstance	インスタンスハンドル
			 * @return	生成の成否
			 */
			HRESULT		Create(HINSTANCE hInstance);

			//---------------------------------------------------------------------------------
			/**
			 * @brief	スレッドワーカー
			 * @param	pInstance	インスタンスハンドルのポインタ
			 * @return	
			 */
			void		Worker(HINSTANCE* pInstance);

			//---------------------------------------------------------------------------------
			/**
			 * @brief	ウィンドウ生成終了までの処理待機
			 * @return	
			 */
			void		Wait();

			//---------------------------------------------------------------------------------
			/**
			 * @brief	ウィンドウハンドルの取得
			 * @return	ウィンドウハンドル
			 */
			const HWND&	Handle();

			//---------------------------------------------------------------------------------
			/**
			 * @brief	ウィンドウが終了しているか否かの取得
			 * @return	終了していればtrue
			 */
			bool		IsEnd();

		};
	}
}