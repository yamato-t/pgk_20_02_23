﻿#pragma once

#include "../material/material.h"
#include "../texture/texture.h"
#include "../render/obj3dRender.h"

namespace core {

	namespace material {

		//---------------------------------------------------------------------------------
		/**
		 * @brief
		 * マテリアル生成情報
		 */
		struct CreateMeshMaterialData {
			std::string			texturePath[2];		///< テクスチャパス
			int					textureCount;		///< テクスチャ数

			vec4				ambient;			///< アンビエント
			vec4				diffuse;			///< ディフューズ
			vec4				specular;			///< スペキュラー
			std::string			name;				///< マテリアル名
		};

		//---------------------------------------------------------------------------------
		/**
		 * @brief
		 * マテリアル情報クラス
		 */
		class MeshMaterial : public Material {
		public:
			vec4					m_ambient;				///< アンビエント
			vec4					m_diffuse;				///< ディフューズ
			vec4					m_specular;				///< スペキュラー

			char					m_name[64];				///< マテリアル名

			texture::Texture*		m_pTextures;			///< テクスチャ
			int						m_textureCount;			///< テクスチャ数

			MeshMaterial(){}
			virtual ~MeshMaterial(){}

			//---------------------------------------------------------------------------------
			/**
			 * @brief	マテリアルデータの設定
			 * @param	pData		元となるデータのポインタ
			 * @return
			 */
			virtual void SetData(void* pData) override {
				auto p = (CreateMeshMaterialData*)(pData);

				m_pTextures = new texture::Texture[p->textureCount];
				for(int i = 0; i < p->textureCount; i++) {
					m_pTextures[i].Create(p->texturePath[i]);
				}

				m_textureCount = p->textureCount;

				sscanf_s(p->name.c_str(), "%s", m_name, (unsigned)_countof(m_name));
				m_ambient  = p->ambient;
				m_diffuse  = p->diffuse;
				m_specular = p->specular;
			}

			//---------------------------------------------------------------------------------
			/**
			 * @brief	マテリアルのコマンド設定
			 * @param	pContext	描画コンテキスト
			 * @return
			 */
			virtual void SetCommand(ID3D11DeviceContext* pContext) override {

				// 描画設定
				auto& state = render::Obj3DRender::Instance().GetRenderState();
				state.SetBlendState(pContext, GetBlendState());
				state.SetDepthStencil(pContext, GetDepthStencil());

				// シェーダー設定
				pContext->IASetInputLayout(m_shader.VertexLayout());
				pContext->VSSetShader(m_shader.VertexShader(), nullptr, 0);
				pContext->PSSetShader(m_shader.PixelShader(), nullptr, 0);

				if(m_pTextures) {

					// テクスチャ設定
					auto views = new IShaderResourceView*[m_textureCount];

					for (int i = 0; i < m_textureCount; i++) {
						views[i] = m_pTextures[i].GetTextureView();
					}
					pContext->PSSetShaderResources(0, m_textureCount, views);

					delete views;
				}
			}
		};
	}
};
