﻿#pragma once

#include "../material/material.h"
#include "../texture/texture3d.h"
#include "../render/obj3dRender.h"

namespace core {

	namespace material {

		//---------------------------------------------------------------------------------
		/**
		 * @brief
		 * マテリアル情報クラス
		 */
		class Texture3dMaterial : public Material {
		public:
			texture::Texture3d		m_texture3d;			///< テクスチャ

			Texture3dMaterial() = default;
			virtual ~Texture3dMaterial() = default;

			//---------------------------------------------------------------------------------
			/**
			 * @brief	マテリアルデータの設定
			 * @param	pData		元となるデータのポインタ
			 * @return
			*/
			virtual void Texture3dMaterial::SetData(void* pData) override {
				m_texture3d.Create(
					"resource/texture/texture3d.raw",
					256, 256, 256,
					DXGI_FORMAT_R8_UNORM,
					D3D11_BIND_SHADER_RESOURCE
				);
			}

			//---------------------------------------------------------------------------------
			/**
			 * @brief	マテリアルのコマンド設定
			 * @param	pContext	描画コンテキスト
			 * @return
			*/
			virtual void Texture3dMaterial::SetCommand(ID3D11DeviceContext* pContext) override {

				// シェーダー設定
				pContext->IASetInputLayout(m_shader.VertexLayout());
				pContext->VSSetShader(m_shader.VertexShader(), nullptr, 0);
				pContext->PSSetShader(m_shader.PixelShader(), nullptr, 0);

				// テクスチャ設定
				IShaderResourceView*	pRes[1] = { m_texture3d.GetTextureView() };
				pContext->PSSetShaderResources(0, 1, pRes);
			}
		};
	}
};
