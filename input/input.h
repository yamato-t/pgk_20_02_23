#pragma once

#include "singleton.h"

namespace core {

	namespace input {
		//---------------------------------------------------------------------------------
		/**
		 * @brief	
		 * 入力情報の管理
		 */
		class Input : public util::Singleton<Input> {
		private:
			friend class util::Singleton<Input>;

		private:
			class cImpl;
			std::shared_ptr<cImpl> m_pImpl;		///< インプリメントクラスポインタ

		private:
			//---------------------------------------------------------------------------------
			/**
			 * @brief	コンストラクタ
			 */
			Input();

			//---------------------------------------------------------------------------------
			/**
			 * @brief	デストラクタ
			 */
			~Input();

		public:
			//---------------------------------------------------------------------------------
			/**
			 * @brief	キー情報の取得
			 * @param	key		キーの識別子
			 * @return	入力されていればtrue
			 */
			bool		GetKey(uint16_t key);

			//---------------------------------------------------------------------------------
			/**
			 * @brief	キー情報の更新
			 * @param	pState		ハードから設定されたキー情報配列のポインタ
			 * @return	
			 */
			void		UpdateKeyState(void* pState);
		};
	}
}