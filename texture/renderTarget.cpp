﻿#include "renderTarget.h"
#include "../dx11/dx11.h"
#include "texture.h"

#include <process.h>
#include <mutex>


namespace core {

	namespace texture {

		using namespace dx11;

		//---------------------------------------------------------------------------------
		/**
		 * @brief
		 * レンダーターゲット制御インプリメントクラス
		 */
		class RenderTarget::cImpl {
		public:
			Texture								m_texture;			///< レンダーターゲットのテクスチャ
			ComPtr<IRenderTargetView>			m_pRenderTarget;	///< レンダーターゲットのビュー

		public:
			//---------------------------------------------------------------------------------
			/**
			 * @brief	コンストラクタ
			 */
			cImpl() {
			}

			//---------------------------------------------------------------------------------
			/**
			 * @brief	コンストラクタ
			 * @param	width		横幅
			 * @param	height		縦幅
			 * @param	format		テクスチャフォーマット
			 */
			cImpl(float width, float height, DXGI_FORMAT format) {
				Create(width, height, format);
			}

			//---------------------------------------------------------------------------------
			/**
			 * @brief	デストラクタ
			 */
			~cImpl() {
			}


			//---------------------------------------------------------------------------------
			/**
			 * @brief	レンダーターゲット作成
			 * @param	width		横幅
			 * @param	height		縦幅
			 * @param	format		テクスチャフォーマット
			 * @return
			 */
			void Create(float width, float height, DXGI_FORMAT format) {

				m_texture.Create(width, height, format);

				D3D11_RENDER_TARGET_VIEW_DESC xRenderTargetDesc;
				memset(&xRenderTargetDesc, 0, sizeof(xRenderTargetDesc));
				xRenderTargetDesc.Format				= format;
				xRenderTargetDesc.ViewDimension			= D3D11_RTV_DIMENSION_TEXTURE2D;
				xRenderTargetDesc.Texture2D.MipSlice	= 0;
				if (FAILED(dx11Device::Instance().Device()->CreateRenderTargetView(m_texture.Get(), &xRenderTargetDesc, m_pRenderTarget.GetPointerPointer()))) {
					assert(false, "レンダーターゲットの作成に失敗しました");
				}
			}

		};

		//---------------------------------------------------------------------------------
		/**
		 * @brief	コンストラクタ
		 */
		RenderTarget::RenderTarget() {
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	コンストラクタ
		 * @param	width		横幅
		 * @param	height		縦幅
		 * @param	format		テクスチャフォーマット
		 */
		RenderTarget::RenderTarget(float width, float height, DXGI_FORMAT format) {
			m_pImpl.reset(new RenderTarget::cImpl(width, height, format));
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	デストラクタ
		 */
		RenderTarget::~RenderTarget() {

		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	レンダーターゲット作成
		 * @param	width		横幅
		 * @param	height		縦幅
		 * @param	format		テクスチャフォーマット
		 * @return
		 */
		void RenderTarget::Create(float width, float height, DXGI_FORMAT format) {
			m_pImpl.reset(new RenderTarget::cImpl());
			m_pImpl->Create(width, height, format);
		}


		//---------------------------------------------------------------------------------
		/**
		 * @brief	レンダーターゲットが有効か否か
		 * @return	有効な場合true
		 */
		bool RenderTarget::IsValid() {
			return (m_pImpl != nullptr);
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	2Dバッファ取得
		 * @return	レンダーターゲットのテクスチャ
		 */
		Texture* RenderTarget::GetTexture() {
			return &m_pImpl->m_texture;
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	ビューの取得
		 * @return	レンダーターゲットのビュー
		 */
		IRenderTargetView* RenderTarget::GetRenderTargetView() {
			return m_pImpl->m_pRenderTarget.GetPointer();
		}
	}
}

