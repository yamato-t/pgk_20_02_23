﻿#include "texture3d.h"
#include "../dx11/dx11.h"

namespace core {
	namespace texture {

		using namespace dx11;

		//---------------------------------------------------------------------------------
		/**
		 * @brief
		 * テクスチャ制御インプリメントクラス
		 */
		class Texture3d::cImpl {
		public:
			ComPtr<ITexture3D>					m_pTexture;			///< テクスチャ
			ComPtr<IShaderResourceView>			m_pTextureView;		///< テクスチャのビュー（シェーダリソースビュー）

		public:
			//---------------------------------------------------------------------------------
			/**
			 * @brief	コンストラクタ
			 */
			cImpl() {
			}


			//---------------------------------------------------------------------------------
			/**
			 * @brief	テクスチャ生成
			 * @param	filePath	ファイルパス
			 * @param	width		横幅
			 * @param	height		縦幅
			 * @param	depth		深度
			 * @param	format		テクスチャフォーマット
			 * @param	bindFlags	バインドフラグ
			 * @return
			 */
			cImpl(const std::string& filePath, uint32_t width, uint32_t height, uint32_t depth, DXGI_FORMAT format, uint32_t bindFlags) {

				auto formatSize = format == DXGI_FORMAT_R8_UNORM ? 1 : 4;
				auto voxelNum	= width * height * depth;
				auto size		= voxelNum * formatSize;
				byte* buffer	= new byte[size];

				FILE* fp = nullptr;
				fopen_s(&fp, filePath.c_str(), "rb");
				fread_s(buffer, size, formatSize, voxelNum, fp);
				fclose(fp);


				D3D11_TEXTURE3D_DESC textureDesc;
				memset(&textureDesc, 0, sizeof(textureDesc));
				textureDesc.Format			= format;
				textureDesc.Usage			= D3D11_USAGE_IMMUTABLE;// D3D11_USAGE_DEFAULT;
				textureDesc.BindFlags		= bindFlags;
				textureDesc.Width			= width;
				textureDesc.Height			= height;
				textureDesc.Depth			= depth;
				textureDesc.CPUAccessFlags	= 0;
				textureDesc.MipLevels		= 1;

				D3D11_SUBRESOURCE_DATA initData;
				ZeroMemory(&initData, sizeof(initData));
				initData.pSysMem			= buffer;
				initData.SysMemPitch		= width;
				initData.SysMemSlicePitch	= height * depth;

				if (FAILED(dx11Device::Instance().Device()->CreateTexture3D(&textureDesc, &initData, m_pTexture.GetPointerPointer()))) {
					assert(false, "テクスチャの作成に失敗しました");
				}

				CreateView();

				// アセット読み込みバッファはもういらない
				delete buffer;
			}

			//---------------------------------------------------------------------------------
			/**
			 * @brief	デストラクタ
			 */
			~cImpl() {
			}

		private:
			//---------------------------------------------------------------------------------
			/**
			 * @brief	テクスチャのビューを作成
			 */
			void CreateView() {

				if (FAILED(dx11Device::Instance().Device()->CreateShaderResourceView(m_pTexture.GetPointer(), nullptr, m_pTextureView.GetPointerPointer()))) {
					assert(false, " シェーダリソースビューの作成に失敗しました");
				}
			}

		};

		//---------------------------------------------------------------------------------
		/**
		 * @brief	コンストラクタ
		 */
		Texture3d::Texture3d() {
			m_pImpl.reset(new Texture3d::cImpl());
		}


		//---------------------------------------------------------------------------------
		/**
		 * @brief	コンストラクタ
		 * @param	filePath	ファイルパス
		 * @param	width		横幅
		 * @param	height		縦幅
		 * @param	depth		深度
		 * @param	format		テクスチャフォーマット
		 * @param	bindFlags	バインドフラグ
		 * @return
		 */
		void Texture3d::Create(const std::string& filePath, uint32_t width, uint32_t height, uint32_t depth, DXGI_FORMAT format, uint32_t bindFlags) {
			m_pImpl.reset(new Texture3d::cImpl(filePath, width, height, depth, format, bindFlags));
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	デストラクタ
		 */
		Texture3d::~Texture3d() {

		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	テクスチャ取得
		 * @return	テクスチャ
		 */
		ITexture3D* Texture3d::Get() {
			return m_pImpl->m_pTexture.GetPointer();
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	テクスチャ取得
		 * @return	テクスチャのビュー
		 */
		IShaderResourceView* Texture3d::GetTextureView() {
			return m_pImpl->m_pTextureView.GetPointer();
		}
	}
}

