#pragma once

#include "../dx11/dx11def.h"
#include "texture.h"

namespace core {

	namespace texture {

		//---------------------------------------------------------------------------------
		/**
		 * @brief	
		 * レンダーターゲット制御クラス
		 */
		class RenderTarget {

		private:
			class cImpl;
			std::shared_ptr<cImpl> m_pImpl;	///< インプリメントクラスポインタ


		public:
			//---------------------------------------------------------------------------------
			/**
			 * @brief	コンストラクタ
			 */
			RenderTarget();

			//---------------------------------------------------------------------------------
			/**
			 * @brief	コンストラクタ
			 * @param	width		横幅
			 * @param	height		縦幅
			 * @param	format		テクスチャフォーマット
			 */
			RenderTarget(float width, float height, DXGI_FORMAT format);

			//---------------------------------------------------------------------------------
			/**
			 * @brief	デストラクタ
			 */
			~RenderTarget();

			//---------------------------------------------------------------------------------
			/**
			 * @brief	レンダーターゲット作成
			 * @param	width		横幅
			 * @param	height		縦幅
			 * @param	format		テクスチャフォーマット
			 */
			void Create(float width, float height, DXGI_FORMAT format);


			//---------------------------------------------------------------------------------
			/**
			 * @brief	レンダーターゲットが有効か否か
			 * @return	有効な場合true
			 */
			bool				IsValid();

			//---------------------------------------------------------------------------------
			/**
			 * @brief	2Dバッファ取得
			 * @return	レンダーターゲットのテクスチャ
			 */
			Texture*				GetTexture();

			//---------------------------------------------------------------------------------
			/**
			 * @brief	ビューの取得
			 * @return	レンダーターゲットのビュー
			 */
			IRenderTargetView*		GetRenderTargetView();

		};
	}
}