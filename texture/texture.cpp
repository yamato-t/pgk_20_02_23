﻿#include "texture.h"
#include "../dx11/dx11.h"

namespace core {
	namespace texture {

		using namespace dx11;

		//---------------------------------------------------------------------------------
		/**
		 * @brief
		 * テクスチャ制御インプリメントクラス
		 */
		class Texture::cImpl {
		public:
			ComPtr<ITexture2D>				m_pTexture;			///< テクスチャ
			ComPtr<IShaderResourceView>		m_pTextureView;		///< テクスチャのビュー（シェーダリソースビュー）

		public:
			//---------------------------------------------------------------------------------
			/**
			 * @brief	コンストラクタ
			 */
			cImpl() {
			}


			//---------------------------------------------------------------------------------
			/**
			 * @brief	テクスチャ生成
			 * @param	width		横幅
			 * @param	height		縦幅
			 * @param	format		テクスチャフォーマット
			 * @param	bindFlags	バインド情報
			 * @return
			 */
			cImpl(float width, float height, DXGI_FORMAT format, uint32_t bindFlags) {

				DXGI_FORMAT xFormat			= format;

				D3D11_TEXTURE2D_DESC xTextureDesc;
				memset(&xTextureDesc, 0, sizeof(xTextureDesc));
				xTextureDesc.Format			= xFormat;
				xTextureDesc.Usage			= D3D11_USAGE_DEFAULT;
				xTextureDesc.BindFlags		= bindFlags;
				xTextureDesc.Width			= (uint32_t)width;
				xTextureDesc.Height			= (uint32_t)height;
				xTextureDesc.CPUAccessFlags = 0;
				xTextureDesc.MipLevels		= 1;
				xTextureDesc.MiscFlags		= 0;
				xTextureDesc.ArraySize		= 1;
				xTextureDesc.SampleDesc.Count = 1;
				xTextureDesc.SampleDesc.Quality = 0;

				if (FAILED(dx11Device::Instance().Device()->CreateTexture2D(&xTextureDesc, NULL, m_pTexture.GetPointerPointer()))) {
					assert(false, "テクスチャの作成に失敗しました");
				}

				if(!(D3D11_BIND_DEPTH_STENCIL & bindFlags))
				{
					CreateView();
				}
			}

			//---------------------------------------------------------------------------------
			/**
			 * @brief	テクスチャ生成
			 * @param	filePath	ファイルパス
			 * @return
			 */
			cImpl(std::string filePath)  {

				ID3D11Resource* resource;

				// テクスチャアセットを読み込んでリソースを作成する
				if (FAILED(D3DX11CreateTextureFromFile(dx11Device::Instance().Device(), filePath.c_str(), nullptr, nullptr, &resource, nullptr))) {
					assert(false, "テクスチャアセット読み込みとリソース作成に失敗しました");
				}

				// リソースはm_pTextureのデストラクタで解放される
				m_pTexture.Reset(static_cast<ID3D11Texture2D*>(resource));

				CreateView();
			}

			//---------------------------------------------------------------------------------
			/**
			 * @brief	デストラクタ
			 */
			~cImpl() {
			}

		private:
			//---------------------------------------------------------------------------------
			/**
			 * @brief	テクスチャのビューを作成
			 */
			void CreateView() {

				D3D11_TEXTURE2D_DESC texDesc;
				m_pTexture->GetDesc( &texDesc );

				D3D11_SHADER_RESOURCE_VIEW_DESC xShaderDesc;
				memset(&xShaderDesc, 0, sizeof(xShaderDesc));
				xShaderDesc.Format						= texDesc.Format;
				xShaderDesc.ViewDimension				= D3D11_SRV_DIMENSION_TEXTURE2D;
				xShaderDesc.Texture2D.MostDetailedMip	= 0;;
				xShaderDesc.Texture2D.MipLevels			= 1;

				if (FAILED(dx11Device::Instance().Device()->CreateShaderResourceView(m_pTexture.GetPointer(), &xShaderDesc, m_pTextureView.GetPointerPointer()))) {
					assert(false, " シェーダリソースビューの作成に失敗しました");
				}
			}

		};

		//---------------------------------------------------------------------------------
		/**
		 * @brief	コンストラクタ
		 */
		Texture::Texture() {
			m_pImpl.reset(new Texture::cImpl());
		}


		//---------------------------------------------------------------------------------
		/**
		 * @brief	コンストラクタ
		 * @param	width		横幅
		 * @param	height		縦幅
		 * @param	format		テクスチャフォーマット
		 * @param	bindFlags	バインドフラグ
		 * @return
		 */
		void Texture::Create(float width, float height, DXGI_FORMAT format, uint32_t bindFlags) {
			auto flags = format == DXGI_FORMAT_D32_FLOAT ?	D3D11_BIND_DEPTH_STENCIL :
															(D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE);

			if (bindFlags)
			{
				flags |= bindFlags;
			}

			m_pImpl.reset(new Texture::cImpl(width, height, format, flags));
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	コンストラクタ
		 * @param	filePath	ファイルパス
		 * @return
		 */
		void Texture::Create(std::string filePath) {
			m_pImpl.reset(new Texture::cImpl(filePath));
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	デストラクタ
		 */
		Texture::~Texture() {

		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	テクスチャ取得
		 * @return	テクスチャ
		 */
		ITexture2D* Texture::Get() {
			return m_pImpl->m_pTexture.GetPointer();
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	テクスチャ取得
		 * @return	テクスチャのビュー
		 */
		IShaderResourceView* Texture::GetTextureView() {
			return m_pImpl->m_pTextureView.GetPointer();
		}
	}
}

