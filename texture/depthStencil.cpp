#include "depthStencil.h"
#include "../dx11/dx11.h"
#include "texture.h"

#include <process.h>
#include <mutex>


namespace core {

	namespace texture {

		using namespace dx11;

		//---------------------------------------------------------------------------------
		/**
		 * @brief	
		 * 深度ステンシル制御インプリメントクラス
		 */
		class DepthStencil::cImpl {
		public:
			Texture									m_texture;				///< 深度ステンシルのテクスチャ
			ComPtr<ID3D11DepthStencilView>			m_pDepthStencilView;	///< 深度ステンシルのビュー

		public:

			//---------------------------------------------------------------------------------
			/**
			 * @brief	コンストラクタ
			 */
			cImpl() {
			}

			//---------------------------------------------------------------------------------
			/**
			 * @brief	デストラクタ
			 */
			~cImpl() {
			}

			//---------------------------------------------------------------------------------
			/**
			 * @brief	深度ステンシル作成
			 * @param	width		横幅
			 * @param	height		縦幅
			 * @return
			 */
			void Create(float width, float height) {

				m_texture.Create(width, height, DXGI_FORMAT_D32_FLOAT);

				if (FAILED(dx11Device::Instance().Device()->CreateDepthStencilView(m_texture.Get(), NULL, m_pDepthStencilView.GetPointerPointer()))) {
					assert(false, "深度ステンシルのビューの作成に失敗しました");
				}
			}

		};

		//---------------------------------------------------------------------------------
		/**
		 * @brief	コンストラクタ
		 */
		DepthStencil::DepthStencil() {
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	デストラクタ
		 */
		DepthStencil::~DepthStencil() {

		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	深度ステンシル作成
		 * @param	width		横幅
		 * @param	height		縦幅
		 * @return
		 */
		void DepthStencil::Create(float width, float height) {
			m_pImpl.reset(new DepthStencil::cImpl());
			m_pImpl->Create(width, height);
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	2Dバッファ取得
		 * @return	深度ステンシルのテクスチャ
		 */
		Texture* DepthStencil::GetTexture() {
			return &m_pImpl->m_texture;
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	ビューの取得
		 * @return	深度ステンシルのビュー
		 */
		IDepthStencilView* DepthStencil::GetDepthStencilView() {
			return m_pImpl->m_pDepthStencilView.GetPointer();
		}

	}
}

