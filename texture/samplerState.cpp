#include "depthStencil.h"
#include "../dx11/dx11.h"
#include "samplerState.h"

namespace core {

	namespace texture {

		using namespace dx11;

		//---------------------------------------------------------------------------------
		/**
		 * @brief	
		 * サンプラーステート制御インプリメントクラス
		 */
		class SamplerState::cImpl {
		public:
			ComPtr<ID3D11SamplerState>			m_pSamplerState;	///< サンプラーステート

		public:

			//---------------------------------------------------------------------------------
			/**
			 * @brief	コンストラクタ
			 */
			cImpl() {
			}

			//---------------------------------------------------------------------------------
			/**
			 * @brief	デストラクタ
			 */
			~cImpl() {
			}

			//---------------------------------------------------------------------------------
			/**
			 * @brief	サンプラーステート作成
			 * @param	wrapMode	ラップモード
			 * @param	filterMode	フィルタモード
			 * @return
			 */
			void Create(D3D11_TEXTURE_ADDRESS_MODE wrapMode, D3D11_FILTER filterMode) {

				// サンプラーステートの設定
				D3D11_SAMPLER_DESC samplerDesc;
				samplerDesc.Filter		= filterMode;
				samplerDesc.AddressU	= wrapMode;		
				samplerDesc.AddressV	= wrapMode;		
				samplerDesc.AddressW	= wrapMode;		
				samplerDesc.MipLODBias	= 0;
				samplerDesc.MinLOD		= 0;                                
				samplerDesc.MaxLOD		= D3D11_FLOAT32_MAX; 

				samplerDesc.MaxAnisotropy	= 1;                        
				samplerDesc.ComparisonFunc	= D3D11_COMPARISON_ALWAYS;  

				// 境界色
				samplerDesc.BorderColor[0] = 0;
				samplerDesc.BorderColor[1] = 0;
				samplerDesc.BorderColor[2] = 0;
				samplerDesc.BorderColor[3] = 0;

				if (FAILED(dx11Device::Instance().Device()->CreateSamplerState( &samplerDesc, m_pSamplerState.GetPointerPointer()))) {
					assert(false, "サンプラーの作成に失敗しました");
				}
			}

		};

		//---------------------------------------------------------------------------------
		/**
		 * @brief	コンストラクタ
		 */
		SamplerState::SamplerState() {
			m_pImpl.reset(new SamplerState::cImpl());
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	デストラクタ
		 */
		SamplerState::~SamplerState() {

		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	サンプラーステート作成
		 * @param	wrapMode	ラップモード
		 * @param	filterMode	フィルタモード
		 * @return
		 */
		void SamplerState::Create(D3D11_TEXTURE_ADDRESS_MODE wrapMode, D3D11_FILTER filterMode) {
			m_pImpl->Create(wrapMode, filterMode);
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	サンプラーステート取得
		 * @return	サンプラーステート
		 */
		ID3D11SamplerState* SamplerState::GetSamplerState() {
			return m_pImpl->m_pSamplerState.GetPointer();
		}
	}
}

