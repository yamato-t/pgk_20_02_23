﻿#pragma once

#include "../dx11/dx11def.h"
#include <string>

namespace core {

	namespace texture {

		//---------------------------------------------------------------------------------
		/**
		 * @brief
		 * 3Dテクスチャの制御
		 */
		class Texture3d {

		private:
			class cImpl;
			std::shared_ptr<cImpl> m_pImpl;	///< インプリメントクラスポインタ

		public:
			//---------------------------------------------------------------------------------
			/**
			 * @brief	コンストラクタ
			 */
			Texture3d();

			//---------------------------------------------------------------------------------
			/**
			 * @brief	デストラクタ
			 */
			~Texture3d();

			//---------------------------------------------------------------------------------
			/**
			 * @brief	テクスチャ作成
			 * @param	filePath	ファイルパス
			 * @param	width		横幅
			 * @param	height		縦幅
			 * @param	depth		深度
			 * @param	format		テクスチャフォーマット
			 * @param	bindFlags	バインドフラグ
			 * @return
			 */
			void Create(const std::string& filePath, uint32_t width, uint32_t height, uint32_t depth, DXGI_FORMAT format, uint32_t bindFlags);

			//---------------------------------------------------------------------------------
			/**
			 * @brief	テクスチャ取得
			 * @return	テクスチャ
			 */
			ITexture3D* Get();

			//---------------------------------------------------------------------------------
			/**
			 * @brief	テクスチャ取得
			 * @return	テクスチャのビュー
			 */
			IShaderResourceView*	GetTextureView();
		};
	}

}
