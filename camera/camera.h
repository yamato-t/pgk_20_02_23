#pragma once

#include "../obj/objBase.h"
#include "../math/math.h"
#include "../buffer/constantBuffer.h"

namespace core {

	namespace camera {

		//---------------------------------------------------------------------------------
		/**
		 * @brief	
		 * 透視投影カメラの制御
		 */
		class Camera : public obj::ObjBase {

		public:
			//---------------------------------------------------------------------------------
			/**
			 * @brief	コントラクタ
			 */
			Camera();

			//---------------------------------------------------------------------------------
			/**
			 * @brief	デストラクタ
			 */
			virtual ~Camera();

			//---------------------------------------------------------------------------------
			/**
			 * @brief	カメラ注視座標設定
			 * @param	target	ターゲットのワールド座標
			 * @return
			 */
			void Target(const vec3& target);

			//---------------------------------------------------------------------------------
			/**
			 * @brief	カメラ注視座標取得
			 * @return　ターゲットのワールド座標
			 */
			const vec3& Target() { return m_target; }


			//---------------------------------------------------------------------------------
			/**
			 * @brief	画角設定
			 * @param	fov		画角
			 * @return
			 */
			void Fov(float fov);

			//---------------------------------------------------------------------------------
			/**
			 * @brief	アスペクト設定
			 * @param	aspect	アスペクト比
			 * @return
			 */
			void Aspect(float aspect);

			//---------------------------------------------------------------------------------
			/**
			 * @brief	ニアとファーの距離を設定
			 * @param	nearClip	ニア距離
			 * @param	farClip		ファー距離
			 * @return
			 */
			void NearAndFar(float nearClip, float farClip);

			//---------------------------------------------------------------------------------
			/**
			 * @brief	ビュー行列とプロジェクション行列の更新
			 * @return
			 */
			virtual void UpdateViewProjection();

			//---------------------------------------------------------------------------------
			/**
			 * @brief	透視投影カメラのコンスタントバッファを更新
			 * @param	pContext	描画コンテキスト
			 * @return
			 */
			virtual void UpdateConstantBuffer(ID3D11DeviceContext* pContext);



		protected:
			buffer::ConstantBuffer	m_constantBuffer;		///< カメラコンスタントバッファ

			vec3					m_target;				///< ターゲットのワールド座標
			float					m_fov;					///< 画角
			float					m_aspect;				///< アスペクト比
			float					m_near;					///< ニアクリップ距離
			float					m_far;					///< ファークリップ距離

			matrix					m_projection;			///< プロジェクション変換行列
		};
	}
};