﻿#include "camera.h"
#include "../win/window.h"

namespace core {

	namespace camera {

		//---------------------------------------------------------------------------------
		/**
		 * @brief
		 * 透視投影カメラのコンスタントバッファ内容
		 */
		struct ConstantBufferFormat
		{
			vec4	position;			///< カメラのワールド座標
			vec4	target;				///< カメラの注視座標
			matrix	view;				///< ビュー変換行列
			matrix	perspective;		///< パースペクティブ変換行列
		};

		//---------------------------------------------------------------------------------
		/**
		 * @brief	コントラクタ
		 */
		Camera::Camera()
		: m_target(0,0,0)
		, m_fov((float)(D3DX_PI / 4.0f))
		, m_aspect((float)WINDOW_WIDTH / (float)WINDOW_HEIGHT)
		, m_near(0.1f)
		, m_far(100.0f) {
			m_position.Set(0,0,-3.3);
			m_constantBuffer.Create(sizeof(ConstantBufferFormat));
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	デストラクタ
		 */
		Camera::~Camera() {
		}


		//---------------------------------------------------------------------------------
		/**
		 * @brief	カメラ注視座標設定
		 * @param	target	ターゲットのワールド座標
		 * @return
		 */
		void Camera::Target(const vec3& target) {
			m_update	|= (m_target != target);
			m_target	= target;
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	画角設定
		 * @param	fov		画角
		 * @return
		 */
		void Camera::Fov(float fov) {
			m_fov = fov;
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	アスペクト設定
		 * @param	aspect	アスペクト比
		 * @return
		 */
		void Camera::Aspect(float aspect) {
			m_aspect = aspect;
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	ニアとファーの距離を設定
		 * @param	nearClip	ニア距離
		 * @param	farClip		ファー距離
		 * @return
		 */
		void Camera::NearAndFar(float nearClip, float farClip) {
			m_near	= nearClip;
			m_far	= farClip;
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	ビュー行列とプロジェクション行列の更新
		 * @return
		 */
		void Camera::UpdateViewProjection() {
			if(m_update) {

				// ビュー変換行列
				vec3 up(0.0f, 1.0f, 0.0f);	// 上方位置（内積と外積から正しいUPベクトルの計算が必要）
				math::MatrixLookAt(&m_transform, &m_position, &up, &m_target);


				// 射影変換行列
				D3DXMATRIX d3dMatrix;
				D3DXMatrixPerspectiveFovLH(&d3dMatrix, m_fov, m_aspect, m_near, m_far);

				m_projection = matrix(
					d3dMatrix.m[0][0], d3dMatrix.m[0][1], d3dMatrix.m[0][2], d3dMatrix.m[0][3],
					d3dMatrix.m[1][0], d3dMatrix.m[1][1], d3dMatrix.m[1][2], d3dMatrix.m[1][3],
					d3dMatrix.m[2][0], d3dMatrix.m[2][1], d3dMatrix.m[2][2], d3dMatrix.m[2][3],
					d3dMatrix.m[3][0], d3dMatrix.m[3][1], d3dMatrix.m[3][2], d3dMatrix.m[3][3]
				);
			}
		}


		//---------------------------------------------------------------------------------
		/**
		 * @brief	カメラのコンスタントバッファを更新
		 * @param	pContext	描画コンテキスト
		 * @return
		 */
		void Camera::UpdateConstantBuffer(ID3D11DeviceContext* pContext) {

			// 必要なときのみコンスタントバッファを更新
			if(m_update) {

				// コンスタントバッファ
				ConstantBufferFormat cBuffer;

				cBuffer.position.Set(m_position.x, m_position.y, m_position.z, 1.0f);
				cBuffer.target.Set(m_target.x, m_target.y, m_target.z, 1.0f);
				math::MatrixTranspose(&cBuffer.view,		&m_transform);
				math::MatrixTranspose(&cBuffer.perspective, &m_projection);

				// コンスタントバッファへコピー
				D3D11_MAPPED_SUBRESOURCE	pData;
				ID3D11Buffer*				pBuffer = m_constantBuffer.GetBuffer();

				if (SUCCEEDED(pContext->Map(pBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &pData))) {
					memcpy(pData.pData, (void*)&cBuffer, sizeof(ConstantBufferFormat));
					pContext->Unmap(pBuffer, 0);
				}

				m_update = false;
			}

			// シェーダーにコンスタントバッファデータを渡す
			ID3D11Buffer* p[1] = { m_constantBuffer.GetBuffer() };
			pContext->VSSetConstantBuffers(1, 1, p);
			pContext->PSSetConstantBuffers(1, 1, p);
		}
	}
}
