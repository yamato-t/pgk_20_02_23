﻿#include "cameraOrtho.h"
#include "../win/window.h"

namespace core {

	namespace camera {

		//---------------------------------------------------------------------------------
		/**
		 * @brief
		 * 透視投影カメラのコンスタントバッファ内容
		 */
		struct ConstantBufferFormat
		{
			vec4	position;	///< カメラのワールド座標
			vec4	target;		///< カメラの注視座標
			matrix	view;		///< ビュー変換行列（未使用）
			matrix	ortho;		///< 正射影変換行列
		};

		//---------------------------------------------------------------------------------
		/**
		 * @brief	コントラクタ
		 */
		CameraOrtho::CameraOrtho()
		{
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	デストラクタ
		 */
		CameraOrtho::~CameraOrtho() {
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	ビュー行列とプロジェクション行列の更新
		 * @return
		 */
		void CameraOrtho::UpdateViewProjection() {
			if(m_update) {

				// ビュー変換行列
				vec3 up(0.0f, 1.0f, 0.0f);	// 上方位置（内積と外積から正しいUPベクトルの計算が必要）
				math::MatrixLookAt(&m_transform, &m_position, &up, &m_target);

				// 正射影変換行列
				D3DXMATRIX d3dMatrix;
				D3DXMatrixOrthoLH(&d3dMatrix, 16/*WINDOW_WIDTH*/, 9/*WINDOW_HEIGHT*/, m_near, m_far);

				m_projection = matrix(
					d3dMatrix.m[0][0], d3dMatrix.m[0][1], d3dMatrix.m[0][2], d3dMatrix.m[0][3],
					d3dMatrix.m[1][0], d3dMatrix.m[1][1], d3dMatrix.m[1][2], d3dMatrix.m[1][3],
					d3dMatrix.m[2][0], d3dMatrix.m[2][1], d3dMatrix.m[2][2], d3dMatrix.m[2][3],
					d3dMatrix.m[3][0], d3dMatrix.m[3][1], d3dMatrix.m[3][2], d3dMatrix.m[3][3]
				);
			}
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	スクリーン投影のビューとプロジェクション行列の更新
		 * @return
		*/
		void CameraOrtho::UpdateScreenViewProjection() {

			if(m_update)
			{
				// ビューは単位行列に
				math::MatrixIdentity(&m_transform);

				// スクリーン投影行列を設定
				m_projection = matrix(
					2/(float)WINDOW_WIDTH,	0.0f,						0.0f, 0.0f,
					0.0f,					-2/(float)WINDOW_HEIGHT,	0.0f, 0.0f,		// 3Dでなくスクリーンスペースなので、Yを反転
					0.0f,					0.0f,						1.0f, 0.0f,
					-1.0f,					1.0f,						0.0f, 1.0f		// 縦横が-1～1に変換されるので、オフセットを入力
				);
			}
		}


		//---------------------------------------------------------------------------------
		/**
		 * @brief	正射影カメラのコンスタントバッファを更新
		 * @param	pContext	描画コンテキスト
		 * @return
		 */
		void CameraOrtho::UpdateConstantBuffer(ID3D11DeviceContext* pContext) {

			// 必要なときのみコンスタントバッファを更新
			if(m_update) {
				// コンスタントバッファ
				ConstantBufferFormat cBuffer;

				cBuffer.position.Set(m_position.x, m_position.y, m_position.z, 1.0f);
				cBuffer.target.Set(m_target.x, m_target.y, m_target.z, 1.0f);
				math::MatrixTranspose(&cBuffer.view,	&m_transform);
				math::MatrixTranspose(&cBuffer.ortho,	&m_projection);

				// コンスタントバッファへコピー
				D3D11_MAPPED_SUBRESOURCE	pData;
				ID3D11Buffer*				pBuffer = m_constantBuffer.GetBuffer();

				if (SUCCEEDED(pContext->Map(pBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &pData))) {
					memcpy(pData.pData, (void*)&cBuffer, sizeof(ConstantBufferFormat));
					pContext->Unmap(pBuffer, 0);
				}

				m_update = false;
			}

			// シェーダーにコンスタントバッファデータを渡す
			ID3D11Buffer* p[1] = { m_constantBuffer.GetBuffer() };
			pContext->VSSetConstantBuffers(1, 1, p);
			pContext->PSSetConstantBuffers(1, 1, p);
		}
	}
}
