#pragma once

#include "../math.h"
#include "singleton.h"

namespace core
{
	namespace math
	{
		namespace collision
		{
			//---------------------------------------------------------------------------------
			/**
			 * @brief	
			 * 球情報構造体
			 */
			struct SPHERE {
				vec4	m_vCenter;	///< 中心位置
				float	m_fRadius;	///< 球半径
			};

			//---------------------------------------------------------------------------------
			/**
			 * @brief	
			 * 球形状のコリジョン
			 */
			class Sphere : public util::Singleton<Sphere> {
			private:
				friend class util::Singleton<Sphere>;

			public:
				// -----------------------------------------------------------------------------
				/**
				 * @brief		球とレイの交差
				 * @param		pSphere		球
				 * @param		pStart		開始座標
				 * @param		pRay		レイ（無限遠）
				 * @param[out]	pCross		交点
				 * @param[out]	pOffset		交点までのオフセット
				 * @return		交差していればtrue
				 */
				bool CrossSphereAndRay(const SPHERE* pSphere, const vec4* pStart, const vec4* pRay, vec4* pCross, vec4* pOffset);

				// -----------------------------------------------------------------------------
				/**
				 * @brief			球とベクトルの交差
				 * @param			pSphere		球
				 * @param			pStart		開始座標
				 * @param			pDir		ベクトル
				 * @param[in,out]	pLength		ベクトルの長さ、ヒット時は交点までの長さに更新
				 * @param[out]		pCross		交点
				 * @param[out]		pOffset		交点までのオフセット
				 * @return			交差していればtrue
				 */
				bool CrossSphereAndDir(const SPHERE* pSphere, const vec4* pStart, const vec4* pDir, float* pLength, vec4* pCross, vec4* pOffset);

				// -----------------------------------------------------------------------------
				/**
				 * @brief		単純な球と球の衝突判定
				 * @param		pSphere1	球
				 * @param		pSphere2	球
				 * @param[out]	pCross		球2の座標
				 * @param[out]	pOffset		球1と球2の距離
				 * @return		交差していればtrue
				 */
				bool HitSphereAndSphere(const SPHERE* pSphere1, const SPHERE* pSphere2, vec4* pCross, vec4* pOffset);

				// -----------------------------------------------------------------------------
				/**
				 * @brief		移動球と球の衝突判定
				 * @param		pMSphere	移動球
				 * @param		pMove		移動ベクトル
				 * @param		pSphere		球
				 * @param[out]	pCross		ぶつかった座標
				 * @param[out]	pOffset		座標までのベクトル
				 * @return		交差していればtrue
				 */
				bool HitMSphereAndSphere(const SPHERE* pMSphere, const vec4* pMove, const SPHERE* pSphere, vec4* pCross, vec4* pOffset);

				// -----------------------------------------------------------------------------
				/**
				 * @brief			移動球同士の衝突判定
				 * @param			pMSphere1	移動球1
				 * @param			pMove1		1の移動ベクトル
				 * @param			pMSphere2	移動球2
				 * @param			pMove2		2の移動ベクトル
				 * @param[out]		pCross		ぶつかった座標
				 * @param[out]		pOffset		座標までのベクトル
				 * @return			交差していればtrue
				 */
				bool HitMSphereAndMSphere(const SPHERE* pMSphere1, const vec4* pMove1, const SPHERE* pMSphere2, const vec4* pMove2, vec4* pCross, vec4* pOffset);

				// -----------------------------------------------------------------------------
				/**
				 * @brief			移動球とポリゴンの衝突判定
				 * @param			pMSphere	移動球
				 * @param			pMove		移動ベクトル
				 * @param			pPolygon	ポリゴン
				 * @param[out]		pCross		接触時の球の座標
				 * @param[out]		pOffset		接触時の球の座標までのオフセット
				 * @return			交差していればtrue
				 */
				template <typename _t>
				bool HitMSphereAndPolygon(const SPHERE* pMSphere, const vec4* pMove, const _t* pPolygon, vec4* pCross, vec4* pOffset);

			private:
				// -----------------------------------------------------------------------------
				/**
				 * @brief		球とレイの交差
				 * @param		pShpere		球
				 * @param		pStart		開始座標
				 * @param		pRay		レイ（無限遠）
				 * @param[out]	pLength		交点までの長さ
				 * @return		交差していればtrue
				 */
				bool CheckCrossSphereAndRay(const SPHERE* pSphere, const vec4* pStart, const vec4* pRay, float* pLength);


				// -----------------------------------------------------------------------------
				/**
				 * @brief		球とベクトルの交差
				 * @param		pShpere		球
				 * @param		pStart		開始座標
				 * @param		pDir		単位ベクトル
				 * @param		fLengthDir	ベクトル長さ
				 * @param[out]	pLength		交点までの長さ
				 * @return		交差していればtrue
				 */
				bool CheckCrossSphereAndDir(const SPHERE* pSphere, const vec4* pStart, const vec4* pDir, float fLengthDir, float* pLength);

				// -----------------------------------------------------------------------------
				/**
				 * @brief		球と平面の交差判定
				 * @param		pSphere		球
				 * @param		pPoint		平面上の点
				 * @param		pNormal		平面上の法線
				 * @param[out]	pLength		交点までの長さ
				 * @return		交差していればtrue
				 */
				bool CheckCrossSphereAndPlane(const SPHERE* pSphere, const vec4* pPoint, const vec4* pNormal, float* pLength);

				// -----------------------------------------------------------------------------
				/**
				 * @brief		移動球と平面の交差判定
				 * @param		pMSphere	移動球
				 * @param		pMove		移動ベクトル
				 * @param		pPoint		平面上の点
				 * @param		pNormal		平面上の法線
				 * @param[out]	pLength		交点までの長さ
				 * @return		交差していればtrue
				 */
				bool CheckHitMSphereAndPlane(const SPHERE* pMSphere, const vec4* pMove, const vec4* pPoint, const vec4* pNormal, float* pLength);

			private:
				Sphere() {}
				virtual ~Sphere() {}


			};
		}
	}
}
