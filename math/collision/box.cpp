#include "box.h"

namespace core {

	namespace math {

		namespace collision {

			// ----------------------------------------------------------------------------------------
			/**
			 * @brief			AABBとベクトルの交差
			 * @param			pAabb		AABB
			 * @param			pStart		開始座標
			 * @param			pDir		単位ベクトル
			 * @param[in,out]	pLength		ベクトルの長さ、ヒット時は交点までの長さに更新
			 * @return			交差していればtrue
			 */
			bool Box::CheckCrossAabbAndDir(const AABB* pAabb, const vec4* pStart, const vec4* pDir, float* pLength) {

				// 各軸に対して、厚みのあるプレート（スラブ）に線分が入った点と出た点を計算
				// 結果、三軸スラブに対しての線分入時間と線分出時間が分かる

				float fTimeIn = FLT_MIN;
				float fTimeOut = FLT_MAX;

				vec4 vRay;
				Vec4Scale(&vRay, pDir, (*pLength));

				// X軸スラブ
				if (fabs(vRay.x) < FLT_EPSILON) {
					// 軸の交差なし
					if (pStart->x < -pAabb->m_vDistance.x || pAabb->m_vDistance.x < pStart->x) {
						return false;
					}
				}
				else {
					// スラブの最遅入時間と最速出時間を計算
					float fInX = (-pAabb->m_vDistance.x - pStart->x) * (1.0f / vRay.x);
					float fOutX = (pAabb->m_vDistance.x - pStart->x) * (1.0f / vRay.x);

					// 逆の関係なので値入れ替え
					if (fOutX < fInX) { float fTemp = fOutX; fOutX = fInX; fInX = fTemp; }

					// 最遅入時間の更新
					if (fTimeIn < fInX) { fTimeIn = fInX; }
					// 最速出時間の更新
					if (fOutX < fTimeOut) { fTimeOut = fOutX; }
					// 交差していない
					if (fTimeOut < fTimeIn) {
						return false;
					}
				}


				// Y軸スラブ
				if (fabs(vRay.y) < FLT_EPSILON) {
					// 軸の交差なし
					if (pStart->y < -pAabb->m_vDistance.y || pAabb->m_vDistance.y < pStart->y) {
						return false;
					}
				}
				else {
					// スラブの最遅入時間と最速出時間を計算
					float fInY = (-pAabb->m_vDistance.y - pStart->y) * (1.0f / vRay.y);
					float fOutY = (pAabb->m_vDistance.y - pStart->y) * (1.0f / vRay.y);

					// 逆の関係なので値入れ替え
					if (fOutY < fInY) { float fTemp = fOutY; fOutY = fInY; fInY = fTemp; }

					// 最遅入時間の更新
					if (fTimeIn < fInY) { fTimeIn = fInY; }
					// 最速出時間の更新
					if (fOutY < fTimeOut) { fTimeOut = fOutY; }
					// 交差していない
					if (fTimeOut < fTimeIn) {
						return false;
					}
				}


				// Z軸スラブ
				if (fabs(vRay.z) < FLT_EPSILON) {
					// 軸の交差なし
					if (pStart->z < -pAabb->m_vDistance.z || pAabb->m_vDistance.z < pStart->z) {
						return false;
					}
				}
				else {
					// スラブの最遅入時間と最速出時間を計算
					float fInZ = (-pAabb->m_vDistance.z - pStart->z) * (1.0f / vRay.z);
					float fOutZ = (pAabb->m_vDistance.z - pStart->z) * (1.0f / vRay.z);

					// 逆の関係なので値入れ替え
					if (fOutZ < fInZ) { float fTemp = fOutZ; fOutZ = fInZ; fInZ = fTemp; }

					// 最遅入時間の更新
					if (fTimeIn < fInZ) { fTimeIn = fInZ; }
					// 最速出時間の更新
					if (fOutZ < fTimeOut) { fTimeOut = fOutZ; }
					// 交差していない
					if (fTimeOut < fTimeIn) {
						return false;
					}
				}

				float fPreviousLength = (*pLength);
				float fLength = ((*pLength)*fTimeIn);

				if (fPreviousLength < fLength || fLength <= 0.0f) {
					// ベクトルの長さ以上の場所で交差している、または後ろ側で交差している
					//TRACE(("[fPreviousLength = %f] [fLength = %f]\n", fPreviousLength, fLength));
					return false;
				}

				*pLength = fLength;

				return true;
			}

			// ----------------------------------------------------------------------------------------
			/**
			 * @brief		点のAABB内包チェック
			 * @param		pAabb		AABB
			 * @param		pPos		座標
			 * @return		点が内包されていればtrue
			 */
			bool Box::CheckPosInAabb(const AABB* pAabb, const vec4* pPos) {
				// 軸の交差なし
				if (pPos->x < -pAabb->m_vDistance.x || pAabb->m_vDistance.x < pPos->x) {
					return false;
				}
				// 軸の交差なし
				if (pPos->y < -pAabb->m_vDistance.y || pAabb->m_vDistance.y < pPos->y) {
					return false;
				}
				// 軸の交差なし
				if (pPos->z < -pAabb->m_vDistance.z || pAabb->m_vDistance.z < pPos->z) {
					return false;
				}

				return true;
			}



			// ----------------------------------------------------------------------------------------
			/**
			 * @brief			OBBとベクトルの交差
			 * @param			pObb		OBB
			 * @param			pStart		開始座標
			 * @param			pDir		単位ベクトル
			 * @param[in,out]	pLength		ベクトルの長さ、ヒット時は交点までの長さに更新
			 * @param[out]		pNormal		交差する平面の法線
			 * @return		交差していればtrue
			 */
			bool Box::CheckCrossObbAndDir(const OBB* pObb, const vec4* pStart, const vec4* pDir, float* pLength, vec4* pNormal) {
				// ベクトルや開始座標をOBBの回転を反映させてAABBとして計算する


				// OBB中心点を原点にする為、レイの開始座標をあらかじめ変更する
				vec4 vStart = *pStart;
				Vec4Subtract(&vStart, &vStart, &pObb->m_vCenter);

				MATRIX mMat;
				MatrixIdentity(&mMat);
				*(vec4*)mMat.m[0] = pObb->m_vAxis[0];
				*(vec4*)mMat.m[1] = pObb->m_vAxis[1];
				*(vec4*)mMat.m[2] = pObb->m_vAxis[2];
				*(vec4*)mMat.m[3] = vec4(0.0f, 0.0f, 0.0f, 1.0f);

				// 逆行列を使用して以下OBB回転をAABBに
				MATRIX mInv;
				MatrixInverse(&mInv, &mMat);

				// AABBとしてセット（座標は原点の為、設定する必要なし）
				AABB xAabb;
				memset(&xAabb, 0, sizeof(AABB));
				xAabb.m_vDistance.x = pObb->m_fRadius[0];
				xAabb.m_vDistance.y = pObb->m_fRadius[1];
				xAabb.m_vDistance.z = pObb->m_fRadius[2];
				Vec4Transform(&xAabb.m_vDistance, &xAabb.m_vDistance, &mInv);

				// レイ開始座標回転
				Vec4Transform(&vStart, &vStart, &mInv);

				// レイ回転
				vec4 vRay;
				Vec4Transform(&vRay, pDir, &mInv);

				if (CheckCrossAabbAndDir(&xAabb, &vStart, &vRay, pLength)) {

					/*
					* 法線はオブジェクトの逆行列を掛け合わせている為
					*	(1.0f, 0.0f, 0.0f) (-1.0f, 0.0f, 0.0f)
					*	(0.0f, 1.0f, 0.0f) (0.0f, -1.0f, 0.0f)
					*	(0.0f, 0.0f, 1.0f) (0.0f, 0.0f, -1.0f)
					* の六通りになる
					*/

					// 交点計算
					vec4 vCross;
					Vec4Scale(&vRay, &vRay, (*pLength));
					Vec4Add(&vCross, &vStart, &vRay);

					float fx = fabs(xAabb.m_vDistance.x - fabs(vCross.x));
					float fy = fabs(xAabb.m_vDistance.y - fabs(vCross.y));
					float fz = fabs(xAabb.m_vDistance.z - fabs(vCross.z));

					// 差が一番小さい軸を調べる
					char cMin = 0;
					if (fx < fy) {
						if (fx < fz) { cMin = 0; }
						else { cMin = 2; }
					}
					else {
						if (fy < fz) { cMin = 1; }
						else { cMin = 2; }
					}

					vec4 vNormal(0.0f, 0.0f, 0.0f, 0.0f);
					switch (cMin) {
					case 0: vNormal.x = (vCross.x < 0) ? -1.0f : 1.0f; break;
					case 1: vNormal.y = (vCross.y < 0) ? -1.0f : 1.0f; break;
					case 2: vNormal.z = (vCross.z < 0) ? -1.0f : 1.0f; break;
					}

					Vec4Transform(pNormal, &vNormal, &mMat);

					return true;
				}


				return false;
			}

			// ----------------------------------------------------------------------------------------
			/**
			 * @brief		OBBとベクトルの交差
			 * @param			pObb		OBB
			 * @param			pStart		開始座標
			 * @param			pDir		単位ベクトル
			 * @param[in,out]	pLength		ベクトルの長さ、ヒット時は交点までの長さに更新
			 * @param[out]		pCross		交点
			 * @param[out]		pOffset		交点までのオフセット
			 * @param[out]		pNormal		当たった面の法線
			 * @return			交差していればtrue
			 */
			bool Box::CrossObbAndDir(const OBB* pObb, const vec4* pStart, const vec4* pDir, float* pLength, vec4* pCross, vec4* pOffset, vec4* pNormal) {

				float fP = *pLength;

				if (!CheckCrossObbAndDir(pObb, pStart, pDir, &fP, pNormal)) {
					return false;
				}

				Vec4Scale(pOffset, pDir, fP);
				Vec4Add(pCross, pStart, pOffset);
				*pLength = fP;

				return true;
			}

			// ----------------------------------------------------------------------------------------
			/**
			 * @brief		OBBと球の当たり
			 * @param		pObb		OBB
			 * @param		pSphere		球
			 * @param[out]	pCross		交点
			 * @param[out]	pOffset		交点までのオフセット
			 * @return	`	当たっていればtrue
			 */
			bool Box::CrossObbAndSphere(const OBB* pObb, const SPHERE* pSphere, vec4* pCross, vec4* pOffset) {
				// ベクトルや開始座標をOBBの回転を反映させてAABBとして計算する

				// OBB中心点を原点にする為、座標をあらかじめ変更する
				vec4 vStart = pSphere->m_vCenter;
				Vec4Subtract(&vStart, &vStart, &pObb->m_vCenter);

				MATRIX mMat;
				MatrixIdentity(&mMat);
				*(vec4*)mMat.m[0] = pObb->m_vAxis[0];
				*(vec4*)mMat.m[1] = pObb->m_vAxis[1];
				*(vec4*)mMat.m[2] = pObb->m_vAxis[2];
				*(vec4*)mMat.m[3] = vec4(0.0f, 0.0f, 0.0f, 1.0f);

				// 逆行列を使用して以下OBB回転をAABBに
				MATRIX mInv;
				MatrixInverse(&mInv, &mMat);

				// AABBとしてセット（座標は原点の為、設定する必要なし）
				AABB xAabb;
				memset(&xAabb, 0, sizeof(AABB));
				xAabb.m_vDistance.x = pObb->m_fRadius[0];
				xAabb.m_vDistance.y = pObb->m_fRadius[1];
				xAabb.m_vDistance.z = pObb->m_fRadius[2];
				Vec4Transform(&xAabb.m_vDistance, &xAabb.m_vDistance, &mInv);

				// 球の半径分、AABBのサイズを大きくする
				xAabb.m_vDistance.x += pSphere->m_fRadius;
				xAabb.m_vDistance.y += pSphere->m_fRadius;
				xAabb.m_vDistance.z += pSphere->m_fRadius;

				// 座標回転
				Vec4Transform(&vStart, &vStart, &mInv);

				if (!CheckPosInAabb(&xAabb, &vStart)) {
					return false;
				}

				*pCross = pObb->m_vCenter;
				Vec4Subtract(pOffset, pCross, &pSphere->m_vCenter);

				return true;
			}
		}
	}
}