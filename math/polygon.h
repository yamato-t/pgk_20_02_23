#pragma once

#include "math.h"

namespace core {

	namespace math {
		/**
		*	三角形ポリゴン
		*/
		struct POLYGON_3 {
			enum {
				VERTEX_NUM = 3
			};

			VECTOR4 m_vVertex[VERTEX_NUM];
		};

		/**
		*	四角形ポリゴン
		*/
		struct POLYGON_4 {
			enum {
				VERTEX_NUM = 4
			};

			VECTOR4 m_vVertex[VERTEX_NUM];
		};


		class Polygon {
		protected:
			Polygon() {}
			virtual ~Polygon() {}

		protected:
			// -------------------------------------------------------------
			/**
			*	ポリゴンの法線取得
			*	pPolygon	ポリゴン
			*/
			template <typename _t>
			void GetNormal(VECTOR4* pNormal, const _t* pPolygon);

			// -------------------------------------------------------------
			/**
			*	ポイントの内包チェック
			*	pPoint		平面上の点
			*	pNormal		正規化済みの面法線
			*	pStart		ライン開始座標
			*	pLine		ラインベクトル
			*	pDistance	衝突ポイントまでの距離
			*/
			template <typename _t>
			bool CheckPointInPolygon(const _t* pPolygon, const VECTOR4* pPoint);
		};
	}
}