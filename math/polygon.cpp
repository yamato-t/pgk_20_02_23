
#include "polygon.h"

namespace core {

	namespace math {
		// -----------------------------------------------------------------------------
		/**
		*	ポリゴンの法線取得
		*	pPolygon	ポリゴン
		*/
		template <typename _t>
		void Polygon::GetNormal(VECTOR4* pNormal, const _t* pPolygon) {
			VECTOR4 vVertexToVertex[2];

			// ポリゴン外周のベクトルから法線取得

			// 方向が逆かもしれない。確認後変更
			Vec4Subtract(&vVertexToVertex[0], &pPolygon->m_vVertex[1], &pPolygon->m_vVertex[0]);
			Vec4Subtract(&vVertexToVertex[1], &pPolygon->m_vVertex[_t::VERTEX_NUM - 1], &pPolygon->m_vVertex[0]);

			VECTOR4 vNormal;
			Vec4Cross(&vNormal, &vVertexToVertex[0], &vVertexToVertex[1]);
			Vec4Normalize(&vNormal, &vNormal);

			*pNormal = vNormal;
		}

		/**-----------------------------------------------------------------------------
		*	ポリゴンのポイント内包チェック
		*	pPorygon ポリゴン
		*	pPorygon チェックポイント
		*/
		template <typename _t>
		bool Polygon::CheckPointInPolygon(const _t* pPolygon, const VECTOR4* pPoint) {
			// ポイントから各頂点までのベクトルを計算し、それぞれ外積を計算
			// 基本の外積ベクトルを決めて、それ以外の外積ベクトルとの内積を計算し、判定


			VECTOR4 vVertexToVertex[_t::VERTEX_NUM];
			for (s32 i = 0; i < _t::VERTEX_NUM; i++) {
				AGCVec4Subtract(&vVertexToVertex[i], &pPolygon->m_vVertex[i], pPoint);
			}

			// 基本にする外積ベクトル
			VECTOR4 vNormal;
			Vec4Cross(&vNormal, &vVertexToVertex[(_t::VERTEX_NUM - 1)], &vVertexToVertex[0]);

			for (s32 i = 0; i < (_t::VERTEX_NUM - 1); i++) {
				// 内包チェック
				VECTOR4 vCross;
				Vec4Cross(&vCross, &vVertexToVertex[i], &vVertexToVertex[i + 1]);
				if (Vec4Dot(&vNormal, &vCross) < 0.0f) { return false; }
			}

			return true;
		}
	}
}