#pragma once	

#include <math.h>
#include <string.h>
#include <float.h>

namespace core
{
	namespace math
	{
		struct VECTOR4
		{
		public:
			float	x, y, z, w;

		public:
			VECTOR4() {}
			VECTOR4(float xx, float yy, float zz, float ww): x(xx), y(yy), z(zz), w(ww) {}
			VECTOR4(float v): x(v), y(v), z(v), w(v) {}

			inline void Clear(void) { x = 0.f; y = 0.f; z = 0.f; w = 0.f; }
			inline void Set(float xx, float yy, float zz, float ww) { x = xx; y = yy; z = zz; w = ww; }

			operator float*(void) { return &x; }

			VECTOR4 &operator+=(const VECTOR4 &vTemp) {
				x += vTemp.x;
				y += vTemp.y;
				z += vTemp.z;
				w += vTemp.w;
				return *this;
			}
			VECTOR4 &operator-=(const VECTOR4 &vTemp) {
				x -= vTemp.x;
				y -= vTemp.y;
				z -= vTemp.z;
				w -= vTemp.w;
				return *this;
			}
			VECTOR4 &operator*=(float fTemp) {
				x *= fTemp;
				y *= fTemp;
				z *= fTemp;
				w *= fTemp;
				return *this;
			}
			VECTOR4 &operator/=(float fTemp) {
				fTemp = 1.0f / fTemp;
				x *= fTemp;
				y *= fTemp;
				z *= fTemp;
				w *= fTemp;
				return *this;
			}

			bool operator!=(const VECTOR4 &vTemp) { return ((x != vTemp.x) || (y != vTemp.y) || (z != vTemp.z) || (w != vTemp.w)); }
			bool operator==(const VECTOR4 &vTemp) { return !(*this != vTemp); }

			float &operator[](int i) { return ((&x)[i]); }

			VECTOR4 operator+(void) const					{ return VECTOR4(+x, +y, +z, +w); }
			VECTOR4 operator-(void) const					{ return VECTOR4(-x, -y, -z, -w); }
			VECTOR4 operator+(const VECTOR4 &vTemp) const	{ return VECTOR4(x+vTemp.x, y+vTemp.y, z+vTemp.z, w+vTemp.w); }
			VECTOR4 operator-(const VECTOR4 &vTemp) const	{ return VECTOR4(x-vTemp.x, y-vTemp.y, z-vTemp.z, w-vTemp.w); }
			VECTOR4 operator*(float fTemp) const { return VECTOR4(x*fTemp, y*fTemp, z*fTemp, w*fTemp); }
			VECTOR4 operator/(float fTemp) const { 
				fTemp = 1.0f / fTemp;
				return VECTOR4(x*fTemp, y*fTemp, z*fTemp, w*fTemp);
			}
		};

		// ----------------------------------------------------------------------------------------
		/**
		 *	加算
		 */
		inline void Vec4Add(VECTOR4 *pOut, const VECTOR4 *pV1, const VECTOR4 *pV2) {
			pOut->x = pV1->x + pV2->x;
			pOut->y = pV1->y + pV2->y;
			pOut->z = pV1->z + pV2->z;
			pOut->w = pV1->w + pV2->w;
		}

		// ----------------------------------------------------------------------------------------
		/**
		 *	減算
		 */
		inline void Vec4Subtract(VECTOR4 *pOut, const VECTOR4 *pV1, const VECTOR4 *pV2) {
			pOut->x = pV1->x - pV2->x;
			pOut->y = pV1->y - pV2->y;
			pOut->z = pV1->z - pV2->z;
			pOut->w = pV1->w - pV2->w;
		}

		// ----------------------------------------------------------------------------------------
		/**
		 *	倍化
		 */
		inline void Vec4Scale(VECTOR4 *pOut, const VECTOR4 *pV, const float s) {
			pOut->x = pV->x * s;
			pOut->y = pV->y * s;
			pOut->z = pV->z * s;
			pOut->w = pV->w * s;
		}

		// ----------------------------------------------------------------------------------------
		/**
		 *	倍化
		 */
		inline void Vec4Scale(VECTOR4 *pOut, const VECTOR4 *pV1, const VECTOR4 *pV2) {
			pOut->x = pV1->x * pV2->x;
			pOut->y = pV1->y * pV2->y;
			pOut->z = pV1->z * pV2->z;
			pOut->w = pV1->w * pV2->w;
		}

		// ----------------------------------------------------------------------------------------
		/**
		 *	長さ（２乗）
		 */
		inline float Vec4LengthSq(const VECTOR4 *pV) {
			return (pV->x * pV->x + pV->y * pV->y + pV->z * pV->z + pV->w * pV->w);
		}

		// ----------------------------------------------------------------------------------------
		/**
		 *	長さ
		 */
		inline float Vec4Length(const VECTOR4 *pV) {
			return sqrtf(pV->x * pV->x + pV->y * pV->y + pV->z * pV->z + pV->w * pV->w);
		}

		// ----------------------------------------------------------------------------------------
		/**
		 *	正規化
		 */
		inline void Vec4Normalize(VECTOR4 *pOut, const VECTOR4 *pV) {
			register float fTemp = Vec4Length(pV);
			fTemp = (fTemp == 0.0f) ? 1.0f : (1.0f / fTemp);
			pOut->x = pV->x * fTemp;
			pOut->y = pV->y * fTemp;
			pOut->z = pV->z * fTemp;
			pOut->w = pV->w * fTemp;
		}

		// ----------------------------------------------------------------------------------------
		/**
		 *	外積
		 */
		inline void Vec4Cross(VECTOR4 *pOut, const VECTOR4 *pV1, const VECTOR4 *pV2) {
			VECTOR4 vTemp;
			vTemp.x = pV1->y * pV2->z - pV1->z * pV2->y;
			vTemp.y = pV1->z * pV2->x - pV1->x * pV2->z;
			vTemp.z = pV1->x * pV2->y - pV1->y * pV2->x;
			vTemp.w = 0.0f;
			*pOut = vTemp;
		}


		// ----------------------------------------------------------------------------------------
		/**
		 *	内積
		 */
		inline float Vec4Dot(const VECTOR4 *pV1, const VECTOR4 *pV2) {
			return (pV1->x * pV2->x + pV1->y * pV2->y + pV1->z * pV2->z + pV1->w * pV2->w);
		}


		// ----------------------------------------------------------------------------------------
		/**
		 *	長さ（２乗）
		 */
		inline float Vec4DistanceSq(const VECTOR4 *pV1,const VECTOR4 *pV2) {
			VECTOR4 V;
			Vec4Subtract( &V, pV1, pV2 );
			return Vec4LengthSq(&V);
		}


		// ----------------------------------------------------------------------------------------
		/**
		 *	長さ
		 */
		inline float Vec4Distance(const VECTOR4 *pV1,const VECTOR4 *pV2) {
			VECTOR4 V;
			Vec4Subtract( &V, pV1, pV2 );
			return Vec4Length(&V);
		}

		// ----------------------------------------------------------------------------------------
		/**
		 *	線形補完
		 */
		inline void Vec4Lerp(VECTOR4 *pOut, const VECTOR4 *pV1, const VECTOR4 *pV2, const float s) {
			register float f0, f1;
			f0 = 1.0f - s;
			f1 = s;
			pOut->x = pV1->x * f0 + pV2->x * f1;
			pOut->y = pV1->y * f0 + pV2->y * f1;
			pOut->z = pV1->z * f0 + pV2->z * f1;
			pOut->w = pV1->w * f0 + pV2->w * f1;
		}

		// ----------------------------------------------------------------------------------------
		/**
		 *	カトマルロム曲線
		 */
		inline void Vec4CatmullRom(VECTOR4 *pOut, const VECTOR4 *pV1, const VECTOR4 *pV2, const VECTOR4 *pV3, const VECTOR4 *pV4, const float s) {
			float t2, t3, h1, h2, h3, h4;
			VECTOR4 r1,r2;

			t2 = s * s;
			t3 = t2 * s;
			h2 = 3.0f * t2 - t3 - t3;
			h1 = 1.0f - h2;
			h4 = t3 - t2;
			h3 = h4 - t2 + s;
			r1 = (*pV3 - *pV1) * 0.5f;
			r2 = (*pV4 - *pV2) * 0.5f;
			*pOut = *pV2 * h1 + *pV3 * h2 + r1 * h3 + r2 * h4;
		}


		struct VECTOR3
		{
		public:
			float	x, y, z;

		public:
			VECTOR3() {}
			VECTOR3(float xx, float yy, float zz): x(xx), y(yy), z(zz) {}
			VECTOR3(float v): x(v), y(v), z(v) {}

			inline void Clear(void) { x = 0.f; y = 0.f; z = 0.f; }
			inline void Set(float xx, float yy, float zz) { x = xx; y = yy; z = zz; }

			operator float*(void) { return &x; }

			VECTOR3 &operator+=(const VECTOR3 &vTemp) {
				x += vTemp.x;
				y += vTemp.y;
				z += vTemp.z;
				return *this;
			}
			VECTOR3 &operator-=(const VECTOR3 &vTemp) {
				x -= vTemp.x;
				y -= vTemp.y;
				z -= vTemp.z;
				return *this;
			}
			VECTOR3 &operator*=(float fTemp) {
				x *= fTemp;
				y *= fTemp;
				z *= fTemp;
				return *this;
			}
			VECTOR3 &operator/=(float fTemp) {
				fTemp = 1.0f / fTemp;
				x *= fTemp;
				y *= fTemp;
				z *= fTemp;
				return *this;
			}

			bool operator!=(const VECTOR3 &vTemp) { return ((x != vTemp.x) || (y != vTemp.y) || (z != vTemp.z)); }
			bool operator==(const VECTOR3 &vTemp) { return !(*this != vTemp); }

			float &operator[](int i) { return ((&x)[i]); }

			VECTOR3 operator+(void) const { return VECTOR3(+x, +y, +z); }
			VECTOR3 operator-(void) const { return VECTOR3(-x, -y, -z); }
			VECTOR3 operator+(const VECTOR3 &vTemp) const { return VECTOR3(x+vTemp.x, y+vTemp.y, z+vTemp.z); }
			VECTOR3 operator-(const VECTOR3 &vTemp) const { return VECTOR3(x-vTemp.x, y-vTemp.y, z-vTemp.z); }
			VECTOR3 operator*(float fTemp) const { return VECTOR3(x*fTemp, y*fTemp, z*fTemp); }
			VECTOR3 operator/(float fTemp) const {
				fTemp = 1.0f / fTemp;
				return VECTOR3(x*fTemp, y*fTemp, z*fTemp);
			}

		};


		// ----------------------------------------------------------------------------------------
		/**
		 *	加算
		 */
		inline void Vec3Add(VECTOR3 *pOut, const VECTOR3 *pV1, const VECTOR3 *pV2) {
			pOut->x = pV1->x + pV2->x;
			pOut->y = pV1->y + pV2->y;
			pOut->z = pV1->z + pV2->z;
		}

		// ----------------------------------------------------------------------------------------
		/** 
		 *	減算
		 */
		inline void Vec3Subtract(VECTOR3 *pOut, const VECTOR3 *pV1, const VECTOR3 *pV2) {
			pOut->x = pV1->x - pV2->x;
			pOut->y = pV1->y - pV2->y;
			pOut->z = pV1->z - pV2->z;
		}

		// ----------------------------------------------------------------------------------------
		/**
		 *	倍化
		 */
		inline void Vec3Scale(VECTOR3 *pOut, const VECTOR3 *pV, const float s) {
			pOut->x = pV->x * s;
			pOut->y = pV->y * s;
			pOut->z = pV->z * s;
		}

		// ----------------------------------------------------------------------------------------
		/**
		 *	倍化
		 */
		inline void Vec3Scale(VECTOR3 *pOut, const VECTOR3 *pV1, const VECTOR3 *pV2) {
			pOut->x = pV1->x * pV2->x;
			pOut->y = pV1->y * pV2->y;
			pOut->z = pV1->z * pV2->z;
		}

		// ----------------------------------------------------------------------------------------
		/**
		 *	長さ（２乗）
		 */
		inline float Vec3LengthSq(const VECTOR3 *pV) {
			return (pV->x * pV->x + pV->y * pV->y + pV->z * pV->z);
		}

		// ----------------------------------------------------------------------------------------
		/**
		 *	長さ
		 */
		inline float Vec3Length(const VECTOR3 *pV) {
			return sqrtf(pV->x * pV->x + pV->y * pV->y + pV->z * pV->z);
		}

		// ----------------------------------------------------------------------------------------
		/**
		 *	正規化
		 */
		inline void Vec3Normalize(VECTOR3 *pOut, const VECTOR3 *pV) {
			register float fTemp = Vec3Length(pV);
			fTemp = (fTemp == 0.0f) ? 1.0f : (1.0f / fTemp);
			pOut->x = pV->x * fTemp;
			pOut->y = pV->y * fTemp;
			pOut->z = pV->z * fTemp;
		}

		// ----------------------------------------------------------------------------------------
		/**
		 *	外積
		 */
		inline void Vec3Cross(VECTOR3 *pOut, const VECTOR3 *pV1, const VECTOR3 *pV2) {
			VECTOR3 vTemp;
			vTemp.x = pV1->y * pV2->z - pV1->z * pV2->y;
			vTemp.y = pV1->z * pV2->x - pV1->x * pV2->z;
			vTemp.z = pV1->x * pV2->y - pV1->y * pV2->x;
			*pOut = vTemp;
		}

		// ----------------------------------------------------------------------------------------
		/**
		 *	内積
		 */
		inline float Vec3Dot(const VECTOR3 *pV1, const VECTOR3 *pV2) {
			return (pV1->x * pV2->x + pV1->y * pV2->y + pV1->z * pV2->z);
		}


		// ----------------------------------------------------------------------------------------
		/**
		 *	長さ（２乗）
		 */
		inline float Vec3DistanceSq(const VECTOR3 *pV1,const VECTOR3 *pV2) {
			VECTOR3 V;
			Vec3Subtract( &V, pV1, pV2 );
			return Vec3LengthSq(&V);
		}

		// ----------------------------------------------------------------------------------------
		/**
		 *	長さ
		 */
		inline float Vec3Distance(const VECTOR3 *pV1,const VECTOR3 *pV2) {
			VECTOR3 V;
			Vec3Subtract( &V, pV1, pV2 );
			return Vec3Length(&V);
		}

		// ----------------------------------------------------------------------------------------
		/**
		 *	線形補完(Linear interpolation)
		 */
		inline void Vec3Lerp(VECTOR3 *pOut, const VECTOR3 *pV1, const VECTOR3 *pV2, const float s) {
			register float f0, f1;
			f0 = 1.0f - s;
			f1 = s;
			pOut->x = pV1->x * f0 + pV2->x * f1;
			pOut->y = pV1->y * f0 + pV2->y * f1;
			pOut->z = pV1->z * f0 + pV2->z * f1;
		}





		struct VECTOR2
		{
		public:
			float	x, y;

		public:
			VECTOR2() {}
			VECTOR2(float xx, float yy) : x(xx), y(yy) {}

			inline void Clear(void) { x = 0.f; y = 0.f; }
			inline void Set(float xx, float yy, float zz) { x = xx; y = yy; }

			operator float*(void) { return &x; }

			VECTOR2 &operator+=(const VECTOR2 &vTemp) {
				x += vTemp.x;
				y += vTemp.y;
				return *this;
			}
			VECTOR2 &operator-=(const VECTOR2 &vTemp) {
				x -= vTemp.x;
				y -= vTemp.y;
				return *this;
			}
			VECTOR2 &operator*=(float fTemp) {
				x *= fTemp;
				y *= fTemp;
				return *this;
			}
			VECTOR2 &operator/=(float fTemp) {
				fTemp = 1.0f / fTemp;
				x *= fTemp;
				y *= fTemp;
				return *this;
			}

			bool operator!=(const VECTOR2& vTemp) { return ((x != vTemp.x) || (y != vTemp.y)); }
			bool operator==(const VECTOR2& vTemp) { return !(*this != vTemp); }

			float &operator[](int i) { return ((&x)[i]); }

			VECTOR2 operator+(void) const { return VECTOR2(+x, +y); }
			VECTOR2 operator-(void) const { return VECTOR2(-x, -y); }

			VECTOR2 operator+(const VECTOR2 &vTemp) const { return VECTOR2(x + vTemp.x, y + vTemp.y); }

			VECTOR2 operator-(const VECTOR2 &vTemp) const { return VECTOR2(x - vTemp.x, y - vTemp.y); }
			VECTOR2 operator*(float fTemp) const { return VECTOR2(x*fTemp, y*fTemp); }
			VECTOR2 operator/(float fTemp) const {
				fTemp = 1.0f / fTemp;
				return VECTOR2(x*fTemp, y*fTemp);
			}

		};


		// ----------------------------------------------------------------------------------------
		/**
		*	加算
		*/
		inline void Vec2Add(VECTOR2 *pOut, const VECTOR2 *pV1, const VECTOR2 *pV2) {
			pOut->x = pV1->x + pV2->x;
			pOut->y = pV1->y + pV2->y;
		}

		// ----------------------------------------------------------------------------------------
		/**
		*	減算
		*/
		inline void Vec2Subtract(VECTOR2 *pOut, const VECTOR2 *pV1, const VECTOR2 *pV2) {
			pOut->x = pV1->x - pV2->x;
			pOut->y = pV1->y - pV2->y;
		}

		// ----------------------------------------------------------------------------------------
		/**
		*	倍化
		*/
		inline void Vec2Scale(VECTOR2 *pOut, const VECTOR2 *pV, const float s) {
			pOut->x = pV->x * s;
			pOut->y = pV->y * s;
		}

		// ----------------------------------------------------------------------------------------
		/**
		*	倍化
		*/
		inline void Vec2Scale(VECTOR2 *pOut, const VECTOR2 *pV1, const VECTOR2 *pV2) {
			pOut->x = pV1->x * pV2->x;
			pOut->y = pV1->y * pV2->y;
		}

		// ----------------------------------------------------------------------------------------
		/**
		*	長さ（２乗）
		*/
		inline float Vec2LengthSq(const VECTOR2 *pV) {
			return (pV->x * pV->x + pV->y * pV->y);
		}

		// ----------------------------------------------------------------------------------------
		/**
		*	長さ
		*/
		inline float Vec2Length(const VECTOR2 *pV) {
			return sqrtf(pV->x * pV->x + pV->y * pV->y);
		}

		// ----------------------------------------------------------------------------------------
		/**
		*	正規化
		*/
		inline void Vec2Normalize(VECTOR2 *pOut, const VECTOR2 *pV) {
			register float fTemp = Vec2Length(pV);
			fTemp = (fTemp == 0.0f) ? 1.0f : (1.0f / fTemp);
			pOut->x = pV->x * fTemp;
			pOut->y = pV->y * fTemp;
		}

		// ----------------------------------------------------------------------------------------
		/**
		*	内積
		*/
		inline float Vec2Dot(const VECTOR2 *pV1, const VECTOR2 *pV2) {
			return (pV1->x * pV2->x + pV1->y * pV2->y);
		}


		// ----------------------------------------------------------------------------------------
		/**
		*	長さ（２乗）
		*/
		inline float Vec2DistanceSq(const VECTOR2 *pV1, const VECTOR2 *pV2) {
			VECTOR2 V;
			Vec2Subtract(&V, pV1, pV2);
			return Vec2LengthSq(&V);
		}

		// ----------------------------------------------------------------------------------------
		/**
		*	長さ
		*/
		inline float Vec2Distance(const VECTOR2 *pV1, const VECTOR2 *pV2) {
			VECTOR2 V;
			Vec2Subtract(&V, pV1, pV2);
			return Vec2Length(&V);
		}

		// ----------------------------------------------------------------------------------------
		/**
		*	線形補完(Linear interpolation)
		*/
		inline void Vec2Lerp(VECTOR2 *pOut, const VECTOR2 *pV1, const VECTOR2 *pV2, const float s) {
			register float f0, f1;
			f0 = 1.0f - s;
			f1 = s;
			pOut->x = pV1->x * f0 + pV2->x * f1;
			pOut->y = pV1->y * f0 + pV2->y * f1;
		}


		// ----------------------------------------------------------------------------------------
		/**
		 *	四元数積
		 */
		inline void QuaternionMultiply(VECTOR4 *pOut, const VECTOR4 *pQ1, const VECTOR4 *pQ2) {
			VECTOR4 vTemp;
			vTemp.x = pQ1->w * pQ2->x + pQ1->x * pQ2->w + pQ1->y * pQ2->z - pQ1->z * pQ2->y;
			vTemp.y = pQ1->w * pQ2->y + pQ1->y * pQ2->w + pQ1->z * pQ2->x - pQ1->x * pQ2->z;
			vTemp.z = pQ1->w * pQ2->z + pQ1->z * pQ2->w + pQ1->x * pQ2->y - pQ1->y * pQ2->x;
			vTemp.w = pQ1->w * pQ2->w - pQ1->x * pQ2->x - pQ1->y * pQ2->y - pQ1->z * pQ2->z;
			*pOut = vTemp;
		}

		struct MATRIX {
		public:
			float	m[4][4];

		public:
			MATRIX() {};
			MATRIX(float m00, float m01, float m02, float m03,
					  float m10, float m11, float m12, float m13,
					  float m20, float m21, float m22, float m23,
					  float m30, float m31, float m32, float m33)
			{
					  m[0][0] = (m00); m[0][1] = (m01); m[0][2] = (m02); m[0][3] = (m03);
					  m[1][0] = (m10); m[1][1] = (m11); m[1][2] = (m12); m[1][3] = (m13);
					  m[2][0] = (m20); m[2][1] = (m21); m[2][2] = (m22); m[2][3] = (m23);
					  m[3][0] = (m30); m[3][1] = (m31); m[3][2] = (m32); m[3][3] = (m33);
			}

			operator float*(void) { return m[0]; }
		};


		// ----------------------------------------------------------------------------------------
		/**
		 *	行列初期化
		 */
		inline void MatrixIdentity(MATRIX *pOut) {
			pOut->m[0][0] = 1.0f; pOut->m[0][1] = 0.0f; pOut->m[0][2] = 0.0f; pOut->m[0][3] = 0.0f;
			pOut->m[1][0] = 0.0f; pOut->m[1][1] = 1.0f; pOut->m[1][2] = 0.0f; pOut->m[1][3] = 0.0f;
			pOut->m[2][0] = 0.0f; pOut->m[2][1] = 0.0f; pOut->m[2][2] = 1.0f; pOut->m[2][3] = 0.0f;
			pOut->m[3][0] = 0.0f; pOut->m[3][1] = 0.0f; pOut->m[3][2] = 0.0f; pOut->m[3][3] = 1.0f;
		}

		// ----------------------------------------------------------------------------------------
		/**
		 *	積
		 */
		inline void MatrixMultiply(MATRIX *pOut, const MATRIX *pM1, const MATRIX *pM2) {
			MATRIX mTemp;
			for (int i=0; i<4; i++) {
				for (int j=0; j<4; j++) {
					mTemp.m[i][j] =
						pM1->m[i][0] * pM2->m[0][j] +
						pM1->m[i][1] * pM2->m[1][j] +
						pM1->m[i][2] * pM2->m[2][j] +
						pM1->m[i][3] * pM2->m[3][j];
				}
			}
			*pOut = mTemp;
		}


		// ----------------------------------------------------------------------------------------
		/**
		 *	転置
		 */
		inline void MatrixTranspose(MATRIX *pOut, const MATRIX *pM) {
			MATRIX mTemp;
			for (register int i=0; i<4; i++) {
				for (register int j=0; j<4; j++) {
					mTemp.m[i][j] = pM->m[j][i];
				}
			}
			*pOut = mTemp;
		}

		// ----------------------------------------------------------------------------------------
		/**
		 *	拡縮行列設定
		 */
		inline void MatrixScale(MATRIX *pOut, const float sx, const float sy, const float sz) {
			pOut->m[0][0] =	  sx; pOut->m[0][1] = 0.0f; pOut->m[0][2] = 0.0f; pOut->m[0][3] = 0.0f;
			pOut->m[1][0] = 0.0f; pOut->m[1][1] =	sy; pOut->m[1][2] = 0.0f; pOut->m[1][3] = 0.0f;
			pOut->m[2][0] = 0.0f; pOut->m[2][1] = 0.0f; pOut->m[2][2] =	  sz; pOut->m[2][3] = 0.0f;
			pOut->m[3][0] = 0.0f; pOut->m[3][1] = 0.0f; pOut->m[3][2] = 0.0f; pOut->m[3][3] = 1.0f;
		}

		// ----------------------------------------------------------------------------------------
		/**
		 *	平行移動行列設定
		 */
		inline void MatrixTranslation(MATRIX *pOut, const float x, const float y, const float z) {
			pOut->m[0][0] = 1.0f; pOut->m[0][1] = 0.0f; pOut->m[0][2] = 0.0f; pOut->m[0][3] = 0.0f;
			pOut->m[1][0] = 0.0f; pOut->m[1][1] = 1.0f; pOut->m[1][2] = 0.0f; pOut->m[1][3] = 0.0f;
			pOut->m[2][0] = 0.0f; pOut->m[2][1] = 0.0f; pOut->m[2][2] = 1.0f; pOut->m[2][3] = 0.0f;
			pOut->m[3][0] =	   x; pOut->m[3][1] =	 y; pOut->m[3][2] =	   z; pOut->m[3][3] = 1.0f;
		}

		// ----------------------------------------------------------------------------------------
		/**
		 *	回転行列（X軸周り）設定
		 */
		inline void MatrixRotationX(MATRIX *pOut, const float Angle) {
			register float fSin, fCos;
			fSin = sinf(Angle);
			fCos = cosf(Angle);
			pOut->m[0][0] = 1.0f; pOut->m[0][1] = 0.0f; pOut->m[0][2] = 0.0f; pOut->m[0][3] = 0.0f;
			pOut->m[1][0] = 0.0f; pOut->m[1][1] = fCos; pOut->m[1][2] = fSin; pOut->m[1][3] = 0.0f;
			pOut->m[2][0] = 0.0f; pOut->m[2][1] =-fSin; pOut->m[2][2] = fCos; pOut->m[2][3] = 0.0f;
			pOut->m[3][0] = 0.0f; pOut->m[3][1] = 0.0f; pOut->m[3][2] = 0.0f; pOut->m[3][3] = 1.0f;
		}

		// ----------------------------------------------------------------------------------------
		/**
		 *	回転行列（Y軸周り）設定
		 */
		inline void MatrixRotationY(MATRIX *pOut, const float Angle) {
			register float fSin, fCos;
			fSin = sinf(Angle);
			fCos = cosf(Angle);
			pOut->m[0][0] = fCos; pOut->m[0][1] = 0.0f; pOut->m[0][2] =-fSin; pOut->m[0][3] = 0.0f;
			pOut->m[1][0] = 0.0f; pOut->m[1][1] = 1.0f; pOut->m[1][2] = 0.0f; pOut->m[1][3] = 0.0f;
			pOut->m[2][0] = fSin; pOut->m[2][1] = 0.0f; pOut->m[2][2] = fCos; pOut->m[2][3] = 0.0f;
			pOut->m[3][0] = 0.0f; pOut->m[3][1] = 0.0f; pOut->m[3][2] = 0.0f; pOut->m[3][3] = 1.0f;
		}

		// ----------------------------------------------------------------------------------------
		/**
		 *	回転行列（Y軸周り）設定
		 */
		inline void MatrixRotationZ(MATRIX *pOut, const float Angle) {
			register float fSin, fCos;
			fSin = sinf(Angle);
			fCos = cosf(Angle);
			pOut->m[0][0] = fCos; pOut->m[0][1] = fSin; pOut->m[0][2] = 0.0f; pOut->m[0][3] = 0.0f;
			pOut->m[1][0] =-fSin; pOut->m[1][1] = fCos; pOut->m[1][2] = 0.0f; pOut->m[1][3] = 0.0f;
			pOut->m[2][0] = 0.0f; pOut->m[2][1] = 0.0f; pOut->m[2][2] = 1.0f; pOut->m[2][3] = 0.0f;
			pOut->m[3][0] = 0.0f; pOut->m[3][1] = 0.0f; pOut->m[3][2] = 0.0f; pOut->m[3][3] = 1.0f;
		}

		// ----------------------------------------------------------------------------------------
		/**
		 *	回転行列（三軸の合成　Z->X->Y）設定
		 */
		inline void MatrixRotationZXY(MATRIX *pOut, const float fz, const float fx, const float fy) {
			MATRIX mRotX, mRotY, mRotZ;
			MatrixRotationZ(&mRotZ, fz);
			MatrixRotationX(&mRotX, fx);
			MatrixRotationY(&mRotY, fy);

			MatrixMultiply(pOut, &mRotZ, &mRotX);
			MatrixMultiply(pOut, pOut, &mRotY);
		}

		// ----------------------------------------------------------------------------------------
		/**
		 *	回転行列（三軸の合成　Y->X->Z）設定
		 */
		inline void MatrixRotationYXZ(MATRIX *pOut, const float fy, const float fx, const float fz) {
			MATRIX mRotX, mRotY, mRotZ;
			MatrixRotationY(&mRotY, fy);
			MatrixRotationX(&mRotX, fx);
			MatrixRotationZ(&mRotZ, fz);

			MatrixMultiply(pOut, &mRotY, &mRotX);
			MatrixMultiply(pOut, pOut, &mRotZ);
		}

		// ----------------------------------------------------------------------------------------
		/**
		 *	逆行列
		 */
		inline void MatrixInverse(MATRIX *pOut, const MATRIX *pM) {
			MATRIX mTemp;
			//	擬似逆行列（回転／移動であれば使用可）
			mTemp.m[0][0] = pM->m[0][0]; mTemp.m[0][1] = pM->m[1][0]; mTemp.m[0][2] = pM->m[2][0]; mTemp.m[0][3] = 0.0f;
			mTemp.m[1][0] = pM->m[0][1]; mTemp.m[1][1] = pM->m[1][1]; mTemp.m[1][2] = pM->m[2][1]; mTemp.m[1][3] = 0.0f;
			mTemp.m[2][0] = pM->m[0][2]; mTemp.m[2][1] = pM->m[1][2]; mTemp.m[2][2] = pM->m[2][2]; mTemp.m[2][3] = 0.0f;
			mTemp.m[3][0] = -(pM->m[0][0] * pM->m[3][0] + pM->m[0][1] * pM->m[3][1] + pM->m[0][2] * pM->m[3][2]);
			mTemp.m[3][1] = -(pM->m[1][0] * pM->m[3][0] + pM->m[1][1] * pM->m[3][1] + pM->m[1][2] * pM->m[3][2]);
			mTemp.m[3][2] = -(pM->m[2][0] * pM->m[3][0] + pM->m[2][1] * pM->m[3][1] + pM->m[2][2] * pM->m[3][2]);
			mTemp.m[3][3] = 1.0f;
			*pOut = mTemp;
		}



		// ----------------------------------------------------------------------------------------
		/**
		 *	行列で四元数を回転させる
		 */
		inline void QuaternionRotationMatrix(VECTOR4 *pOut, const MATRIX *pM) {
			register int i;
			register float tr, sq, mx;

			i = 0;
			tr = pM->m[0][0] + pM->m[1][1] + pM->m[2][2];

			if (tr > 0.0f) {
				sq		= sqrtf(tr + 1.0f);
				pOut->w = 0.5f * sq;
				sq		= 0.5f / sq;
				pOut->x = (pM->m[1][2] - pM->m[2][1]) * sq;
				pOut->y = (pM->m[2][0] - pM->m[0][2]) * sq;
				pOut->z = (pM->m[0][1] - pM->m[1][0]) * sq;

			} else {
				mx = pM->m[0][0];
				if (pM->m[1][1] > mx) { i = 1; mx = pM->m[1][1]; }
				if (pM->m[2][2] > mx) { i = 2; }
				switch (i) {
				case 0:
					sq		= sqrtf(pM->m[0][0] - pM->m[1][1] - pM->m[2][2] + 1.0f);
					pOut->x = 0.5f * sq;
					sq		= 0.5f / sq;
					pOut->y = (pM->m[1][0] + pM->m[0][1]) * sq;
					pOut->z = (pM->m[0][2] + pM->m[2][0]) * sq;
					pOut->w = (pM->m[1][2] - pM->m[2][1]) * sq;
					break;
				case 1:
					sq		= sqrtf(pM->m[1][1] - pM->m[0][0] - pM->m[2][2] + 1.0f);
					pOut->y = 0.5f * sq;
					sq		= 0.5f / sq;
					pOut->z = (pM->m[2][1] + pM->m[1][2]) * sq;
					pOut->x = (pM->m[1][0] + pM->m[0][1]) * sq;
					pOut->w = (pM->m[2][0] - pM->m[0][2]) * sq;
					break;
				case 2:
					sq		= sqrtf(pM->m[2][2] - pM->m[0][0] - pM->m[1][1] + 1.0f);
					pOut->z = 0.5f * sq;
					sq		= 0.5f / sq;
					pOut->x = (pM->m[0][2] + pM->m[2][0]) * sq;
					pOut->y = (pM->m[2][1] + pM->m[1][2]) * sq;
					pOut->w = (pM->m[0][1] - pM->m[1][0]) * sq;
					break;
				}
			}
		}

		// ----------------------------------------------------------------------------------------
		/**
		 *	軸指定で四元数を回転させる
		 */
		inline void RotationQuaternion(VECTOR4* pQuat, const VECTOR4* pAxis, const float fRot) {
			VECTOR4 vResultQuat(0.0f,0.0f,0.0f,0.0f);

			float fCos = cosf((fRot * 0.5f));
			float fSin = sinf((fRot * 0.5f));
	
			vResultQuat = *pAxis;
	
			Vec4Scale(&vResultQuat, &vResultQuat, fSin);
			vResultQuat.w = fCos;

			*pQuat = vResultQuat;
		}

		// ----------------------------------------------------------------------------------------
		/**
		 *	三軸角度で四元数を回転させる
		 */
		inline void QuaternionRotationYawPitchRoll(VECTOR4 *pOut, const float Yaw, const float Pitch, const float Roll) {
			register float fSinYaw	= sinf(Yaw * 0.5f);
			register float fSinPitch = sinf(Pitch * 0.5f);
			register float fSinRoll	= sinf(Roll * 0.5f);
			register float fCosYaw	= cosf(Yaw * 0.5f);
			register float fCosPitch = cosf(Pitch * 0.5f);
			register float fCosRoll	= cosf(Roll * 0.5f);

			pOut->x = fCosRoll * fSinPitch * fCosYaw - fSinRoll * fCosPitch * fSinYaw;
			pOut->y = fCosRoll * fCosPitch * fSinYaw + fSinRoll * fSinPitch * fCosYaw;
			pOut->z = fSinRoll * fCosPitch * fCosYaw + fCosRoll * fSinPitch * fSinYaw;
			pOut->w = fCosRoll * fCosPitch * fCosYaw - fSinRoll * fSinPitch * fSinYaw;
		}

		// ----------------------------------------------------------------------------------------
		/**
		 *	線形補完	
		 */
		inline void QuaternionSlerp(VECTOR4 *pOut, const VECTOR4 *pQ1, const VECTOR4 *pQ2, const float t) {
			float c = pQ1->x * pQ2->x + pQ1->y * pQ2->y + pQ1->z * pQ2->z + pQ1->w * pQ2->w;
			float angle;
			float s;
			float r1, r2;

			int reverse = 0;
			if ( c < 0 ) {
				c = - c;
				reverse = 1;
			}

			angle = acosf( c < -1.f ? -1.f : ( c > 1.f ? 1.f : c ) );
			s = sinf( angle );
			if ( fabsf(s) <= (1.192092896e-07f * 4.0f) ) {
				memcpy(pOut, pQ2, sizeof(VECTOR4));
				return;
			}
			r1 = sinf( ( 1.0f - t ) * angle ) / s;
			r2 = sinf( t * angle ) / s;
			if ( reverse ) {
				r2 = - r2;
			}

			pOut->x = pQ1->x * r1 + pQ2->x * r2;
			pOut->y = pQ1->y * r1 + pQ2->y * r2;
			pOut->z = pQ1->z * r1 + pQ2->z * r2;
			pOut->w = pQ1->w * r1 + pQ2->w * r2;
		}


		// ----------------------------------------------------------------------------------------
		/**
		 *	正規化
		 */
		inline void QuaternionNormalize(VECTOR4 *pOut, const VECTOR4 *pQ) {
			float x, y, z, w, q;
			x = pQ->x;
			y = pQ->y;
			z = pQ->z;
			w = pQ->w;
			q = 1 / sqrtf(x*x + y*y + z*z + w*w);
			pOut->x = x * q;
			pOut->y = y * q;
			pOut->z = z * q;
			pOut->w = w * q;
		}

		// ----------------------------------------------------------------------------------------
		/**
		 *	四元数を行列へ変換
		 */
		inline void MakeMatrixFromQuaternion(MATRIX *pOut, const VECTOR4 *pQ) {
			register float xx, yy, zz;
			register float xy, xz, yz;
			register float wx, wy, wz;

			xx = pQ->x * pQ->x; yy = pQ->y * pQ->y; zz = pQ->z * pQ->z;
			xy = pQ->x * pQ->y; xz = pQ->x * pQ->z; yz = pQ->y * pQ->z;
			wx = pQ->w * pQ->x; wy = pQ->w * pQ->y; wz = pQ->w * pQ->z;

			pOut->m[0][0] = 1.0f - (yy + zz) * 2.0f;
			pOut->m[1][0] = 2.0f * (xy - wz);
			pOut->m[2][0] = 2.0f * (xz + wy);
			pOut->m[3][0] = 0.0f;

			pOut->m[0][1] = 2.0f * (xy + wz);
			pOut->m[1][1] = 1.0f - (xx + zz) * 2.0f;
			pOut->m[2][1] = 2.0f * (yz - wx);
			pOut->m[3][1] = 0.0f;

			pOut->m[0][2] = 2.0f * (xz - wy);
			pOut->m[1][2] = 2.0f * (yz + wx);
			pOut->m[2][2] = 1.0f - (xx + yy) * 2.0f;
			pOut->m[3][2] = 0.0f;

			pOut->m[0][3] = 0.0f;
			pOut->m[1][3] = 0.0f;
			pOut->m[2][3] = 0.0f;
			pOut->m[3][3] = 1.0f;
		}


		// ----------------------------------------------------------------------------------------
		/**
		 *	オイラー角／ラジアン変換
		 */
		#define	_PI								((float) 3.141592654f)
		#define	ToRadian(deg)					((deg) * (_PI / 180.0f))
		#define	ToDegree(rad)					((rad) * (180.0f / _PI))

		// ----------------------------------------------------------------------------------------
		/**
		 *	丸め演算
		 */
		#define	_PI2								(_PI * 2.0f)
		#define	_PIH								(_PI * 0.5f)
		#define	_PIQ								(_PIH * 0.5f)

		inline float rLim(float fPi)
		{
			while (fPi >   _PI) fPi -= _PI2;
			while (fPi <= -_PI) fPi += _PI2;
			return fPi;
		}

		inline void GetAngleDirectionXY( VECTOR4 *angle, const VECTOR4 *direction ) {
			VECTOR4 n;
			Vec4Normalize( &n, direction );
			angle->x = asinf( n.y );
			if( n.z == 0.0f ){
				if( n.x >= 0.0f )	angle->y = _PI / 2.0f;
				else				angle->y = -_PI / 2.0f;
			}else{
				angle->y = atan2f( n.x, n.z );
			}
		}


		// ----------------------------------------------------------------------------------------
		/**
		 *
		 */
		inline void Vec4Transform(VECTOR4 *pOut, const VECTOR4 *pV, const MATRIX *pM) {
			VECTOR4 vTemp = *pV;
			pOut->x = vTemp.x * pM->m[0][0] + vTemp.y * pM->m[1][0] + vTemp.z * pM->m[2][0] + vTemp.w * pM->m[3][0];
			pOut->y = vTemp.x * pM->m[0][1] + vTemp.y * pM->m[1][1] + vTemp.z * pM->m[2][1] + vTemp.w * pM->m[3][1];
			pOut->z = vTemp.x * pM->m[0][2] + vTemp.y * pM->m[1][2] + vTemp.z * pM->m[2][2] + vTemp.w * pM->m[3][2];
			pOut->w = vTemp.x * pM->m[0][3] + vTemp.y * pM->m[1][3] + vTemp.z * pM->m[2][3] + vTemp.w * pM->m[3][3];
		}

		// ----------------------------------------------------------------------------------------
		/**
		 *
		 */
		inline void Vec4TransformCoord(VECTOR4 *pOut, const VECTOR4 *pV, const MATRIX *pM) {
			VECTOR4 vTemp = VECTOR4(pV->x, pV->y, pV->z, 1.0f);
			Vec4Transform(pOut, &vTemp, pM);
			pOut->w = 0.0f;
		}

		// ----------------------------------------------------------------------------------------
		/**
		 *
		 */
		inline void Vec4TransformNormal(VECTOR4 *pOut, const VECTOR4 *pV, const MATRIX *pM) {
			VECTOR4 vTemp = VECTOR4(pV->x, pV->y, pV->z, 0.0f);
			Vec4Transform(pOut, &vTemp, pM);
			pOut->w = 0.0f;
		}


		// ----------------------------------------------------------------------------------------
		/**
		 *
		 */
		inline void MatrixRotationRollPitchYaw(MATRIX *pMat, const VECTOR4 *pRot, const VECTOR4 *pTrans) {
			if (pRot) {
				MatrixRotationZXY(pMat, pRot->z, pRot->x, pRot->y);
			} else {
				MatrixIdentity(pMat);
			}
			if (pTrans) {
				pMat->m[3][0] = pTrans->x;
				pMat->m[3][1] = pTrans->y;
				pMat->m[3][2] = pTrans->z;
			}
		}

		// ----------------------------------------------------------------------------------------
		/**
		 *
		 */
		inline void MatrixRotationYawPitchRoll(MATRIX *pMat, const VECTOR4 *pRot, const VECTOR4 *pTrans) {
			if (pRot) {
				MatrixRotationYXZ(pMat, pRot->y, pRot->x, pRot->z);
			} else {
				MatrixIdentity(pMat);
			}
			if (pTrans) {
				pMat->m[3][0] = pTrans->x;
				pMat->m[3][1] = pTrans->y;
				pMat->m[3][2] = pTrans->z;
			}
		}

		// ----------------------------------------------------------------------------------------
		/**
		 *
		 */
		inline void MatrixScaling(MATRIX *pMat, const VECTOR4 *pScale) {
			Vec4Scale((VECTOR4 *) pMat->m[0], (VECTOR4 *) pMat->m[0], pScale->x);
			Vec4Scale((VECTOR4 *) pMat->m[1], (VECTOR4 *) pMat->m[1], pScale->y);
			Vec4Scale((VECTOR4 *) pMat->m[2], (VECTOR4 *) pMat->m[2], pScale->z);
		}

		// ----------------------------------------------------------------------------------------
		/**
		 *
		 */
		inline void MatrixNormalize(MATRIX *pOut, const MATRIX *pM) {
			Vec4Normalize((VECTOR4 *) pOut->m[0], (VECTOR4 *) pM->m[0]);
			Vec4Normalize((VECTOR4 *) pOut->m[1], (VECTOR4 *) pM->m[1]);
			Vec4Normalize((VECTOR4 *) pOut->m[2], (VECTOR4 *) pM->m[2]);
		}

		// ----------------------------------------------------------------------------------------
		/**
		 *
		 */
		inline void MakeMatrixFromVec4(MATRIX *pMat, const VECTOR4 *pVec) {
			VECTOR4 TmpVec[4];

			if ((pVec->x == 0.0f) && (pVec->z == 0.0f)) {
				TmpVec[1] = VECTOR4(pVec->x, (pVec->y + 5.0f), (pVec->z + 5.0f), 0.0f);
			} else {
				TmpVec[1] = VECTOR4(pVec->x, (pVec->y + 5.0f), (pVec->z), 0.0f);
			}

			TmpVec[0] = *pVec;
			Vec4Cross(&TmpVec[2], &TmpVec[1], &TmpVec[0]);
			Vec4Cross(&TmpVec[3], &TmpVec[0], &TmpVec[2]);
			Vec4Normalize(&TmpVec[2], &TmpVec[2]);
			Vec4Normalize(&TmpVec[3], &TmpVec[3]);
			MatrixIdentity(pMat);
			memcpy(pMat->m[0], &TmpVec[2], sizeof(VECTOR4));
			memcpy(pMat->m[1], &TmpVec[3], sizeof(VECTOR4));
			memcpy(pMat->m[2], &TmpVec[0], sizeof(VECTOR4));
		}

		// ----------------------------------------------------------------------------------------
		/**
		 *
		 */
		inline void MakeVec4FromMatrixZXY(VECTOR4 *pOut, const MATRIX *pMat) {
			//VECTOR4 vecResult(0.0f, 0.0f, 0.0f, 0.0f);
			//float aang;
			//
			//// 新たな回転マトリックスから角度を求める
			//aang = pMat->m[2][1];
			//if( aang > 1.0f )	aang = 1.0f;
			//else if( aang < -1.0f )	aang = -1.0f;
			//vecResult.x = asinf( aang );							// X
			//
			//vecResult.y = atan2f( pMat->m[2][0], pMat->m[2][2] );		// Y
			//
			//MATRIX matTemp;
			//MATRIX matRotX;
			//MATRIX matRotY;
			//MatrixRotationY(&matRotY, vecResult.y);
			//MatrixRotationX(&matRotX, vecResult.x);
			//MatrixMultiply(&matTemp, pMat, &matRotY);
			//MatrixMultiply(&matTemp, &matTemp, &matRotX);

			//aang = matTemp.m[0][0];
			//if( aang > 1.0f )	aang = 1.0f;
			//else if( aang < -1.0f )	aang = -1.0f;
			//vecResult.z = acosf( aang );
			//if( matTemp.m[0][1] < 0.0f ) vecResult.z *= -1.0f;

			//*pOut = vecResult;

			// Zマイナス方向
			// Z→X→Yで作られた行列を角度へ変換する場合
			VECTOR4 vecResult(0.0f,0.0f,0.0f,0.0f);
			float fAngle;

			MATRIX mWork;
			MatrixIdentity( &mWork );
			mWork = *pMat;

			fAngle = -mWork.m[2][1];
			if(fAngle < -1.0f)	{ fAngle = -1.0f; }
			if(1.0f < fAngle)	{ fAngle = 1.0f; }

			vecResult.x = asinf( fAngle );						// X
			vecResult.z = atan2f( mWork.m[0][1], mWork.m[1][1] );	// Z
			vecResult.y = atan2f( mWork.m[2][0], mWork.m[2][2] );	// Y

			*pOut = vecResult;
		}



		// ----------------------------------------------------------------------------------------
		/**
		 *
		 */
		inline void MatrixMirror(MATRIX *pOut, const MATRIX *pM, const int iAxis, const float fBorder) {
			memcpy(pOut, pM, sizeof(MATRIX));

			pOut->m[0][iAxis]  = -pOut->m[0][iAxis];
			pOut->m[1][iAxis]  = -pOut->m[1][iAxis];
			pOut->m[2][iAxis]  = -pOut->m[2][iAxis];
			pOut->m[3][iAxis] -= fBorder;
			pOut->m[3][iAxis]  = fBorder - pOut->m[3][iAxis];
		}



		// ----------------------------------------------------------------------------------------
		/**
		 *	ビュー行列計算
		 */
		inline void MatrixLookAt( MATRIX *pOut, const VECTOR3 *pCameraPos, const VECTOR3 *pCameraUp, const VECTOR3 *pCameraTarget ) {
			// 視線
			VECTOR3 vLook;
			Vec3Subtract(&vLook, pCameraTarget, pCameraPos);
			Vec3Normalize(&vLook, &vLook);

			// 横方向
			VECTOR3 vRight;
			Vec3Cross(&vRight, pCameraUp, &vLook);
			Vec3Normalize(&vRight, &vRight);

			// 上方向
			VECTOR3 vUp;
			Vec3Cross(&vUp, &vLook, &vRight);

			pOut->m[0][0] = vRight.x;
			pOut->m[0][1] = vUp.x;
			pOut->m[0][2] = vLook.x;
			pOut->m[0][3] = 0.0f;

			pOut->m[1][0] = vRight.y;
			pOut->m[1][1] = vUp.y;
			pOut->m[1][2] = vLook.y;
			pOut->m[1][3] = 0.0f;

			pOut->m[2][0] = vRight.z;
			pOut->m[2][1] = vUp.z;
			pOut->m[2][2] = vLook.z;
			pOut->m[2][3] = 0.0f;

			pOut->m[3][0] = -Vec3Dot(&vRight, pCameraPos);
			pOut->m[3][1] = -Vec3Dot(&vUp, pCameraPos);
			pOut->m[3][2] = -Vec3Dot(&vLook, pCameraPos);
			pOut->m[3][3] = 1.0f;
		}
	}
}

typedef core::math::VECTOR2	vec2; 
typedef core::math::VECTOR3	vec3;
typedef core::math::VECTOR4	vec4;
typedef core::math::MATRIX	matrix;