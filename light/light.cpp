#include "light.h"
#include "../win/window.h"

namespace core {

	namespace light {

		//---------------------------------------------------------------------------------
		/**
		 * @brief	
		 * 平行光源のコンスタントバッファ内容
		 */
		struct ConstantBufferFormat {
			vec4	pos;			///< ライトのワールド座標
			vec4	dir;			///< ライトのワールド空間上の向き
			vec4	color;			///< ライトカラー
			matrix	lightView;		///< ライトビュー行列
			matrix	lightMatrix;	///< ライトビュー行列
		};

		//---------------------------------------------------------------------------------
		/**
		 * @brief	コントラクタ
		 */
		Light::Light()
		: m_dir(1.0f,-1.0f,0) {
			m_position.Set(0,10,10);
			m_constantBuffer.Create(sizeof(ConstantBufferFormat));
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	デストラクタ
		 */
		Light::~Light() {
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	ライト向き設定
		 * @param	dir	ワールド空間上の向き
		 * @return
		 */
		void Light::Dir(const vec3& dir) {
			m_update	|= (m_dir != dir);
			m_dir		= dir;
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	ライトカラー設定
		 * @param	color	ライト色
		 * @return
		 */
		void Light::Color(const vec4& color) {
			m_update	|= (m_color != color);
			m_color		= color;
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	ライトのコンスタントバッファを更新
		 * @param	pContext	描画コンテキスト
		 * @return
		 */
		void Light::UpdateConstantBuffer(ID3D11DeviceContext* pContext) {

			// 必要なときのみコンスタントバッファを更新
			if(m_update) {

				// コンスタントバッファ
				ConstantBufferFormat cBuffer;

				cBuffer.color	= m_color;
				cBuffer.dir		= vec4(m_dir.x, m_dir.y, m_dir.z, 0);
				cBuffer.pos		= vec4(m_position.x, m_position.y, m_position.z, 0);

				// ビュー変換行列
				vec3 up(0.0f, 1.0f, 0.0f);			// ライトの上方位置はこれでいい
				vec3 m_target = -m_dir;
				math::MatrixLookAt(&cBuffer.lightView, &m_position, &up, &m_target);
				math::MatrixTranspose(&cBuffer.lightView, &cBuffer.lightView);

				// 正射影変換行列
				D3DXMATRIX d3dMatrix;
				D3DXMatrixOrthoLH(&d3dMatrix, 16/*WINDOW_WIDTH*/, 9/*WINDOW_HEIGHT*/, 1.0f, 100.0f);
				matrix ortho(
					d3dMatrix.m[0][0], d3dMatrix.m[0][1], d3dMatrix.m[0][2], d3dMatrix.m[0][3],
					d3dMatrix.m[1][0], d3dMatrix.m[1][1], d3dMatrix.m[1][2], d3dMatrix.m[1][3],
					d3dMatrix.m[2][0], d3dMatrix.m[2][1], d3dMatrix.m[2][2], d3dMatrix.m[2][3],
					d3dMatrix.m[3][0], d3dMatrix.m[3][1], d3dMatrix.m[3][2], d3dMatrix.m[3][3]
				);
				math::MatrixTranspose(&cBuffer.lightMatrix, &ortho);

				// コンスタントバッファへコピー
				D3D11_MAPPED_SUBRESOURCE	pData;
				ID3D11Buffer*				pBuffer = m_constantBuffer.GetBuffer();

				if (SUCCEEDED(pContext->Map(pBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &pData))) {
					memcpy(pData.pData, (void*)&cBuffer, sizeof(ConstantBufferFormat));
					pContext->Unmap(pBuffer, 0);
				}

				m_update = false;
			}

			// シェーダーにコンスタントバッファデータを渡す	
			ID3D11Buffer* p[1] = { m_constantBuffer.GetBuffer() };
			pContext->VSSetConstantBuffers(2, 1, p);
			pContext->PSSetConstantBuffers(2, 1, p);
		}
	}
}
