
#include "common.hlsl"

// obj3dコンスタントバッファ スロット０
cbuffer primitive  : register(b0) {
	matrix m_world;			// ワールド変換行列
};


// テクスチャとサンプラ
Texture2D		m_heightMap	: register(t0);
SamplerState	m_sampler	: register(s0);

///////////////////////////////////////////////////////////////////////////////////////

// 頂点の入力レイアウト
struct VS_INPUT {
	float3 Pos		: IN_POSITION;
};


// 頂点の出力レイアウト
struct VS_OUTPUT {
	float4 Pos		: SV_POSITION;
};
 
// 頂点シェーダー
VS_OUTPUT VS( VS_INPUT input )
{
	VS_OUTPUT output	= (VS_OUTPUT)0;

	// 座標を一つ一つ変換するより、全行列を掛け合わせて一度計算する方が効率的
	matrix wv	= mul(m_world,				camera.m_view);
	matrix wvp	= mul(wv,					camera.m_projection);
	output.Pos	= mul(float4(input.Pos, 1), wvp);


	return output;
}

///////////////////////////////////////////////////////////////////////////////////////

// 平面距離関数
float distPlane(float3 p, float4 n)
{
	return dot(p, n.xyz) + n.w;
}

// 高さマップ反映距離関数
float distFunc(float3 p)
{
	float4 plane	= float4(0.0f, 1.5f, 0.0f, 1.0f);
	float d			= distPlane(p, plane);
	float4 tex		= m_heightMap.Sample(m_sampler, fmod(p.xz * 0.2f, 1.0f));
	return d -(tex.x * 2.0f);
}

// 法線取得
float3 getNormal(float3 p)
{
	const float e = 0.001f;
	const float3 dx = float3(e, 0.0f, 0.0f);
	const float3 dy = float3(0.0f, e, 0.0f);
	const float3 dz = float3(0.0f, 0.0f, e);

	float d = distFunc(p);

	return normalize(float3(
		d - distFunc(p - dx),
		d - distFunc(p - dy),
		d - distFunc(p - dz)
	));
}

// ピクセルシェーダー
float4 PS( VS_OUTPUT input ) : SV_TARGET
{
	float3 skyCol	= float3(0.5f, 0.2f, 0.0f);
	float3 lightDir = float3(1.0f, 1.0f, -2.0f);

	// input.Pos はレンダーターゲット上の座標(0～レンダーターゲットサイズまでの間)を指す
	float2 resolution	= float2(1280.0f, 720.0f);
	float2 screenPos	= (input.Pos.xy * 2.0f - resolution.xy) / min(resolution.x, resolution.y);

	float3 cameraPos	= camera.m_position.xyz;
	float3 cameraDir	= normalize(camera.m_target.xyz - cameraPos);
	float3 cameraSide	= normalize(cross(cameraDir, float3(0.0f, 1.0f, 0.0f)));
	float3 cameraUp		= normalize(cross(cameraSide, cameraDir));

	float targetDepth	= 2.0f;
	float3 ray			= normalize(cameraSide * screenPos.x + cameraUp * -screenPos.y + cameraDir * targetDepth);

	int maxSteps = 80;

	float depth	   = 0.0f;
	float sumDepth = 0.0f;
	float3 pos;

	float3 col = float3(0.0f, 0.0f, 0.0f);

	for (int i = 0; i < maxSteps; i++)
	{
		pos		= cameraPos + ray * sumDepth;
		depth	= distFunc(pos) * 0.25;

		if (depth <= 0.001f) { break; }

		sumDepth += depth;
	}

	if (depth <= 0.001f)
	{
		float3 normal = getNormal(pos);
		float diff	= 1.0f - dot(normal, normalize(lightDir));
		float fog	= 1.0f - exp(-sumDepth * 0.12f);
		col = lerp(float3(diff, diff, diff), skyCol, fog);
	}
	else
	{
		col = skyCol;
	}


	return float4(col, 1.0f);
}


///////////////////////////////////////////////////////////////////////////////////////


// テクニック
technique10 SympleTec {
	pass P0 {
		SetVertexShader(CompileShader(vs_5_0, VS()));
		SetGeometryShader(NULL);
		SetPixelShader(CompileShader(ps_5_0, PS()));
	}
}