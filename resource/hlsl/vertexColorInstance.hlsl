
#include "common.hlsl"

// obj3dコンスタントバッファ スロット０
cbuffer primitive  : register(b0) {
	matrix m_world;			// ワールド変換行列
};


///////////////////////////////////////////////////////////////////////////////////////

// 頂点の入力レイアウト
struct VS_INPUT {
	float3					Pos			: IN_POSITION;
	float4					Color		: IN_COLOR;
	column_major float4x4	World		: MATRIX;			// ワールド変換行列
	uint					InstanceId  : SV_InstanceID;	// インスタンスID
};


// 頂点の出力レイアウト
struct VS_OUTPUT {
	float4 Pos		: SV_POSITION;
	float4 Color	: COLOR;
};
 
// 頂点シェーダー
VS_OUTPUT VS( VS_INPUT input )
{
	VS_OUTPUT output	= (VS_OUTPUT)0;

	// 座標を一つ一つ変換するより、全行列を掛け合わせて一度計算する方が効率的
	matrix wv	= mul(input.World,			camera.m_view);
	matrix wvp	= mul(wv,					camera.m_projection);
	output.Pos	= mul(float4(input.Pos, 1), wvp);

	output.Color = input.Color;

	return output;
}

///////////////////////////////////////////////////////////////////////////////////////


// ピクセルシェーダー
float4 PS( VS_OUTPUT input ) : SV_TARGET
{
	return input.Color;
}


///////////////////////////////////////////////////////////////////////////////////////


// テクニック
technique10 SympleTec {
	pass P0 {
		SetVertexShader(CompileShader(vs_4_0, VS()));
		SetGeometryShader(NULL);
		SetPixelShader(CompileShader(ps_4_0, PS()));
	}
}