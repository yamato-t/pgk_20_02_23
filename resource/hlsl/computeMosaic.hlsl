
// 現在の描画結果
Texture2D<float4>	colorBuffer : register(t0);
// colorBuffer のサンプリング設定
SamplerState		linearSampler : register(s0);

// 出力バッファ
RWTexture2D<float4> destBuffer : register(u0);

// 1グループを構成するスレッド数
#define blocksizeX 16
#define blocksizeY 16
#define blocksizeZ 1

// グループ単位の共有バッファ
#define groupthreads (blocksizeX * blocksizeY * blocksizeZ)
groupshared float4 accum[blocksizeX][blocksizeY];


// コンピュート処理
// SV_GroupID			…　現在のグループID
// SV_DispatchThreadID	…　全グループ全スレッドにおける通しID（SV_GroupID * groupthreads + SV_GroupThreadID）
// SV_GroupThreadID		…　現グループにおけるスレッドID
// SV_GroupIndex		…　現グループにおけるスレッドの通しインデックス

[numthreads(blocksizeX, blocksizeY, blocksizeZ)]
void CS_Main(uint3 Gid : SV_GroupID, uint3 DTid : SV_DispatchThreadID, uint3 GTid : SV_GroupThreadID, uint GI : SV_GroupIndex)
{
	float4 s	= colorBuffer.Load(uint3(DTid.xy, 0));
	accum[GTid.x][GTid.y] = s;
	
	// ここまででグループ単位の同期を行う
	GroupMemoryBarrierWithGroupSync();

	float4 f = float4(0, 0, 0, 0);
	for (int y = 0; y < blocksizeY; ++y)
	{
		for (int x = 0; x < blocksizeX; ++x)
		{
			f += accum[x][y];
		}
	}

	f /= groupthreads;
	f.a = 1;
	destBuffer[DTid.xy] = f;
}