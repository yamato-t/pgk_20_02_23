
// 現在の描画結果
Texture2D<float4>	colorBuffer : register(t0);
// colorBuffer のサンプリング設定
SamplerState		linearSampler : register(s0);

// 出力バッファ
RWTexture2D<float4> destBuffer : register(u0);

// 1グループを構成するスレッド数
#define blocksizeX 16
#define blocksizeY 16
#define blocksizeZ 1

// グループ単位の共有バッファ
#define groupthreads (blocksizeX * blocksizeY * blocksizeZ)
groupshared float4 accum[blocksizeX][blocksizeY];

float4 SNN(float2 center, float2 offset )
{
	float2 r = center + offset;
	float2 l = center - offset;

	r = clamp(r, float2(0, 0), float2(15, 15));
	l = clamp(l, float2(0, 0), float2(15, 15));

	float4 rColor = accum[r.x][r.y];
	float4 lColor = accum[l.x][l.y];
	float4 cColor = accum[center.x][center.y];

	float3 rd = rColor.rgb - cColor.rgb;
	float3 ld = lColor.rgb - cColor.rgb;
	if (dot(ld, ld) < dot(rd, rd))
	{
		return lColor;
	}
	return rColor;
}

// コンピュート処理
// SV_GroupID			…　現在のグループID
// SV_DispatchThreadID	…　全グループ全スレッドにおける通しID（SV_GroupID * groupthreads + SV_GroupThreadID）
// SV_GroupThreadID		…　現グループにおけるスレッドID
// SV_GroupIndex		…　現グループにおけるスレッドの通しインデックス

[numthreads(blocksizeX, blocksizeY, blocksizeZ)]
void CS_Main(uint3 Gid : SV_GroupID, uint3 DTid : SV_DispatchThreadID, uint3 GTid : SV_GroupThreadID, uint GI : SV_GroupIndex)
{
	float4 s	= colorBuffer.Load(uint3(DTid.xy, 0));
	accum[GTid.x][GTid.y] = s;
	
	// ここまででグループ単位の同期を行う
	GroupMemoryBarrierWithGroupSync();

	float2 center = GTid.xy;

	float4 color = float4(0,0,0,0);
	int count = 1;

	for (int x = 0; x < 7; x++)
	{
		for (int y = -7; y < 7; y++)
		{
			if (x == 0 && y == 0) { continue; }
			color += (SNN(center, float2(x, y)) * 2);
			count += 2;
		}

	}

	color /= count;
	color.a = 1;

	destBuffer[DTid.xy] = color;
}