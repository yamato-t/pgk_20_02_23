

// 固定スロット1 カメラコンスタントバッファ 
struct CameraConstBuffer {
	float4	m_position;		// カメラのワールド座標
	float4	m_target;		// カメラの注視座標
	matrix  m_view;			// ビュー変換行列
	matrix  m_projection;	// 射影変換行列（透視投影 or 正射影）
};
cbuffer camera  : register(b1) {

	CameraConstBuffer camera;
};

// 固定スロット2 ライトコンスタントバッファ
struct LightConstBuffer {
	float4	m_position;		// ライトのワールド座標
	float4	m_dir;			// ライトのワールド空間上の向き
	float4	m_color;		// ライトカラー
	matrix	m_lightView;	// ライトビュー変換行列
	matrix	m_lightOrtho;	// ライト正射影変換行列
};
cbuffer light  : register(b2) {
	LightConstBuffer light;
};


// 固定テクスチャサンプラ10 シャドウマップ設定
Texture2D		m_shadowMap			: register(t10);
SamplerState	m_shadowMapSampler	: register(s10);


///////////////////////////////////////////////////////////////////////////////////////
// TexCoordの計算
///////////////////////////////////////////////////////////////////////////////////////
float2 CalcTexCoord(float4 pos) {

	//float2 coord;
	//coord.x = (1.0f + pos.x/pos.w)*0.5f;
	//coord.y = (1.0f - pos.y/pos.w)*0.5f;

	// 透視投影or正射影の計算後から算出
	// ※ 正射影時は透視投影時と違い w = 1 なので、本来除算する必要はない
	float2 coord = (float2(0.5, -0.5) * pos.xy/pos.w) + 0.5f;
	return coord;
}


///////////////////////////////////////////////////////////////////////////////////////
// 法線
///////////////////////////////////////////////////////////////////////////////////////

// 法線マップ反映
float3 CalcNormal(float3 tangent, float3 binormal, float3 normal, float3 fetchColor) {

	// 法線マップの内容に下記の「接空間にビュー変換を反映した変換行列」を掛け合わせ、ビュー空間上での法線を計算する
	float3x3 mat 	= { tangent, binormal, normal };
	float3 res		= 2.0f * fetchColor - 1.0f;
	res.z			= sqrt(1.0f - ((normal.x * normal.x) + (normal.y * normal.y)));
	return mul(res, mat);	
}

// 接空間変換行列の逆行列を計算
float3x3 CalcTangentSpaceInvMatrix(float3 tangent, float3 binormal, float3 normal) {
	// 各ベクトルを接空間に変換する為に、接空間変換行列の逆行列を作成する
	// ライトベクトルや視線ベクトルを接空間へ変換し、法線マップはフェッチ内容をそのまま利用する形になる
	float3x3 mat 	= transpose(float3x3(tangent, binormal, normal));

	return mat;
}


///////////////////////////////////////////////////////////////////////////////////////
// ライト
///////////////////////////////////////////////////////////////////////////////////////

// 拡散強度計算
float3 CalcDiffusePower(float3 normal, float3 lightDir) {
	float power	= dot(normal, lightDir);

	return power;
}

// ハーフランバート拡散強度計算
float3 CalcHalfLambertDiffusePower(float3 normal, float3 lightDir) {
	float power	= dot(normal, lightDir) * 0.5 + 0.5;

	return power;
}

// 鏡面反射強度計算
float3 CalcSpecularPower(float3 posToEye, float3 lightDir, float3 normal, float phong) {
	// 鏡面反射（ハーフベクトルを利用）
	float3	halfVec	= normalize((posToEye + lightDir));
	float	power	= pow(dot(halfVec, normal), phong);

	return power;
}

// リムライト強度計算
float CalcRimPower(float3 posToEye, float3 normal, float lightPower, float rimCoef, float rimPower) {
	float coef	= (1 - clamp(dot(posToEye, normal), 0, 1));
	float power	= pow(coef, rimCoef) * clamp(lightPower, 0, 1) * rimPower;

	return power;
}