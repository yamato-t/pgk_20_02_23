﻿#pragma once

#ifdef USE_DXSDK
#pragma warning(push)
#pragma warning(disable : 4005)
#include <d3dx11.h>
#pragma comment(lib,"d3dx11.lib")
#pragma warning(pop)
#include <d3dx10.h>
#pragma comment(lib,"d3dx10.lib")
#endif

// Windows Kits
#include <d3d11.h>
#pragma comment(lib,"d3d11.lib")
#include <DXGI.h>
#pragma comment(lib,"DXGI.lib")

namespace core
{
	namespace dx11
	{
#define SAFE_COM_RELEASE(x) if(x) { x->Release(); x = nullptr; }

		//---------------------------------------------------------------------------------
		/**
		 * @brief
		 * COMオブジェクト（ID3D関連）スマートポインタ
		 */
		template <class T>
		class ComPtr
		{
		private:
			T * p;		///< 指定（ID3D関連）型のポインタ

		public:
			//---------------------------------------------------------------------------------
			/**
			 * @brief	コンストラクタ
			 */
			ComPtr() { p = nullptr; }

			//---------------------------------------------------------------------------------
			/**
			 * @brief	デストラクタ
			 */
			~ComPtr() { Reset(); }

			//---------------------------------------------------------------------------------
			/**
			 * @brief	ポインタの設定
			 * @param	pp		指定型のポインタ
			 * @return
			 */
			void Reset(T* pp = nullptr)
			{
				SAFE_COM_RELEASE(p);

				p = pp;
			}

			//---------------------------------------------------------------------------------
			/**
			 * @brief	ポインタ取得のオペレータ
			 * @return	指定型のポインタ
			 */
			T*		operator->() { return p; }

			//---------------------------------------------------------------------------------
			/**
			 * @brief	ポインタ直接取得
			 * @return	指定型のポインタ
			 */
			T*		GetPointer() { return p; }

			//---------------------------------------------------------------------------------
			/**
			 * @brief	ポインタのポインタ取得
			 * @return	指定型のポインタのポインタ
			 */
			T**		GetPointerPointer() { return &p; }

		private:
			// その他コンストラクタを許可しない
			ComPtr(const ComPtr& r)				= delete;
			ComPtr& operator=(const ComPtr& r)	= delete;
			ComPtr(ComPtr&& r)					= delete;
			ComPtr& operator=(ComPtr&& r)		= delete;
		};
	}
}

typedef ID3D11Texture2D				ITexture2D;
typedef ID3D11Texture3D				ITexture3D;
typedef ID3D11ShaderResourceView	IShaderResourceView;
typedef ID3D11UnorderedAccessView	IUnorderedAccessView;
typedef ID3D11RenderTargetView		IRenderTargetView;
typedef ID3D11DepthStencilView		IDepthStencilView;
typedef ID3D11Buffer				IBuffer;
