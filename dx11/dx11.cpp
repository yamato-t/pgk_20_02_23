﻿#include "dx11.h"
#include "../win/window.h"
#include "../math/math.h"
#include "../texture/depthStencil.h"
#include "../texture/renderTarget.h"

#define MULTI_SAMPLE_BACKBUFFER (0)
#define USE_DEFERRED_CONTEXT	(0)

namespace core {

	namespace dx11 {

		//---------------------------------------------------------------------------------
		/**
		 * @brief
		 * DirectX11デバイス制御のインプリメントクラス
		 */
		class dx11Device::cImpl {
		public:
			ComPtr<ID3D11Device>			m_pDevice;				///< デバイス
			ComPtr<ID3D11DeviceContext>		m_pContext;				///< コンテキスト
			ComPtr<IDXGISwapChain>			m_pSwapChain;			///< スワップチェイン
			ComPtr<ID3D11RenderTargetView>	m_pBackBufferView;		///< バックバッファ用ビューオブジェクト
			ComPtr<ID3D11DeviceContext>		m_pDeferrdContext;		///< 遅延コンテキスト（現在未使用）
			ComPtr<ID3D11Query>				m_pQuery;				///< GPUイベント取得用クエリ

			ComPtr<ID3D11RasterizerState>	m_pRasterizerState;		///< ラスタライザステート


			ComPtr<IDXGIFactory>			m_pDXGIFactory;			///< ディスプレイ制御
			ComPtr<IDXGIAdapter>			m_pDXGIAdapter;			///< ディスプレイモード取得用アダプタ
			std::vector<vec2>				m_xAdapterSize;			///< 対応解像度のリスト
			DXGI_SWAP_CHAIN_DESC			m_xBackBufferDesc;		///< スワップチェイン作成情報
			DXGI_SAMPLE_DESC				m_xSampleDesc;			///< スワップチェインサンプラの作成情報



		public:
			//---------------------------------------------------------------------------------
			/**
			 * @brief	コンストラクタ
			 */
			cImpl() {
			}

			//---------------------------------------------------------------------------------
			/**
			 * @brief	デストラクタ
			 */
			~cImpl() {
			}

			//---------------------------------------------------------------------------------
			/**
			 * @brief	デバイスの初期化
			 * @return
			 */
			void Initialize() {
				// ディスプレイアダプタ設定
				SetDisplayAdapter();

				// デバイスの作成
				CreateDeviceAndSwapChain();

				// ディスプレイモードの確認
				CheckDisplayMode();
			}

			//---------------------------------------------------------------------------------
			/**
			 * @brief	ディスプレイアダプタの設定
			 * @return
			 */
			void SetDisplayAdapter() {
				WORD	wAdapterCount = 0;
				char	szAdapterName[128 * 16];
				ZeroMemory(szAdapterName, sizeof(szAdapterName));
				ComPtr<IDXGIAdapter> pDXGIAdapter;

				CreateDXGIFactory(__uuidof(IDXGIFactory), (void**)&m_pDXGIFactory);

				// アダプタを列挙
				while (m_pDXGIFactory->EnumAdapters(wAdapterCount, pDXGIAdapter.GetPointerPointer()) != DXGI_ERROR_NOT_FOUND) {
					DXGI_ADAPTER_DESC AdapterDesc;
					pDXGIAdapter->GetDesc(&AdapterDesc);
					wsprintf(szAdapterName, "%s%s\n", szAdapterName, AdapterDesc.Description);
					trace(szAdapterName);

					wAdapterCount++;
					pDXGIAdapter.Reset();
				}

				if (wAdapterCount == 0) {
					assert(false, "ディスプレイが見つからない");
				}

				// ゼロ番目のアダプタ保存
				if (S_OK != m_pDXGIFactory->EnumAdapters(0, m_pDXGIAdapter.GetPointerPointer())) {
					assert(false, "ディスプレイの取得に失敗");
				}
			}

			//---------------------------------------------------------------------------------
			/**
			 * @brief	デバイスとスワップチェインの作成
			 * @return
			 */
			void CreateDeviceAndSwapChain() {
				// デバイス作成
				D3D_FEATURE_LEVEL	pFeatureLevels = D3D_FEATURE_LEVEL_11_0;
				D3D_FEATURE_LEVEL*	pFeatureLevel = nullptr;

				if (FAILED(D3D11CreateDevice(nullptr,
					D3D_DRIVER_TYPE_HARDWARE,
					nullptr,
					0,
					&pFeatureLevels,
					1,
					D3D11_SDK_VERSION,
					m_pDevice.GetPointerPointer(),
					pFeatureLevel,
					m_pContext.GetPointerPointer()))) {
					assert(false, "デバイス作成に失敗");
				}


				// スワップチェイン作成
				DXGI_SWAP_CHAIN_DESC sd;
				ZeroMemory(&sd, sizeof(sd));
				sd.OutputWindow			= windows::Window::Instance().Handle();
				sd.BufferCount			= 1;
				sd.BufferDesc.Width		= WINDOW_WIDTH;
				sd.BufferDesc.Height	= WINDOW_HEIGHT;
				sd.BufferDesc.Format	= DXGI_FORMAT_R8G8B8A8_UNORM_SRGB;//DXGI_FORMAT_R8G8B8A8_UNORM(最終出来にデガンマしなければならないので変更)
				sd.BufferUsage			= DXGI_USAGE_RENDER_TARGET_OUTPUT;
				sd.BufferDesc.RefreshRate.Numerator = 60;
				sd.BufferDesc.RefreshRate.Denominator = 1;
				sd.SampleDesc.Count = 1;
				sd.SampleDesc.Quality = 0;
				sd.Windowed = TRUE;

				m_xSampleDesc = sd.SampleDesc;

				for (int i = D3D11_MAX_MULTISAMPLE_SAMPLE_COUNT; 0 <= i; i--) {
					UINT iSampleQuality = 0;
					m_pDevice->CheckMultisampleQualityLevels(sd.BufferDesc.Format, (UINT)i, &iSampleQuality);
					if (0 != iSampleQuality) {
						m_xSampleDesc.Count = i;
						m_xSampleDesc.Quality = iSampleQuality - 1;
						break;
					}
				}

				if (FAILED(m_pDXGIFactory->CreateSwapChain(m_pDevice.GetPointer(),
					&sd,
					m_pSwapChain.GetPointerPointer()))) {
					assert(false, "スワップチェイン作成に失敗");
				}
				m_xBackBufferDesc = sd;

				// バックバッファビューの作成
				ComPtr<ID3D11Texture2D> pBackBuffer;
				m_pSwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)pBackBuffer.GetPointerPointer());
				m_pDevice->CreateRenderTargetView(pBackBuffer.GetPointer(), nullptr, m_pBackBufferView.GetPointerPointer());

				// ラスタライズ設定
				D3D11_RASTERIZER_DESC rdc;
				ZeroMemory(&rdc, sizeof(rdc));
				rdc.CullMode = D3D11_CULL_BACK;
				rdc.FillMode = D3D11_FILL_SOLID;
				rdc.FrontCounterClockwise = TRUE; // 三角形が前向きか後ろ向きかを決定します。このパラメーターが true の場合、三角形の頂点がレンダー ターゲット上で左回りならば三角形は前向きと見なされ、右回りならば後ろ向きと見なされます。このパラメーターが false の場合は逆になります。
				m_pDevice->CreateRasterizerState(&rdc, m_pRasterizerState.GetPointerPointer());

				// GPUイベント取得用クエリの作成
				D3D11_QUERY_DESC query_desc;
				query_desc.MiscFlags	= 0;
				query_desc.Query		= D3D11_QUERY_EVENT;	// 処理状況確認
				m_pDevice->CreateQuery(&query_desc, m_pQuery.GetPointerPointer());
			}

			//---------------------------------------------------------------------------------
			/**
			 * @brief	ディスプレイモードの確認
			 * @return
			 */
			void CheckDisplayMode() {
				HRESULT hr;
				ComPtr<IDXGIOutput> pOutput;
				hr = m_pDXGIAdapter->EnumOutputs(0, pOutput.GetPointerPointer());
				if (FAILED(hr)) { assert(false, "モニターのモード取得に失敗"); }

				UINT iDisplayModeNum = 0;
				// ディスプレイモード数を取得
				hr = pOutput->GetDisplayModeList(m_xBackBufferDesc.BufferDesc.Format, 0, &iDisplayModeNum, 0);
				if (FAILED(hr)) { assert(false, "モニターのモード取得に失敗"); }

				// ディスプレイモードを列挙
				std::shared_ptr<DXGI_MODE_DESC> pDisplayMode;
				pDisplayMode.reset(new DXGI_MODE_DESC[iDisplayModeNum], std::default_delete<DXGI_MODE_DESC[]>());
				hr = pOutput->GetDisplayModeList(m_xBackBufferDesc.BufferDesc.Format, 0, &iDisplayModeNum, pDisplayMode.get());
				if (FAILED(hr)) { assert(false, "モニターのモード取得に失敗"); }

				UINT nCount = 0;
				for (UINT i = 0; i < iDisplayModeNum; i++) {

					vec2 vSize((float)pDisplayMode.get()[i].Width, (float)pDisplayMode.get()[i].Height);

					bool bFind = false;
					for (auto x : m_xAdapterSize) {
						if (true == (bFind = ((x.x == pDisplayMode.get()[i].Width) && (x.y == pDisplayMode.get()[i].Height)))) { break; }
					}

					if (!bFind) { m_xAdapterSize.push_back(vSize); }
				}
			}

			//---------------------------------------------------------------------------------
			/**
			 * @brief	レンダーターゲットをバックバッファに設定
			 * @param	pContext	描画コンテキスト
			 * @return
			 */
			void ResetRenderTarget(ID3D11DeviceContext* pContext) {
				// レンダーターゲットをバックバッファに設定
				IRenderTargetView* targets[1] = { m_pBackBufferView.GetPointer() };
				pContext->OMSetRenderTargets(1, targets, nullptr);
			}

			//---------------------------------------------------------------------------------
			/**
			 * @brief	バックバッファクリア
			 * @param	pContext	描画コンテキスト
			 * @return
			 */
			void Clear(ID3D11DeviceContext* pContext) {
				// バックバッファカラークリア
				float  clearColor[4] = { 0.2f, 0.2f, 0.2f, 1.0f };
				pContext->ClearRenderTargetView(m_pBackBufferView.GetPointer(), clearColor);

				// ラスタライザ設定
				pContext->RSSetState(m_pRasterizerState.GetPointer());
			}

			//---------------------------------------------------------------------------------
			/**
			 * @brief	フロントバッファへの出力
			 * @return
			 */
			void Present() {
				m_pSwapChain->Present(1, 0);
			}


			//---------------------------------------------------------------------------------
			/**
			 * @brief	GPU処理が終了するまで待機
			 * @return
			 */
			void WaitGPU() {

				m_pContext->End(m_pQuery.GetPointer());

				BOOL bFinish = FALSE;

				while(true) {
					HRESULT hr = m_pContext->GetData( m_pQuery.GetPointer(), &bFinish, sizeof(bFinish), 0);
					if(hr == S_OK) {
						if(bFinish) {
							break;
						}
					}
				}
			}
		};

		//---------------------------------------------------------------------------------
		/**
		 * @brief	コンストラクタ
		 */
		dx11Device::dx11Device() {
			m_pImpl.reset(new dx11Device::cImpl());
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	デストラクタ
		 */
		dx11Device::~dx11Device() {
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	デバイスの初期化
		 * @return
		 */
		void dx11Device::Initialize() {
			m_pImpl->Initialize();
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	レンダーターゲットにバックバッファを設定
		 * @param	pContext	描画コンテキスト
		 * @return
		 */
		void dx11Device::ResetRenderTarget(ID3D11DeviceContext* pContext) {
			m_pImpl->ResetRenderTarget(pContext);
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	バックバッファのクリア
		 * @param	pContext	描画コンテキスト
		 * @return
		 */
		void dx11Device::Clear(ID3D11DeviceContext* pContext) {
			m_pImpl->Clear(pContext);
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	フロントバッファへ出力
		 * @return
		 */
		void dx11Device::Present() {
			m_pImpl->Present();
		}


		//---------------------------------------------------------------------------------
		/**
		 * @brief	デバイス取得
		 * @return	デバイスのポインタ
		 */
		ID3D11Device* dx11Device::Device() {
			return m_pImpl->m_pDevice.GetPointer();
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	デバイスコンテキストの取得
		 * @return	デバイスコンテキストのポインタ
		 */
		ID3D11DeviceContext* dx11Device::DeviceContext() {
			return m_pImpl->m_pContext.GetPointer();
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	GPU処理が終了するまで待機
		 * @return
		 */
		void dx11Device::WaitGPU() {
			m_pImpl->WaitGPU();
		}
	}
}
