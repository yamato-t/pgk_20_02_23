﻿#pragma once

#include "../obj/drawObj.h"

namespace core {

	namespace obj3d {

		//---------------------------------------------------------------------------------
		/**
		 * @brief
		 * プレーン3Dテクスチャ描画オブジェクト
		 */
		class PlaneTexture3d : public obj::DrawObj {

		public:
			//---------------------------------------------------------------------------------
			/**
			 * @brief	コンストラクタ
			 */
			PlaneTexture3d();

			//---------------------------------------------------------------------------------
			/**
			 * @brief	デストラクタ
			 */
			virtual ~PlaneTexture3d();

		public:
			//---------------------------------------------------------------------------------
			/**
			 * @brief	プレーンの初期化
			 * @return
			 */
			virtual void Initialize();

			//---------------------------------------------------------------------------------
			/**
			 * @brief	描画オブジェクトの描画処理
			 * @param	pContext	描画コンテキスト
			 * @param	pMaterial	外部指定のマテリアル
			 * @return
			 */
			virtual void Draw(ID3D11DeviceContext* pContext);

		};
	}
};
