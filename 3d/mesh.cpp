﻿#include "mesh.h"
#include <fstream>
#include <sstream>
#include "../render/obj3dRender.h"

namespace core {

	namespace obj3d {

		//---------------------------------------------------------------------------------
		/**
		 * @brief
		 * メッシュ描画オブジェクトのコンスタントバッファ内容
		 */
		struct ConstantBufferFormat {
			matrix world;		///< ワールド変換行列
		};

		//---------------------------------------------------------------------------------
		/**
		 * @brief	コンストラクタ
		 */
		Mesh::Mesh() {
			// 描画登録
			obj::DrawObjContainer::Instance().Register(obj::DrawObjContainer::layer::opaque, this);
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	デストラクタ
		 */
		Mesh::~Mesh() {
			// 登録抹消
			//render::Obj3DRender::Instance().Erase(this);
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	メッシュの読み込み
		 * @param	filePath	ファイルパス
		 * @return
		 */
		void Mesh::Load(std::string filePath) {

			// テキスト形式のOBJファイル読み込み
			std::ifstream	file;
			file.open(filePath);

			// 文字列ストリームを利用して入力
			std::stringstream strstr;
			strstr << file.rdbuf();
			m_resource = strstr.str();
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	メッシュの初期化
		 * @return
		 */
		void Mesh::Initialize() {

			// ロードで設定しているのでまたストリームを利用するのは無駄
			std::istringstream	file(m_resource);
			std::string			str;

			uint16_t	vertexNum	= 0;	// 頂点数
			uint16_t	normalNum	= 0;	// 法線数
			uint16_t	uvNum		= 0;	// uv数
			uint16_t	faceNum		= 0;	// フェイス数
			uint16_t	materialNum	= 0;	// マテリアル数

			// OBJファイルの内容から各要素数を先に取得
			{
				while(std::getline(file, str)) {
					// 頂点
					if(str.find("v ") != std::string::npos)				{ vertexNum++;	}
					// 法線
					else if(str.find("vn ") != std::string::npos)		{ normalNum++;	}
					// UV
					else if(str.find("vt ") != std::string::npos)		{ uvNum++;		}
					// フェイス
					else if(str.find("f ") != std::string::npos)		{ faceNum++;	}
					// マテリアル
					else if(str.find("mtllib ") != std::string::npos)	{ materialNum++; }
				}
			}

			// ファイルの先頭に移動
			file.clear();
			file.seekg(0, std::ios_base::beg);

			vec3*		pVertex	= new vec3[vertexNum];			// 頂点
			vec2*		pUv		= new vec2[uvNum];				// UV
			vec3*		pNormal	= new vec3[normalNum];			// 法線

			int currentVertexNum	= 0;
			int currentNormalNum	= 0;
			int currentUvNum		= 0;

			// 情報の種類を指すキー文字列の格納先
			char key[8] = {};

			// 各要素の内容を読み込む
			{
				while(std::getline(file, str)) {

					// 頂点
					if(str.find("v ") != std::string::npos) {
						// OBJファイルは右手座標系なのでx or ｚを反転
						SetVertexInfo(pVertex[currentVertexNum], str);
						pVertex[currentVertexNum].x = -pVertex[currentVertexNum].x;
						currentVertexNum++;
					}
					// 法線
					else if(str.find("vn ") != std::string::npos) {
						// OBJファイルは右手座標系なのでx or ｚを反転
						SetVertexInfo(pNormal[currentNormalNum], str);
						pNormal[currentNormalNum].x = -pNormal[currentNormalNum].x;
						currentNormalNum++;
					}
					// UV
					else if(str.find("vt ") != std::string::npos){
						// OBJファイルはY成分が反転
						SetVertexInfo(pUv[currentUvNum], str);
						pUv[currentUvNum].y = 1 - pUv[currentUvNum].y;
						currentUvNum++;
					}
					// マテリアル
					else if(str.find("mtllib ") != std::string::npos)	{
						char fileName[32] = {};
						sscanf_s(str.c_str(), "%s %s", key, (unsigned)_countof(key), fileName, (unsigned)_countof(fileName));
						std::string s = "resource/mesh/" + std::string(fileName);
						LoadMaterialFromFile(s);
					}
				}
			}

			// ファイルの先頭に移動
			file.clear();
			file.seekg(0, std::ios_base::beg);

			// 頂点バッファのフォーマットを定義
			uint32_t formats[] = {
				buffer::VertexBuffer::format::pos,
				buffer::VertexBuffer::format::normal,
				buffer::VertexBuffer::format::uv,
			};

			// 頂点バッファの内容を作成
			struct IBuffer {
				vec3		pos;		// 頂点
				vec3		normal;		// 法線
				vec2		uv;			// UV
			};

			// 頂点バッファを作成
			IBuffer* pVertexBuffers	= new IBuffer[faceNum * 3];
			uint16_t currentVertex	= 0;

			// インデックスバッファを作成
			m_pIndexBuffers = new buffer::IndexBuffer[m_materialNum];

			// インデックスバッファはマテリアル単位で格納する
			for(uint16_t i = 0; i < m_materialNum; i++) {

				// ファイルの先頭に移動
				file.clear();
				file.seekg(0, std::ios_base::beg);

				// インデックスバッファ作成（3頂点で1フェイスを構成）
				uint16_t* pIndex		= new uint16_t[faceNum * 3];
				uint16_t currentIndex	= 0;

				bool isSet = true;

				// 各要素の内容を設定する
				while(std::getline(file, str)) {

					// マテリアル名を元に設定する頂点バッファの内容を選別する
					if(str.find("usemtl ") != std::string::npos) {
						char materialName[32] = {};
						sscanf_s(str.c_str(), "%s %s", key, (unsigned)_countof(key), materialName, (unsigned)_countof(materialName));
						auto p = dynamic_cast<material::MeshMaterial*>(m_pMaterials[i]);
						isSet = (std::string(p->m_name) == std::string(materialName));
					}

					// フェイス
					if(isSet && str.find("f ") != std::string::npos) {
						// フェイスを構成する三頂点の各情報が入力されている配列の番号を取得
						char		c		= {};
						uint32_t	v1[3]	= {};
						uint32_t	v2[3]	= {};
						uint32_t	v3[3]	= {};
						sscanf_s(str.c_str(), "%s %d/%d/%d %d/%d/%d %d/%d/%d", key, (unsigned)_countof(key), &v1[0],&v1[1],&v1[2],&v2[0],&v2[1],&v2[2],&v3[0],&v3[1],&v3[2]);

						// 配列番号から実データを取得し、頂点バッファに入力する
						auto vertexIndex						= currentVertex * 3;
						pVertexBuffers[vertexIndex].pos			= pVertex[v1[0] - 1];
						pVertexBuffers[vertexIndex].uv			= pUv	 [v1[1] - 1];
						pVertexBuffers[vertexIndex].normal		= pNormal[v1[2] - 1];

						pVertexBuffers[vertexIndex+1].pos		= pVertex[v2[0] - 1];
						pVertexBuffers[vertexIndex+1].uv		= pUv	 [v2[1] - 1];
						pVertexBuffers[vertexIndex+1].normal	= pNormal[v2[2] - 1];

						pVertexBuffers[vertexIndex+2].pos		= pVertex[v3[0] - 1];
						pVertexBuffers[vertexIndex+2].uv		= pUv	 [v3[1] - 1];
						pVertexBuffers[vertexIndex+2].normal	= pNormal[v3[2] - 1];

						// 頂点バッファ番号をインデックスバッファに入力する
						auto indexIndex			= currentIndex * 3;
						pIndex[indexIndex]		= vertexIndex;
						pIndex[indexIndex+1]	= vertexIndex+1;
						pIndex[indexIndex+2]	= vertexIndex+2;

						currentIndex++;
						currentVertex++;
					}
				}

				m_pIndexBuffers[i].Create(sizeof(uint16_t), (currentIndex * 3), pIndex);
				delete[] pIndex;
			}

			// 頂点バッファ作成
			uint32_t formatCount = sizeof(formats) / sizeof(formats[0]);
			uint32_t vertexCount = (faceNum * 3);
			m_vertexBuffer.Create(formatCount, formats, vertexCount, pVertexBuffers);

			// 生成時に利用したバッファを削除
			delete[] pVertex;
			delete[] pNormal;
			delete[] pUv;
			delete[] pVertexBuffers;


			// シェーダ作成
			std::string path = "resource/hlsl/mesh.hlsl";
			for(uint16_t i = 0; i < m_materialNum; i++) {
				m_pMaterials[i]->SetShader(path, m_vertexBuffer.GetFormat());
			}

			// コンスタントバッファ作成
			m_constantBuffer.Create(sizeof(ConstantBufferFormat));

			m_prepared = true;

		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	描画オブジェクトの描画処理
		 * @param	pContext	描画コンテキスト
		 * @param	pMaterial	外部指定のマテリアル
		 * @return
		*/
		void Mesh::Draw(ID3D11DeviceContext* pContext) {

			// 更新がある時のみ
			if(m_update) {
				// コンスタントバッファ
				ConstantBufferFormat cBuffer;

				// ワールド変換行列
				math::MatrixTranspose(&cBuffer.world, &m_transform);

				// コンスタントバッファへコピー
				D3D11_MAPPED_SUBRESOURCE	pData;
				ID3D11Buffer*				pBuffer = m_constantBuffer.GetBuffer();

				if (SUCCEEDED(pContext->Map(pBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &pData))) {
					memcpy(pData.pData, (void*)&cBuffer, sizeof(ConstantBufferFormat));
					pContext->Unmap(pBuffer, 0);
				}

				m_update = false;
			}

			// シェーダーにコンスタントバッファデータを渡す
			ID3D11Buffer* p[1] = { m_constantBuffer.GetBuffer() };
			pContext->VSSetConstantBuffers(0, 1, p);
			pContext->PSSetConstantBuffers(0, 1, p);

			// 頂点バッファ設定
			auto vertex = GetVertexBuffer();
			auto buffer = vertex->GetBuffer();
			UINT stride = vertex->Stride();
			UINT offset = vertex->Offset();
			pContext->IASetVertexBuffers(0, 1, &buffer, &stride, &offset);

			// プリミティブトポロジー設定（3頂点で1フェイスを構成）
			pContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

			// マテリアルの数だけ描画する
			for(uint16_t i = 0; i < m_materialNum; i++) {
				// インデックスバッファ設定
				auto index			= GetIndexBuffer(i);
				auto indexBuffer	= index->GetBuffer();
				pContext->IASetIndexBuffer(indexBuffer, index->GetDXGIFormat(), 0);

				// マテリアル設定
				auto pTemp = m_pMaterial ? m_pMaterial : m_pMaterials[i];
				pTemp->SetCommand(pContext);

				// サンプラーステート設定
				ID3D11SamplerState* pSampler[1] = { m_samplerState.GetSamplerState() };
				pContext->PSSetSamplers(0, 1, pSampler);

				// インデックスバッファを利用した描画
				pContext->DrawIndexed(index->GetCount(), 0, 0);
			}
		}



		//---------------------------------------------------------------------------------
		/**
		 * @brief		頂点情報をファイルから設定
		 * @param[out]	pDest	情報の設定先
		 * @param		str		テキスト形式の情報
		 * @return
		 */
		void Mesh::SetVertexInfo(float* pDest, std::string& str) {
			int		i = 0;
			std::string			split;
			std::stringstream	strStr(str);

			while(std::getline(strStr, split, ' ')) {
				if(0 != i) { pDest[i - 1] = std::stof(split); }
				i++;
			}
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief		マテリアルの読み込みと設定
		 * @param		fileName	マテリアルファイル名
		 * @return
		 */
		void Mesh::LoadMaterialFromFile(const std::string& fileName) {

			// マテリアルフォーマットを調べる
			std::ifstream	file;
			std::string		str;

			// テキスト形式のOBJファイル読み込み
			file.open(fileName);

			int					materialCount	= 0;
			std::vector<int>	texturesCount;

			// マテリアル数を調べる
			while(std::getline(file, str)) {
				if(str.find("newmtl ") != std::string::npos)		{ materialCount++;	texturesCount.push_back(0); }
				else if(str.find("map_Kd ") != std::string::npos)	{ texturesCount[materialCount - 1]+=2;			}	// とりあえず法線マップの分も加える
			}

			// マテリアルの数を保存
			m_materialNum = materialCount;

			// マテリアルが無いのでここで終了
			if(0 == m_materialNum) { file.close(); return; }

			// ファイルに格納されているマテリアルの数を元にデータ生成
			auto data = new material::CreateMeshMaterialData[m_materialNum];
			// テクスチャ数設定(法線マップの分も含める)
			for(int i = 0; i < m_materialNum; i++ ) {
				data[i].textureCount = texturesCount[i];
			}

			// 色情報
			vec4								color(0, 0, 0, 1);
			int16_t								materialIndex	= -1;
			uint16_t							textureIndex	= 0;
			material::CreateMeshMaterialData*	pTemp			= nullptr;
			std::string							path			= fileName.substr(0, fileName.find_last_of("/"));

			// ファイルの先頭に移動
			file.clear();
			file.seekg(0, std::ios_base::beg);

			// マテリアル内容の確認
			char key[8] = {};
			while(std::getline(file, str)) {
				if(str.find("newmtl ") != std::string::npos) {
					pTemp = &data[++materialIndex];

					char materialName[64] = {};
					sscanf_s(str.c_str(), "%s %s", key, (unsigned)_countof(key), materialName, (unsigned)_countof(materialName));
					pTemp->name = materialName;
					textureIndex = 0;
				} else if(str.find("map_Kd ") != std::string::npos) {
					char textureName[32] = {};
					sscanf_s(str.c_str(), "%s %s", key, (unsigned)_countof(key), textureName, (unsigned)_countof(textureName));
					std::string strFile = textureName;
					if(!strFile.empty() && textureIndex < data[materialIndex].textureCount) {
						// テクスチャ作成
						pTemp->texturePath[textureIndex++] = path + "/" + strFile;
						// 法線マップもついでに設定する
						pTemp->texturePath[textureIndex++] = path + "/BumpMapTexturePreview.png";
					}
				} else if(str.find("Ka ") != std::string::npos) {
					sscanf_s(str.c_str(), "%s %f %f %f", key, (unsigned)_countof(key), &color.x, &color.y, &color.z);
					pTemp->ambient = color;
				} else if(str.find("Kd ") != std::string::npos) {
					sscanf_s(str.c_str(), "%s %f %f %f", key, (unsigned)_countof(key), &color.x, &color.y, &color.z);
					pTemp->diffuse = color;
				} else if(str.find("Ks ") != std::string::npos) {
					sscanf_s(str.c_str(), "%s %f %f %f", key, (unsigned)_countof(key), &color.x, &color.y, &color.z);
					pTemp->specular = color;
				}
			}

			file.close();

			// マテリアル配列の生成
			m_pMaterials = new material::Material*[m_materialNum];
			// マテリアル作成
			for(int i = 0; i < m_materialNum; i++ ) {
				m_pMaterials[i] = new material::MeshMaterial();
				m_pMaterials[i]->SetData(&data[i]);

				// テストで適当に設定
				//if(1 < m_materialNum)	{ m_pMaterials[i]->SetBlend(render::RenderState::blendState::default); }
				//else					{ m_pMaterials[i]->SetBlend(render::RenderState::blendState::add); }
			}


			// サンプラ作成
			m_samplerState.Create(D3D11_TEXTURE_ADDRESS_WRAP, D3D11_FILTER_MIN_MAG_MIP_LINEAR);

			// 生成データを削除
			delete[] data;
		}
	}
}
