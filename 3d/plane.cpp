﻿#include "plane.h"
#include "../render/obj3dRender.h"
#include "../material/textureMaterial.h"


namespace core {

	namespace obj3d {

		//---------------------------------------------------------------------------------
		/**
		 * @brief
		 * プレーン描画オブジェクトのコンスタントバッファ内容
		 */
		struct ConstantBufferFormat
		{
			matrix world;		///< ワールド変換行列
		};

		//---------------------------------------------------------------------------------
		/**
		 * @brief	コンストラクタ
		 */
		Plane::Plane() {
			// 描画登録
			obj::DrawObjContainer::Instance().Register(obj::DrawObjContainer::layer::opaque, this);
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	デストラクタ
		 */
		Plane::~Plane() {
			// 登録抹消
			//render::Obj3DRender::Instance().Erase(this);
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	プレーンの初期化
		 * @return
		 */
		void Plane::Initialize() {

			// 頂点バッファのフォーマットを定義
			uint32_t formats[] = {
				buffer::VertexBuffer::format::pos,
				buffer::VertexBuffer::format::color,
				buffer::VertexBuffer::format::uv,
			};

			// 頂点バッファの内容を作成
			struct IBuffer {
				vec3		pos;		// 位置
				vec4		color;		// カラー
				vec2		uv;			// 頂点座標
			};
			IBuffer vertexBuffers[4];
			{
				vertexBuffers[0].pos = vec3(-1.0f, 1.0f, -1.0f);
				vertexBuffers[0].color	= vec4(1);
				vertexBuffers[0].uv		= vec2(0, 0);

				vertexBuffers[1].pos = vec3(-1.0f, -1.0f, -1.0f);
				vertexBuffers[1].color	= vec4(1);
				vertexBuffers[1].uv		= vec2(0, 1);

				vertexBuffers[2].pos = vec3(1.0f, 1.0f, -1.0f);
				vertexBuffers[2].color	= vec4(1);
				vertexBuffers[2].uv		= vec2(1, 0);

				vertexBuffers[3].pos = vec3(1.0f, -1.0f, -1.0f);
				vertexBuffers[3].color	= vec4(1);
				vertexBuffers[3].uv		= vec2(1, 1);
			}
			uint32_t formatCount = sizeof(formats) / sizeof(formats[0]);
			uint32_t vertexCount = sizeof(vertexBuffers) / sizeof(vertexBuffers[0]);
			m_vertexBuffer.Create(formatCount, formats, vertexCount, vertexBuffers);

			// インデックスバッファを作成
			m_pIndexBuffers = new buffer::IndexBuffer[1];
			uint16_t index[] = { 0, 1, 2, 3 };
			m_pIndexBuffers[0].Create(sizeof(uint16_t), vertexCount, index);

			// マテリアル作成
			m_materialNum	= 1;
			m_pMaterials	= new material::Material*[m_materialNum];
			m_pMaterials[0]	= new material::TextureMaterial();
			material::CreateTextureMaterialData data;
			data.texturePath = "resource/texture/pattern.bmp";
			m_pMaterials[0]->SetData(&data);
			// シェーダ設定
			std::string shaderPath = "resource/hlsl/vertexColorTexture.hlsl";
			m_pMaterials[0]->SetShader(shaderPath, m_vertexBuffer.GetFormat());

			// サンプラ
			m_samplerState.Create(D3D11_TEXTURE_ADDRESS_WRAP, D3D11_FILTER_MIN_MAG_MIP_LINEAR);

			// コンスタントバッファ
			m_constantBuffer.Create(sizeof(ConstantBufferFormat));

			m_prepared = true;
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	プレーンの描画
		 * @param	pContext	描画コンテキスト
		 * @param	pMaterial	外部指定のマテリアル
		 * @return
		 */
		void Plane::Draw(ID3D11DeviceContext* pContext) {

			// 更新がある時のみ
			{
				// コンスタントバッファ
				ConstantBufferFormat cBuffer;

				// ワールド変換行列
				math::MatrixIdentity(&cBuffer.world);
				cBuffer.world.m[0][0] = 1.6f;
				math::MatrixTranspose(&cBuffer.world, &cBuffer.world);

				// コンスタントバッファへコピー
				D3D11_MAPPED_SUBRESOURCE	pData;
				ID3D11Buffer*				pBuffer = m_constantBuffer.GetBuffer();

				if (SUCCEEDED(pContext->Map(pBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &pData))) {
					memcpy(pData.pData, (void*)&cBuffer, sizeof(ConstantBufferFormat));
					pContext->Unmap(pBuffer, 0);
				}
			}

			// シェーダーにコンスタントバッファデータを渡す
			ID3D11Buffer* p[1] = { m_constantBuffer.GetBuffer() };
			pContext->VSSetConstantBuffers(0, 1, p);
			pContext->PSSetConstantBuffers(0, 1, p);

			// 頂点バッファ設定
			auto vertex = GetVertexBuffer();
			auto buffer = vertex->GetBuffer();
			UINT stride = vertex->Stride();
			UINT offset = vertex->Offset();
			pContext->IASetVertexBuffers(0, 1, &buffer, &stride, &offset);

			// プリミティブトポロジー設定
			pContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP);

			// インデックスバッファ設定
			auto index			= GetIndexBuffer();
			auto indexBuffer	= index->GetBuffer();
			pContext->IASetIndexBuffer(indexBuffer, index->GetDXGIFormat(), 0);

			// マテリアル設定
			if (m_pMaterial)	{ m_pMaterial->SetCommand(pContext); }
			else				{ m_pMaterials[0]->SetCommand(pContext); }

			// サンプラーステート設定
			ID3D11SamplerState*			pSampler[1] = { m_samplerState.GetSamplerState() };
			pContext->PSSetSamplers(0, 1, pSampler);

			// インデックスバッファを利用した描画
			pContext->DrawIndexed(index->GetCount(), 0, 0);
		}
	}
}
