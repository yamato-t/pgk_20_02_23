﻿#pragma once

#include "../obj/drawObj.h"

namespace core {

	namespace obj3d {

		//---------------------------------------------------------------------------------
		/**
		 * @brief
		 * インスタンス描画オブジェクトクラス
		 */
		class Instance : public obj::DrawObj {

		public:
			//---------------------------------------------------------------------------------
			/**
			 * @brief	コンストラクタ
			 */
			Instance();

			//---------------------------------------------------------------------------------
			/**
			 * @brief	デストラクタ
			 */
			virtual ~Instance();

		public:
			//---------------------------------------------------------------------------------
			/**
			 * @brief	インスタンス描画オブジェクトの初期化
			 * @return
			 */
			virtual void Initialize();

			//---------------------------------------------------------------------------------
			/**
			 * @brief	インスタンス描画処理
			 * @param	pContext	描画コンテキスト
			 * @param	pMaterial	外部指定のマテリアル
			 * @return
			 */
			virtual void Draw(ID3D11DeviceContext* pContext);

		private:
			buffer::VertexBuffer m_instanceBuffer;	///< インスタンス描画オブジェクト用追加頂点バッファ

		};
	}
};
