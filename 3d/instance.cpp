﻿#include "instance.h"
#include "../render/obj3dRender.h"

namespace core {

	namespace obj3d {

		//---------------------------------------------------------------------------------
		/**
		 * @brief
		 * インスタンス描画オブジェクトのコンスタントバッファ内容
		 */
		struct ConstantBufferFormat
		{
			matrix world;		///< ワールド変換行列
		};

		static const uint8_t g_instanceCount = 2;

		//---------------------------------------------------------------------------------
		/**
		 * @brief	コンストラクタ
		 */
		Instance::Instance() {
			// 描画登録
			obj::DrawObjContainer::Instance().Register(obj::DrawObjContainer::layer::opaque, this);
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	デストラクタ
		 */
		Instance::~Instance() {
			// 登録抹消
			//render::Obj3DRender::Instance().Erase(this);
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	インスタンス描画オブジェクトの初期化
		 * @return
		 */
		void Instance::Initialize() {

			// 頂点バッファのフォーマットを定義
			uint32_t formats[] = {
				buffer::VertexBuffer::format::pos,
				buffer::VertexBuffer::format::color,
			};
			// 頂点バッファの内容を作成
			struct IBuffer {
				vec3		pos;		// 位置
				vec4		color;		// カラー
			};
			IBuffer vertexBuffers[6];
			{
				vertexBuffers[0].pos = vec3(0.0f, 0.5f, 0);
				vertexBuffers[0].color = vec4(0, 0, 0, 1);

				vertexBuffers[1].pos = vec3(0.0f, 0.0f, 0);
				vertexBuffers[1].color = vec4(0, 0, 0, 1);

				vertexBuffers[2].pos = vec3(0.5f, 0.5f, 0);
				vertexBuffers[2].color = vec4(0, 0, 0, 1);

				vertexBuffers[3].pos = vec3(0.5f, 0.0f, 0);
				vertexBuffers[3].color = vec4(0, 0, 0, 1);

				vertexBuffers[4].pos = vec3(1.0f, 0.5f, 0);
				vertexBuffers[4].color = vec4(1, 1, 1, 1);

				vertexBuffers[5].pos = vec3(1.0f, 0.0f, 0);
				vertexBuffers[5].color = vec4(1, 1, 1, 1);

			}
			uint32_t formatCount = sizeof(formats) / sizeof(formats[0]);
			uint32_t vertexCount = sizeof(vertexBuffers) / sizeof(vertexBuffers[0]);
			m_vertexBuffer.Create(formatCount, formats, vertexCount, vertexBuffers);

			// インスタンス描画に必要な情報を頂点フォーマットに追加するバッファを作成
			uint32_t instanceFormats[] = {
				buffer::VertexBuffer::format::float4x4,
			};
			m_instanceBuffer.Create(1, instanceFormats,	g_instanceCount, nullptr);

			// インデックスバッファを作成
			m_pIndexBuffers = new buffer::IndexBuffer[1];
			uint16_t index[] = { 0, 1, 2, 3, 4, 5 };
			m_pIndexBuffers[0].Create(sizeof(uint16_t), vertexCount, index);

			// コンスタントバッファ
			m_constantBuffer.Create(sizeof(ConstantBufferFormat));

			// マテリアル作成
			m_materialNum	= 1;
			m_pMaterials	= new material::Material*[m_materialNum];
			m_pMaterials[0]	= new material::Material();
			// シェーダ設定
			std::string shaderPath = "resource/hlsl/vertexColorInstance.hlsl";
			m_pMaterials[0]->SetShader(shaderPath, m_vertexBuffer.GetFormat(), m_instanceBuffer.GetFormat());

			m_prepared = true;
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	インスタンス描画処理
		 * @param	pContext	描画コンテキスト
		 * @param	pMaterial	外部指定のマテリアル
		 * @return
		 */
		void Instance::Draw(ID3D11DeviceContext* pContext) {
			// インスタンス設定
			{
				D3D11_MAPPED_SUBRESOURCE	pMatrix;
				ID3D11Buffer*				pBuffer = m_instanceBuffer.GetBuffer();

				if (SUCCEEDED(pContext->Map( pBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &pMatrix ))) {

					matrix* m = (matrix*)(pMatrix.pData);
					for(int i = 0; i < g_instanceCount; i++) {

						// インスタンス別のワールド変換行列を試しにここで更新
						math::MatrixIdentity(&m[i]);
						m[i].m[3][0] = 1.5f * i;
						m[i].m[3][1] = 0.5f * i;
						m[i].m[3][2] = 2.f	* i;
						math::MatrixTranspose(&m[i], &m[i]);
					}
					pContext->Unmap(pBuffer, 0);
				}
			}

			// コンスタントバッファ設定
			{
				ConstantBufferFormat cBuffer;

				// ワールド変換行列
				math::MatrixIdentity(&cBuffer.world);
				cBuffer.world.m[3][0] = -2.f;
				math::MatrixTranspose(&cBuffer.world, &cBuffer.world);

				// コンスタントバッファへコピー
				D3D11_MAPPED_SUBRESOURCE	pData;
				ID3D11Buffer*				pBuffer = m_constantBuffer.GetBuffer();

				if (SUCCEEDED(pContext->Map(pBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &pData))) {
					memcpy(pData.pData, (void*)&cBuffer, sizeof(ConstantBufferFormat));
					pContext->Unmap(pBuffer, 0);
				}
			}

			// シェーダーにコンスタントバッファデータを渡す
			ID3D11Buffer* p[1] = { m_constantBuffer.GetBuffer() };
			pContext->VSSetConstantBuffers(0, 1, p);
			pContext->PSSetConstantBuffers(0, 1, p);

			// インスタンス描画用頂点バッファ設定
			auto vertex = GetVertexBuffer();
			ID3D11Buffer*	pBuffer[g_instanceCount]	= { vertex->GetBuffer(),	m_instanceBuffer.GetBuffer() };
			UINT			stride[g_instanceCount]		= { vertex->Stride(),		m_instanceBuffer.Stride() };
			UINT			offset[g_instanceCount]		= { vertex->Offset(),		m_instanceBuffer.Offset() };
			pContext->IASetVertexBuffers(0, g_instanceCount, pBuffer, stride, offset);

			// プリミティブトポロジー設定
			pContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP);

			// インデックスバッファ設定
			auto index			= GetIndexBuffer();
			auto indexBuffer	= index->GetBuffer();
			pContext->IASetIndexBuffer(indexBuffer, index->GetDXGIFormat(), 0);

			// マテリアル設定
			m_pMaterials[0]->SetCommand(pContext);

			// インスタンス描画
			pContext->DrawIndexedInstanced(index->GetCount(), g_instanceCount, 0, 0, 0);
		}
	}
}
