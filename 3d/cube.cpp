﻿#include "cube.h"
#include "../render/obj3dRender.h"
#include "../material/textureMaterial.h"


namespace core {

	namespace obj3d {

		//---------------------------------------------------------------------------------
		/**
		 * @brief
		 * 立方体描画オブジェクトのコンスタントバッファ内容
		 */
		struct ConstantBufferFormat
		{
			matrix	toWorld;			///< ワールド空間への変換行列
			matrix	toObject;			///< オブジェクト空間への変換行列
		};

		//---------------------------------------------------------------------------------
		/**
		 * @brief	コンストラクタ
		 */
		Cube::Cube() {
			// 描画登録
			obj::DrawObjContainer::Instance().Register(obj::DrawObjContainer::layer::opaque, this);
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	デストラクタ
		 */
		Cube::~Cube() {
			// 登録抹消
			//render::Obj3DRender::Instance().Erase(this);
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	立方体初期化
		 * @return
		 */
		void Cube::Initialize() {

			// 頂点バッファのフォーマットを定義
			uint32_t formats[] = {
				buffer::VertexBuffer::format::pos,
				buffer::VertexBuffer::format::uvw,
			};

			// 頂点バッファの内容を作成
			struct IBuffer {
				vec3		pos;		// 位置
				vec3		uvw;		// 頂点座標
			};
			IBuffer vertexBuffers[8];
			{
				vertexBuffers[0].pos	= vec3(-1.0f, 1.0f, -1.0f);
				vertexBuffers[0].uvw	= vec3(0.0f, 0.0f, 0.0f);

				vertexBuffers[1].pos	= vec3(-1.0f, -1.0f, -1.0f);
				vertexBuffers[1].uvw	= vec3(0.0f, 1.0f, 0.0f);

				vertexBuffers[2].pos	= vec3(1.0f, 1.0f, -1.0f);
				vertexBuffers[2].uvw	= vec3(1.0f, 0.0f, 0.0f);

				vertexBuffers[3].pos	= vec3(1.0f, -1.0f, -1.0f);
				vertexBuffers[3].uvw	= vec3(1.0f, 1.0f, 0.0f);

				vertexBuffers[4].pos	= vec3(-1.0f, 1.0f, 1.0f);
				vertexBuffers[4].uvw	= vec3(0.0f, 0.0f, 1.0f);

				vertexBuffers[5].pos	= vec3(-1.0f, -1.0f, 1.0f);
				vertexBuffers[5].uvw	= vec3(0.0f, 1.0f, 1.0f);

				vertexBuffers[6].pos	= vec3(1.0f, 1.0f, 1.0f);
				vertexBuffers[6].uvw	= vec3(1.0f, 0.0f, 1.0f);

				vertexBuffers[7].pos	= vec3(1.0f, -1.0f, 1.0f);
				vertexBuffers[7].uvw	= vec3(1.0f, 1.0f, 1.0f);

			}
			uint32_t formatCount = sizeof(formats) / sizeof(formats[0]);
			uint32_t vertexCount = sizeof(vertexBuffers) / sizeof(vertexBuffers[0]);
			m_vertexBuffer.Create(formatCount, formats, vertexCount, vertexBuffers);

			// インデックスバッファを作成
			m_pIndexBuffers = new buffer::IndexBuffer[1];
			uint16_t index[] = {
				0, 1, 2, 3,
				2, 3, 6, 7,
				6, 7, 4, 5,
				4, 5, 0, 1,
				1, 5, 3, 7,
				4, 0, 6, 2,
			};
			m_pIndexBuffers[0].Create(sizeof(uint16_t), 4 * 6, index);

			// サンプラ
			m_samplerState.Create(D3D11_TEXTURE_ADDRESS_WRAP, D3D11_FILTER_MIN_MAG_MIP_LINEAR);

			// コンスタントバッファ
			m_constantBuffer.Create(sizeof(ConstantBufferFormat));



			m_prepared = true;
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	立方体描画
		 * @param	pContext	描画コンテキスト
		 * @param	pMaterial	外部指定のマテリアル
		 * @return
		 */
		void Cube::Draw(ID3D11DeviceContext* pContext) {

			// 更新がある時のみ
			{
				// コンスタントバッファ
				ConstantBufferFormat cBuffer;

				// ワールド変換行列
				math::MatrixIdentity(&cBuffer.toWorld);
				//cBuffer.toWorld.m[3][0] = -0.5f;
				//cBuffer.toWorld.m[3][1] = -0.5f;
				math::MatrixTranspose(&cBuffer.toWorld, &cBuffer.toWorld);
				// ローカル変換行列

				// コンスタントバッファへコピー
				D3D11_MAPPED_SUBRESOURCE	pData;
				ID3D11Buffer*				pBuffer = m_constantBuffer.GetBuffer();

				if (SUCCEEDED(pContext->Map(pBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &pData))) {
					memcpy(pData.pData, (void*)&cBuffer, sizeof(ConstantBufferFormat));
					pContext->Unmap(pBuffer, 0);
				}
			}

			// シェーダーにコンスタントバッファデータを渡す
			ID3D11Buffer* p[1] = { m_constantBuffer.GetBuffer() };
			pContext->VSSetConstantBuffers(0, 1, p);
			pContext->PSSetConstantBuffers(0, 1, p);

			// 頂点バッファ設定
			auto vertex = GetVertexBuffer();
			auto buffer = vertex->GetBuffer();
			UINT stride = vertex->Stride();
			UINT offset = vertex->Offset();
			pContext->IASetVertexBuffers(0, 1, &buffer, &stride, &offset);

			// プリミティブトポロジー設定
			pContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP);

			// インデックスバッファ設定
			auto index			= GetIndexBuffer();
			auto indexBuffer	= index->GetBuffer();
			pContext->IASetIndexBuffer(indexBuffer, index->GetDXGIFormat(), 0);

			// マテリアル設定
			if (m_pMaterial)	{ m_pMaterial->SetCommand(pContext); }
			else				{ m_pMaterials[0]->SetCommand(pContext); }

			// サンプラーステート設定
			ID3D11SamplerState*			pSampler[1] = { m_samplerState.GetSamplerState() };
			pContext->PSSetSamplers(0, 1, pSampler);

			// インデックスバッファを利用した描画
			pContext->DrawIndexed(index->GetCount(), 0, 0);
		}
	}
}
