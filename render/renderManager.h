﻿#pragma once

#include "singleton.h"
#include "callback.h"
#include "renderBase.h"

namespace core {

	namespace render {

		//---------------------------------------------------------------------------------
		/**
		 * @brief
		 * 描画管理クラス
		 */
		class RenderManager : public util::Singleton<RenderManager> {
		private:
			friend class util::Singleton<RenderManager>;

		private:
			class cImpl;
			std::shared_ptr<cImpl> m_pImpl;	///< インプリメントクラスポインタ

		public:
			//---------------------------------------------------------------------------------
			/**
			 * @brief	描画コマンド設定のコールバック関数のフォーマット宣言
			 */
			using CallBackFunc = core::util::CallBack<void, ID3D11DeviceContext*, void*>;

			//---------------------------------------------------------------------------------
			/**
			 * @brief
			 * 描画コマンド内容
			 */
			struct Command {
				CallBackFunc		func;			///< コールバック関数
				void*				pParam;			///< 付加パラメータのポインタ

				Command()
				{
					pParam	= nullptr;
					func	= nullptr;
				}
			};

		private:
			//---------------------------------------------------------------------------------
			/**
			 * @brief	コンストラクタ
			 */
			RenderManager();

			//---------------------------------------------------------------------------------
			/**
			 * @brief	デストラクタ
			 */
			virtual ~RenderManager();

		public:
			//---------------------------------------------------------------------------------
			/**
			 * @brief	ワーク関数を開始する
			 */
			void StartWorker();

			//---------------------------------------------------------------------------------
			/**
			 * @brief	描画コマンド処理スレッドのワーク関数
			 * @param	pContext		描画コンテキスト
			 * @return
			 */
			void Worker(ID3D11DeviceContext* pContext);

			//---------------------------------------------------------------------------------
			/**
			 * @brief	描画コマンド設定の開始
			 * @return
			 */
			void Begin();


			//---------------------------------------------------------------------------------
			/**
			 * @brief	描画コマンドの設定
			 * @param	func		描画関数
			 * @param	pParam		付加パラメータのポインタ
			 * @return
			 */
			void SetCommand(CallBackFunc func, void* pParam);

			//---------------------------------------------------------------------------------
			/**
			 * @brief	描画コマンドの設定
			 * @param	Render		描画処理インスタンスのポインタ
			 * @param	pParam		付加パラメータのポインタ
			 * @return
			 */
			void SetCommand(Render* pRender, void* pParam);

			//---------------------------------------------------------------------------------
			/**
			 * @brief	描画コマンド設定の終了
			 * @return
			 */
			void End();

			//---------------------------------------------------------------------------------
			/**
			 * @brief	描画対象オブジェクトの処理を待つ（GPU待ちも込み）
			 * @return
			 */
			void WaitFinish();

		};
	}
};
