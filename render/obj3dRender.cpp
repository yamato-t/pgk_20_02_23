﻿#include "obj3dRender.h"
#include "../win/window.h"
#include "../input/input.h"

#include "../3d/plane.h"
#include "../3d/instance.h"
#include "../3d/mesh.h"

#include "../screen/screen.h"
#include "callback.h"

namespace core {

	namespace render {

		using namespace objScreen;

		//---------------------------------------------------------------------------------
		/**
		 * @brief	コンストラクタ
		 */
		Obj3DRender::Obj3DRender() {
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	デストラクタ
		 */
		Obj3DRender::~Obj3DRender() {
			Screen::Release();
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	3Dオブジェクト描画処理の初期化
		 */
		void Obj3DRender::Initialize() {
			// アプリケーションレンダーターゲットの作成
			m_colorTarget.Create(WINDOW_WIDTH, WINDOW_HEIGHT, DXGI_FORMAT_R8G8B8A8_UNORM);

			// 法線
			m_normalTarget.Create(WINDOW_WIDTH, WINDOW_HEIGHT, DXGI_FORMAT_R8G8B8A8_UNORM);

			// シャドウマップ
			m_shadowMapTarget.Create(WINDOW_WIDTH, WINDOW_HEIGHT, DXGI_FORMAT_R32G32_FLOAT);

			// サンプラ
			m_samplerState.Create(D3D11_TEXTURE_ADDRESS_CLAMP, D3D11_FILTER_MIN_MAG_LINEAR_MIP_POINT);

			// 深度ステンシルの作成
			m_depthStencil.Create(WINDOW_WIDTH, WINDOW_HEIGHT);

			// スクリーン描画処理の初期化
			Screen::Build();
			Screen::Instance().Initialize();

			// シャドウマップシェーダ(Mesh頂点フォーマットに合わせる)
			std::vector<buffer::VertexBuffer::format> formats;
			formats.push_back(buffer::VertexBuffer::format::pos);
			formats.push_back(buffer::VertexBuffer::format::normal);
			formats.push_back(buffer::VertexBuffer::format::uv);
			std::string path = "resource/hlsl/lightDepth.hlsl";
			m_shadowMap.SetShader(path, formats);
			m_shadowMap.SetBlend(render::RenderState::blendState::default);
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	3Dオブジェクト描画処理のリセット
		 * @return
		 */
		void Obj3DRender::Reset() {
			// スクリーン描画をリセット
			Screen::Instance().Reset();
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	3Dオブジェクト描画物の更新
		 * @return
		 */
		void Obj3DRender::Update()
		{
			// 3Dカメラの更新
			{
				vec3 pos	= m_camera.Position();
				vec3 target = m_camera.Target();

				if(input::Input::Instance().GetKey(VK_UP))			{ pos.y += 0.1f;} //target.y += 0.1f;}
				else if(input::Input::Instance().GetKey(VK_DOWN))	{ pos.y -= 0.1f;} //target.y -= 0.1f;}
				else if(input::Input::Instance().GetKey(VK_LEFT))	{ pos.x -= 0.1f;} //target.x -= 0.1f;}
				else if(input::Input::Instance().GetKey(VK_RIGHT))	{ pos.x += 0.1f;} //target.x += 0.1f;}
				else if(input::Input::Instance().GetKey(VK_HOME))	{ pos.z -= 0.1f;} //target.z -= 0.1f;}
				else if(input::Input::Instance().GetKey(VK_END))	{ pos.z += 0.1f;} //target.z += 0.1f;}

				m_camera.Position(pos);
				m_camera.Target(target);

				// ビュー行列とプロジェクション行列の更新
				m_camera.UpdateViewProjection();
			}

			// ライトの更新
			{
				vec3 dir	= m_dirLight.Dir();
				if(input::Input::Instance().GetKey('W'))			{ dir.z += 0.1f;} //target.y += 0.1f;}
				else if(input::Input::Instance().GetKey('A'))		{ dir.x += 0.1f;} //target.y -= 0.1f;}
				else if(input::Input::Instance().GetKey('D'))		{ dir.x -= 0.1f;} //target.x -= 0.1f;}
				else if(input::Input::Instance().GetKey('S'))		{ dir.z -= 0.1f;} //target.x += 0.1f;}

				m_dirLight.Dir(dir);
			}

			// オブジェクトの更新
			auto drawObj = obj::DrawObjContainer::Instance().GetDrawObj(obj::DrawObjContainer::layer::opaque);
			for(auto o : drawObj) {

				if (o->Prepared()) {
					o->Update();
				}
			}

			// オブジェクトの距離を見て、レイヤー別にソートする
		}


		//---------------------------------------------------------------------------------
		/**
		 * @brief	3Dオブジェクトの描画コマンド設定
		 *			別スレッドから呼ばれている事に注意
		 * @return
		 */
		void Obj3DRender::OnDraw(ID3D11DeviceContext* pContext) {

			// 平行光源コンスタントバッファ更新
			m_dirLight.UpdateConstantBuffer(pContext);

			auto drawObj = obj::DrawObjContainer::Instance().GetDrawObj(obj::DrawObjContainer::layer::opaque);

			// シャドウマップパス
			//{
			//	// ビューポート設定
			//	m_state.SetViewport(pContext, vec2(0,0), vec2(WINDOW_WIDTH, WINDOW_HEIGHT));

			//	// カラークリア
			//	float  clearColor[4] = { 0.0f, 0.0f, 0.0f, 1.0f };
			//	pContext->ClearRenderTargetView(m_shadowMapTarget.GetRenderTargetView(), clearColor);

			//	// レンダーターゲット
			//	IRenderTargetView* targets[] = {
			//		m_shadowMapTarget.GetRenderTargetView()
			//	};
			//	pContext->OMSetRenderTargets(1, targets, m_depthStencil.GetDepthStencilView());

			//	// 深度ステンシルクリア
			//	pContext->ClearDepthStencilView(m_depthStencil.GetDepthStencilView(), D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0);

			//	// オブジェクトの描画
			//	for(auto o : drawObj) {

			//		if (o->Prepared()) {
			//			o->SetMaterial(&m_shadowMap);
			//			o->Draw(pContext);
			//			o->SetMaterial(nullptr);
			//		}
			//	}
			//}

			// レンダーターゲットの変更
			IRenderTargetView* targets[] = {
				m_colorTarget.GetRenderTargetView(),
				m_normalTarget.GetRenderTargetView()
			};
			pContext->OMSetRenderTargets(2, targets, m_depthStencil.GetDepthStencilView());

			// シャドウマップ設定
			//{
			//	// シャドウマップをシェーダリソースに設定（レンダーターゲットから外した後）
			//	IShaderResourceView* pRes[] =  { m_shadowMapTarget.GetTexture()->GetTextureView() };
			//	pContext->PSSetShaderResources(10, 1, pRes);
			//	// サンプラーステート設定
			//	ID3D11SamplerState*			pSampler[1] = { m_samplerState.GetSamplerState() };
			//	pContext->PSSetSamplers(10, 1, pSampler);
			//}

			// 3D描画パス
			{
				// ビューポート設定
				m_state.SetViewport(pContext, vec2(0,0), vec2(WINDOW_WIDTH, WINDOW_HEIGHT));

				// 3Dカメラコンスタントバッファ更新
				m_camera.UpdateConstantBuffer(pContext);

				// カラークリア
				float  clearColor[4] = { 0.1f, 0.1f, 0.1f, 1.0f };
				pContext->ClearRenderTargetView(m_colorTarget.GetRenderTargetView(), clearColor);
				pContext->ClearRenderTargetView(m_normalTarget.GetRenderTargetView(), clearColor);

				// 深度ステンシルクリア
				pContext->ClearDepthStencilView(m_depthStencil.GetDepthStencilView(), D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0);

				// オブジェクトの描画
				for(auto o : drawObj) {
					if (o->Prepared()) {
						o->Draw(pContext);
					}
				}
			}

			// スクリーン貼り付け
			{
				// ビューポート設定
				m_state.SetViewport(pContext, vec2(0,0), vec2(WINDOW_WIDTH, WINDOW_HEIGHT));

				// レンダーターゲットをバックバッファに戻す
				dx11::dx11Device::Instance().ResetRenderTarget(pContext);

				// 描画設定
				m_state.SetBlendState(pContext, render::RenderState::blendState::default);
				render::RenderState::DepthStencilState depthStencil;
				depthStencil.m_isWriteZ = depthStencil.m_isZTest = false;
				m_state.SetDepthStencil(pContext, depthStencil);

				// カラーレンダーターゲットの内容を描画する
				Screen::Instance().DrawTexture(pContext, m_colorTarget.GetTexture());
			}
		}
	}
}
