﻿#include "obj2dRender.h"

#include "../win/window.h"

#include "../2d/sprite.h"

namespace core {

	namespace render {

		//---------------------------------------------------------------------------------
		/**
		 * @brief	コンストラクタ
		 */
		Obj2DRender::Obj2DRender() {

		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	デストラクタ
		 */
		Obj2DRender::~Obj2DRender() {

		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	2Dオブジェクト描画処理の初期化
		 */
		void Obj2DRender::Initialize() {
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	2Dオブジェクト描画処理のリセット
		 */
		void Obj2DRender::Reset() {
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	2Dオブジェクト描画物の更新
		 * @return
		 */
		void Obj2DRender::Update()
		{
			// 2Dカメラの更新
			{
				m_cameraOrtho.Position(vec3(0.0f, 1.5f, -4.5f));
				m_cameraOrtho.Target(vec3(0.0f, 0.0f, 0.0f));
				// スクリーン投影設定に更新
				m_cameraOrtho.UpdateScreenViewProjection();
			}
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	2Dオブジェクトの描画コマンド設定
		 *			別スレッドから呼ばれている事に注意
		 * @return
		 */
		void Obj2DRender::OnDraw(ID3D11DeviceContext* pContext) {

			// ビューポート設定
			m_state.SetViewport(pContext, vec2(0,0), vec2(WINDOW_WIDTH, WINDOW_HEIGHT));

			// カメラコンスタントバッファ更新
			m_cameraOrtho.UpdateConstantBuffer(pContext);

			// 描画設定
			m_state.SetBlendState(pContext, render::RenderState::blendState::default);
			render::RenderState::DepthStencilState depthStencil;
			m_state.SetDepthStencil(pContext, depthStencil);

			// オブジェクトの描画
			auto drawObj = obj::DrawObjContainer::Instance().GetDrawObj(obj::DrawObjContainer::layer::ui);
			for(auto o : drawObj) {
				if (o->Prepared()) {
					o->Draw(pContext);
				}
			}
		}
	}
}
