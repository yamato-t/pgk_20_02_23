﻿#include "thread.h"
#include "../dx11/dx11.h"
#include "../win/window.h"

#include "renderManager.h"

namespace core {

	namespace render {

		//---------------------------------------------------------------------------------
		/**
		 * @brief
		 * 描画コマンドの変更処理を排他制御化する
		 */
		static std::mutex s_mutex;

		//---------------------------------------------------------------------------------
		/**
		 * @brief	描画管理スレッドのワーク関数
		 * @param	arg			描画コンテキスト
		 * @return
		 */
		static uint32_t __stdcall RenderWorker(void* arg)
		{
			ID3D11DeviceContext* pContext = static_cast<ID3D11DeviceContext*>(arg);
			RenderManager::Instance().Worker(pContext);
			return 0;
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief
		 * 描画管理のインプリメントクラス
		 */
		class RenderManager::cImpl {
		public:
			util::Thread			m_thread;			///< 描画管理スレッド
			std::vector<Command>	m_commands;			///< 描画コマンドのコンテナ

			void*					m_beginCommand;		///< 描画コマンド設定開始を表すイベント
			void*					m_finish;			///< 描画管理スレッドの終了を表すイベント

			bool					m_destory;			///< 描画管理の終了フラグ

		public:
			//---------------------------------------------------------------------------------
			/**
			 * @brief	コンストラクタ
			 */
			cImpl()
			: m_destory(false){
			}

			//---------------------------------------------------------------------------------
			/**
			 * @brief	デストラクタ
			 */
			~cImpl() {
			}


			//---------------------------------------------------------------------------------
			/**
			 * @brief	描画管理スレッドの開始
			 */
			void StartWorker()
			{
				m_thread.Start(RenderWorker, dx11::dx11Device::Instance().DeviceContext());

				m_beginCommand = CreateEvent(nullptr, false, true, "BeginCommand");
				m_finish = CreateEvent(nullptr, false, false, "Finish");
			}

			//---------------------------------------------------------------------------------
			/**
			 * @brief	描画管理スレッドのワーク関数
			 *			追加された描画コマンドの設定を行う
			 *
			 * @param	pContext		描画コンテキスト
			 * @return
			 */
			void Worker(ID3D11DeviceContext* pContext) {

				while (true) {
					std::vector<Command> tempCommand;

					{
						// クリティカルセクション
						std::lock_guard<std::mutex> lock(s_mutex);

						// コマンドコピー
						tempCommand.swap(m_commands);
					}

					// コピーの実行
					for (auto& c : tempCommand) {
						c.func(pContext, c.pParam);
					}

					// 終了処理が呼び出されているなら即スレッドを終了させる
					if(m_destory) { break; }
				}

				// スレッドの終了イベントを発生させる
				SetEvent(m_finish);
			}

			//---------------------------------------------------------------------------------
			/**
			 * @brief	描画コマンドを追加
			 * @param	command		描画コマンド
			 * @return
			 */
			void Add(const Command& command) {
				// クリティカルセクション
				std::lock_guard<std::mutex> lock(s_mutex);
				m_commands.push_back(command);
			}

			//---------------------------------------------------------------------------------
			/**
			 * @brief	Endコマンド処理をメインスレッドで待つ
			 * @return
			 */
			void WaitEndCommand() {
				// コマンドの開始イベントが発生するまで待つ
				WaitForSingleObject(m_beginCommand, INFINITE);

				// イベントを停止させる
				ResetEvent(m_beginCommand);
			}


			//---------------------------------------------------------------------------------
			/**
			 * @brief	描画コマンド設定の開始
			 * @param	pContext	描画コンテキスト
			 * @return
			 */
			void Begin(ID3D11DeviceContext* pContext, void* pParam) {

				// 最初にコンテキストのクリア
				pContext->ClearState();

				// バックバッファのクリア
				dx11::dx11Device::Instance().Clear(pContext);

				dx11::dx11Device::Instance().ResetRenderTarget(pContext);
			}

			//---------------------------------------------------------------------------------
			/**
			 * @brief	描画コマンド設定の終了
			 * @param	pContext	描画コンテキスト
			 * @return
			 */
			void End(ID3D11DeviceContext* pContext, void* pParam) {
				dx11::dx11Device::Instance().Present();

				// コマンド開始イベントを発生させる
				SetEvent(m_beginCommand);
			}

			//---------------------------------------------------------------------------------
			/**
			 * @brief	描画管理全体を終了させる
			 * @return
			 */
			void Finish() {
				m_destory = true;
			}
		};

		using namespace std::placeholders;

		//---------------------------------------------------------------------------------
		/**
         * @brief	コンストラクタ
		 */
		RenderManager::RenderManager() {
			m_pImpl.reset(new RenderManager::cImpl());
		}

		//---------------------------------------------------------------------------------
		/**
         * @brief	デストラクタ
		 */
		RenderManager::~RenderManager() {

		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	描画コマンド処理スレッドの開始
		 */
		void RenderManager::StartWorker()
		{
			m_pImpl->StartWorker();
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	描画コマンド処理スレッドのワーク関数
		 * @param	pContext		描画コンテキスト
		 * @return
		 */
		void RenderManager::Worker(ID3D11DeviceContext* pContext) {
			m_pImpl->Worker(pContext);
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	描画コマンド設定の開始
		 * @return
		 */
		void RenderManager::Begin() {

			// Endコマンドを待つ
			m_pImpl->WaitEndCommand();

			Command command;
			command.func = std::bind(&cImpl::Begin, m_pImpl, _1, _2);
			m_pImpl->Add(command);
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	描画コマンドの設定
		 * @param	func		描画関数
		 * @param	pParam		付加パラメータのポインタ
		 * @return
		 */
		void RenderManager::SetCommand(CallBackFunc func, void* pParam) {

			Command command;
			command.func	= func;
			command.pParam	= pParam;
			m_pImpl->Add(command);
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	描画コマンドの設定
		 * @param	Render		描画処理インスタンスのポインタ
		 * @param	pParam		付加パラメータのポインタ
		 * @return
		 */
		void RenderManager::SetCommand(Render* pRender, void* pParam) {
			Command command;
			command.func = std::bind(&Render::SetCommand, pRender, _1, _2);
			command.pParam = pParam;
			m_pImpl->Add(command);
		}


		//---------------------------------------------------------------------------------
		/**
		 * @brief	描画コマンド設定の終了
		 * @return
		 */
		void RenderManager::End() {
			Command command;
			command.func = std::bind(&cImpl::End, m_pImpl, _1, _2);
			m_pImpl->Add(command);
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	描画対象オブジェクトの処理を待つ（GPU待ちも込み）
		 * @return
		 */
		void RenderManager::WaitFinish() {
			m_pImpl->Finish();

			// ここでスレッドの終了を待つ
			WaitForSingleObject(m_pImpl->m_finish, INFINITE);

			// GPU処理待ちも必要
			dx11::dx11Device::Instance().WaitGPU();
		}

	}
}
