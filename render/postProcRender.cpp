﻿#include "postProcRender.h"

namespace core {

	namespace render {

		//---------------------------------------------------------------------------------
		/**
		 * @brief	デストラクタ
		 */
		PostProcRender::~PostProcRender()
		{
			m_pProcs.clear();
		}


		//---------------------------------------------------------------------------------
		/**
		 * @brief	ポストの初期化
		 * @return
		 */
		void PostProcRender::Initialize() {

		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	リセット
		 */
		void PostProcRender::Reset() {

		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	描画オブジェクトの登録
		 * @param	pProc プロセス
		 * @return
		 */
		void PostProcRender::Register(obj::Proc* pProc) {
			m_pProcs.push_back(pProc);
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	更新
		 * @return
		 */
		void PostProcRender::Update()
		{
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	ポスト描画
		 *			別スレッドから呼ばれている事に注意
		 * @return
		 */
		void PostProcRender::OnDraw(ID3D11DeviceContext* pContext) {

			for (auto proc : m_pProcs)
			{
				proc->Draw(pContext);
			}
		}
	}
}
