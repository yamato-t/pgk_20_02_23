#include "renderState.h"

#include "../dx11/dx11.h"

namespace core {

	namespace render {

		//---------------------------------------------------------------------------------
		/**
		 * @brief	不透明設定
		 * @param	renderTarget（in/out）	レンダーターゲット別ブレンド設定構造体
		 * @return	
		 */
		void BleandDefault(D3D11_RENDER_TARGET_BLEND_DESC& renderTarget) {

			renderTarget.BlendEnable			= 0;
			renderTarget.SrcBlend				= D3D11_BLEND_ONE;
			renderTarget.DestBlend				= D3D11_BLEND_ZERO;
			renderTarget.BlendOp				= D3D11_BLEND_OP_ADD;
			renderTarget.SrcBlendAlpha			= D3D11_BLEND_ONE;
			renderTarget.DestBlendAlpha			= D3D11_BLEND_ZERO;
			renderTarget.BlendOpAlpha			= D3D11_BLEND_OP_ADD;
			renderTarget.RenderTargetWriteMask	= D3D11_COLOR_WRITE_ENABLE_ALL;			
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	アルファブレンド設定
		 * @param	renderTarget（in/out）	レンダーターゲット別ブレンド設定構造体
		 * @return	
		 */
		void BleandAlpha(D3D11_RENDER_TARGET_BLEND_DESC& renderTarget) {

			renderTarget.BlendEnable			= 1;
			renderTarget.SrcBlend				= D3D11_BLEND_SRC_ALPHA;
			renderTarget.DestBlend				= D3D11_BLEND_INV_SRC_ALPHA;
			renderTarget.BlendOp				= D3D11_BLEND_OP_ADD;
			renderTarget.SrcBlendAlpha			= D3D11_BLEND_ONE;
			renderTarget.DestBlendAlpha			= D3D11_BLEND_ZERO;
			renderTarget.BlendOpAlpha			= D3D11_BLEND_OP_ADD;
			renderTarget.RenderTargetWriteMask	= D3D11_COLOR_WRITE_ENABLE_ALL;
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	加算設定
		 * @param	renderTarget（in/out）	レンダーターゲット別ブレンド設定構造体
		 * @return	
		 */
		void BleandAdd(D3D11_RENDER_TARGET_BLEND_DESC& renderTarget) {

			renderTarget.BlendEnable			= 1;
			renderTarget.SrcBlend				= D3D11_BLEND_SRC_ALPHA;	// アルファで加算強度を設定
			renderTarget.DestBlend				= D3D11_BLEND_ONE;
			renderTarget.BlendOp				= D3D11_BLEND_OP_ADD;
			renderTarget.SrcBlendAlpha			= D3D11_BLEND_ONE;
			renderTarget.DestBlendAlpha			= D3D11_BLEND_ZERO;
			renderTarget.BlendOpAlpha			= D3D11_BLEND_OP_ADD;
			renderTarget.RenderTargetWriteMask	= D3D11_COLOR_WRITE_ENABLE_ALL;
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	減算設定
		 * @param	renderTarget（in/out）	レンダーターゲット別ブレンド設定構造体
		 * @return	
		 */
		void BleandSub(D3D11_RENDER_TARGET_BLEND_DESC& renderTarget) {

			renderTarget.BlendEnable			= 1;
			renderTarget.SrcBlend				= D3D11_BLEND_SRC_ALPHA;
			renderTarget.DestBlend				= D3D11_BLEND_ONE;
			renderTarget.BlendOp				= D3D11_BLEND_OP_REV_SUBTRACT;
			renderTarget.SrcBlendAlpha			= D3D11_BLEND_ONE;
			renderTarget.DestBlendAlpha			= D3D11_BLEND_ZERO;
			renderTarget.BlendOpAlpha			= D3D11_BLEND_OP_ADD;
			renderTarget.RenderTargetWriteMask	= D3D11_COLOR_WRITE_ENABLE_ALL;
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	乗算設定
		 * @param	renderTarget（in/out）	レンダーターゲット別ブレンド設定構造体
		 * @return	
		 */
		void BleandMulti(D3D11_RENDER_TARGET_BLEND_DESC& renderTarget) {

			renderTarget.BlendEnable			= 1;
			renderTarget.SrcBlend				= D3D11_BLEND_ZERO;
			renderTarget.DestBlend				= D3D11_BLEND_SRC_COLOR;
			renderTarget.BlendOp				= D3D11_BLEND_OP_ADD;
			renderTarget.SrcBlendAlpha			= D3D11_BLEND_ONE;
			renderTarget.DestBlendAlpha			= D3D11_BLEND_ZERO;
			renderTarget.BlendOpAlpha			= D3D11_BLEND_OP_ADD;
			renderTarget.RenderTargetWriteMask	= D3D11_COLOR_WRITE_ENABLE_ALL;
		}


		//---------------------------------------------------------------------------------
		/**
		 * @brief	コンストラクタ
		 */
		RenderState::RenderState(){}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	デストラクタ
		 */
		RenderState::~RenderState() {
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	ブレンドの設定
		 * @param	pContext		描画コンテキスト
		 * @param	blendState		ブレンド設定
		 * @return	
		 */
		void RenderState::SetBlendState(ID3D11DeviceContext* pContext, RenderState::blendState blendState) {
			using Func = void (*)(D3D11_RENDER_TARGET_BLEND_DESC& renderTarget);
			Func pBlendFunc[] = {
				&BleandDefault,
				&BleandAlpha,
				&BleandAdd,
				&BleandSub,
				&BleandMulti,
			};

			// ブレンドステート
			D3D11_BLEND_DESC descBlend;
			ZeroMemory(&descBlend, sizeof(D3D11_BLEND_DESC));
			descBlend.AlphaToCoverageEnable			= 0;
			descBlend.IndependentBlendEnable		= 1;

			D3D11_RENDER_TARGET_BLEND_DESC x;
			(pBlendFunc[blendState])(x);

			// レンダーターゲット数を参照して対応する必要がある
			descBlend.RenderTarget[0] = x;
			descBlend.RenderTarget[1] = x;

			dx11::ComPtr<ID3D11BlendState>	pBlendState;
			dx11::dx11Device::Instance().Device()->CreateBlendState(&descBlend, pBlendState.GetPointerPointer());
			pContext->OMSetBlendState(pBlendState.GetPointer(), D3DXVECTOR4( 0.0f, 0.0f, 0.0f, 0.0f ), 0xffffffff );			
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	深度ステンシルの設定
		 * @param	pContext		描画コンテキスト
		 * @param	depthStencil	深度ステンシル設定
		 * @return	
		 */
		void RenderState::SetDepthStencil(ID3D11DeviceContext* pContext, RenderState::DepthStencilState depthStencil) {

			D3D11_DEPTH_STENCIL_DESC ddsDesc;
			ZeroMemory( &ddsDesc, sizeof( ddsDesc ) );
			ddsDesc.DepthEnable					= depthStencil.m_isZTest;
			ddsDesc.DepthWriteMask				= depthStencil.m_isWriteZ ? D3D11_DEPTH_WRITE_MASK_ALL : D3D11_DEPTH_WRITE_MASK_ZERO;
			ddsDesc.DepthFunc					= D3D11_COMPARISON_LESS;

			ddsDesc.StencilEnable				= depthStencil.m_isStencilTest;
			ddsDesc.StencilReadMask				= depthStencil.m_stencilRead;
			ddsDesc.StencilWriteMask			= depthStencil.m_stencilWrite;
			ddsDesc.FrontFace.StencilPassOp		= D3D11_STENCIL_OP_REPLACE;
			ddsDesc.FrontFace.StencilFunc		= D3D11_COMPARISON_NOT_EQUAL;

			dx11::ComPtr<ID3D11DepthStencilState>	pDepthStencilState;	
			dx11::dx11Device::Instance().Device()->CreateDepthStencilState( &ddsDesc, pDepthStencilState.GetPointerPointer());			
			pContext->OMSetDepthStencilState(pDepthStencilState.GetPointer(), depthStencil.m_stencilRef);
		}


		//---------------------------------------------------------------------------------
		/**
		 * @brief	ビューポートの設定
		 * @param	pContext		描画コンテキスト
		 * @param	topLeft			ビューポート左上の位置
		 * @param	size			ビューポートサイズ
		 * @return	
		 */
		void RenderState::SetViewport(ID3D11DeviceContext* pContext, const vec2& topLeft, const vec2& size) {
			// ビューポートの設定
			D3D11_VIEWPORT vp;
			vp.TopLeftX = topLeft.x;
			vp.TopLeftY = topLeft.y;
			vp.Width	= size.x;
			vp.Height	= size.y;
			vp.MinDepth = 0.0f;
			vp.MaxDepth = 1.0f;
			pContext->RSSetViewports(1, &vp);
		}
	}
};