#include "allocator.h"
#include "debug.h"
#include <mutex>

/** @def
 * アロケータを利用するか否か
 */
#define USE_ALLOCATOR	(0)

/** @def
 * メモリ状況を表示するか否か
 */
#define DEBUG_MEMORY	(0)

namespace core {

	namespace util {
	

		//---------------------------------------------------------------------------------
		/**
		 * @brief	
		 * メモリプール
		 *
		 * アプリケーション全体で利用する為に確保されているメモリ領域の制御
		 */
		class MemoryPool {
		public:

			//---------------------------------------------------------------------------------
			/**
			 * @brief	コンストラクタ
			 */
			MemoryPool()
				: m_buffer(nullptr)
				, m_size(0) {
				// メモリリーク検出
				_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);

				Initialize(1024 * 1024 * 32);
			}

			//---------------------------------------------------------------------------------
			/**
			 * @brief	デストラクタ
			 */
			virtual ~MemoryPool() {
				Finalize();
			}

			//---------------------------------------------------------------------------------
			/**
			 * @brief	次の配置予定アドレスの取得
			 * @return	空きアドレス
			 */
			void* Next() {
				return (void*)m_next;
			}

			//---------------------------------------------------------------------------------
			/**
			 * @brief	次の配置予定アドレスの設定
			 * @param	address		配置予定アドレス
			 */
			void SetNext(uintptr_t address) {
				m_next = address;
			}


		private:
			//---------------------------------------------------------------------------------
			/**
			 * @brief	メモリ領域の確保
			 * @param	size		確保するサイズ
			 */
			void Initialize(size_t size) {
				m_buffer = malloc(size);
				memset(m_buffer, 0, size);
				m_next = (uintptr_t)m_buffer;
			}

			//---------------------------------------------------------------------------------
			/**
			 * @brief	終了処理
			 */
			void Finalize() {
				free(m_buffer);

				m_buffer = nullptr;
			}

		private:
			void*		m_buffer;	///< 確保済みメモリ領域
			size_t		m_size;		///< 確保サイズ
			uintptr_t	m_next;		///< 次の配置予定アドレス
		};

		//---------------------------------------------------------------------------------
		/**
		 * @brief	
		 * アロケータ
		 *
		 * メモリプールを利用したアロケート制御
		 * 配置アドレスにはヘッダ領域が追加されており、
		 * メモリの使用状況や検索の為の連結情報が保存されている
		 */
		class Allocator {

		private:
			//---------------------------------------------------------------------------------
			/**
			 * @brief	メモリ使用状況のヘッダ定義
			 */
			struct Header {
				intptr_t	use;	///< 使用中か否か
				char*		prev;	///< 前の配置アドレス
				char*		next;	///< 次の配置アドレス

				//---------------------------------------------------------------------------------
				/**
				 * @brief	コンストラクタ
				 */
				Header()
				{
					use				= 0xffffffff;
					prev			= nullptr;
					next			= nullptr;
				}
			};

			const size_t s_headerSize	= sizeof(Header);	///< ヘッダのサイズ

		public:
			//---------------------------------------------------------------------------------
			/**
			 * @brief	コンストラクタ
			 */
			Allocator() {
			}

			//---------------------------------------------------------------------------------
			/**
			 * @brief	デストラクタ
			 */
			~Allocator() {
			}

		public:

			//---------------------------------------------------------------------------------
			/**
			 * @brief	配置可能な空きアドレスの検索
			 * @param	size	配置サイズ
			 * @return	配置可能アドレス
			 */
			void* SearchAddress(size_t size) {
				// 配置予定アドレスを取得
				void* address = m_pool.Next();

				// 配置アドレスの選択と決定
				while(true) {
					Header* newHeader =	((Header*)address);

					// 利用中か否か
					if(newHeader->use <= 0) {
	
						// 空白サイズの計算
						uintptr_t blankSize = ((uintptr_t)newHeader->next - (uintptr_t)newHeader) - s_headerSize;

						if(size < blankSize || newHeader->next == nullptr) {
							// このアドレスに配置します
							break;
						}
					}

					// 次のアドレスで再チェック
					address = newHeader->next;
				}

				return address;
			}


			//---------------------------------------------------------------------------------
			/**
			 * @brief	割り当て
			 * @param	size	配置サイズ
			 * @return	配置アドレス
			 */
			void* Allocate(size_t size) {
				// ヘッダー部分を含めたサイズを取得する
				int sumAllocateSize = ALIGN(s_headerSize + size);
				auto p = SearchAddress(sumAllocateSize);

				// 次の情報があるか否か
				auto prevNextHeader = (Header*)(((Header*)p)->next);

				// 次の配置予定アドレスを保存する
				uintptr_t nextAddress	= (uintptr_t)p + sumAllocateSize;
				((Header*)p)->next		= (char*)(nextAddress);

				// 次の配置予定アドレスのヘッダーに現アドレスを保存させる
				auto nextHeader = (Header*)(((Header*)p)->next);
				nextHeader->prev = (char*)p;

				// 使用中設定
				((Header*)p)->use		= 1;

				// つなぎ直しが発生する
				if(prevNextHeader) {
					nextHeader->next		= (char*)prevNextHeader;
					prevNextHeader->prev	= (char*)nextHeader;
				}

				//  次のアドレスを設定しておく
				m_pool.SetNext(nextAddress);

				return (void*)((char*)(p) + s_headerSize);
			}

			//---------------------------------------------------------------------------------
			/**
			 * @brief	割り当て解除（開放）
			 * @param	p	開放するポインタ
			 * @return	
			 */
			void DeAllocate(void* p) {
				// 開放するポインタのヘッダ情報を取得
				auto header = (Header*)((char*)(p) - s_headerSize);

				// 未使用設定
				header->use = -1;

				// 次に確保するアドレスは、今開放した領域に
				m_pool.SetNext((uintptr_t)header);
			}

			//---------------------------------------------------------------------------------
			/**
			 * @brief	メモリデフラグ
			 * @return	
			 */
			void Defragmentation() {
			}

		private:
			MemoryPool m_pool;	///< メモリプール
		};


		//---------------------------------------------------------------------------------
		/**
		 * @brief	アロケータクラスのインスタンス取得
		 * @return	アロケータクラスのインスタンス
		 */
		Allocator&	AllocatorInstance() {
			static Allocator s_allocator;
			return s_allocator;
		}

	}
}

#if USE_ALLOCATOR


/** @brief
 * スレッドセーフの為のブロック
 */
std::mutex mutex;

//---------------------------------------------------------------------------------
/**
 * @brief	メモリ割り当て
 * @param	size	割り当てるサイズ
 * @return	割り当てられたアドレス
 */
void* operator new(size_t size) {
	std::lock_guard<std::mutex> lock(mutex);

	return core::util::AllocatorInstance().Allocate(size);
}

//---------------------------------------------------------------------------------
/**
 * @brief	メモリ割り当て
 * @param	size	割り当てるサイズ
 * @return	割り当てられたアドレス
 */
void* operator new[](size_t size) {
	std::lock_guard<std::mutex> lock(mutex);

	return core::util::AllocatorInstance().Allocate(size);
}

//---------------------------------------------------------------------------------
/**
 * @brief	メモリ開放
 * @param	ptr		開放するポインタ
 * @return	
 */
void operator delete(void* ptr) { 
	std::lock_guard<std::mutex> lock(mutex);

	core::util::AllocatorInstance().DeAllocate(ptr);
}

//---------------------------------------------------------------------------------
/**
 * @brief	メモリ開放
 * @param	ptr		開放するポインタ
 * @return	
 */
void operator delete[](void* ptr) {
	std::lock_guard<std::mutex> lock(mutex);

	core::util::AllocatorInstance().DeAllocate(ptr);
}

#if 0 // placement new/delete
// --------------------------------------------------------------
/**
*
*/
void* operator new(std::size_t, void* ptr) {
}

// --------------------------------------------------------------
/**
*
*/
void* operator new[](std::size_t, void* ptr) {
}

// --------------------------------------------------------------
/**
*
*/
void operator delete(void* ptr, void* p) {
}

// --------------------------------------------------------------
/**
*
*/
void operator delete[](void* ptr, void* p) {
}
#endif

#endif