﻿#pragma once

#include <unordered_map>

#include "singleton.h"

namespace core {

	namespace util {

		//---------------------------------------------------------------------------------
		/**
		 * @brief
		 * 時間計測クラス
		 *
		 * スコープ内でインスタンス化される事を想定
		 */
		class Time {
		public:
			//---------------------------------------------------------------------------------
			/**
			 * @brief	コンストラクタ（計測開始）
			 * @param	tag			識別タグ
			 */
			Time(const char* tag);

			//---------------------------------------------------------------------------------
			/**
			 * @brief	デストラクタ（計測終了）
			 */
			~Time();

		private:
			uint64_t		m_startTime;	///< 開始時間
			std::string		m_tag;			///< 識別タグ
		};


		//---------------------------------------------------------------------------------
		/**
		 * @brief
		 * 時間計測コンテナ
		 *
		 * シングルトンによる制御
		 */
		class TimeContainer : public Singleton<TimeContainer> {
		private:
			friend class Singleton<TimeContainer>;

		private:
			//---------------------------------------------------------------------------------
			/**
			 * @brief	コンストラクタ
			 */
			TimeContainer();

			//---------------------------------------------------------------------------------
			/**
			 * @brief	デストラクタ
			 */
			~TimeContainer();

		public:
			//---------------------------------------------------------------------------------
			/**
			 * @brief	計測対象の追加
			 * @param	tag			識別タグ
			 * @param	microsec	加算するマイクロ秒
			 * @return
			 */
			void Add(const std::string& tag, double microsec);

			//---------------------------------------------------------------------------------
			/**
			 * @brief	計測結果の表示
			 * @param	tag		表示する識別タグ（nullの場合はすべての識別タグを表示）
			 * @return
			 */
			void Print(char* tag = nullptr);

		private:
			std::unordered_map<std::string, double>	m_container;	///< 識別タグと計測時間のコンテナ
		};
	}
}


#define TIME_CONTAINER_VALID()	(core::util::TimeContainer::IsValid())
#define TIME_CONTAINER()		(core::util::TimeContainer::Instance())

#if DEBUG
#define TIME_PRINT(tag)			(core::util::TimeContainer::Instance().Print(tag))
#define TIME_CHECK_SCORP(tag)	core::util::Time timeCheck(tag)
#else
#define TIME_PRINT(tag)
#define TIME_CHECK_SCORP(tag)
#endif
