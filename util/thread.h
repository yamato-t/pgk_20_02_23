#pragma once

#include <thread>

/** @def
 * ウィンドウズスレッドか否か
 */
#define WINDOWS_THREAD (1) 

namespace core {

	namespace util {

#if WINDOWS_THREAD 
		typedef unsigned(__stdcall WorkerFunc)(void*);
#else
		typedef unsigned (WorkerFunc)(void*);
#endif

		//---------------------------------------------------------------------------------
		/**
		 * @brief	
		 * スレッド制御クラス
		 *
		 * 
		 */
		class Thread {
		private:
			class cImpl;
			std::shared_ptr<cImpl> m_pImpl;

		public:
			//---------------------------------------------------------------------------------
			/**
			 * @brief	コンストラクタ
			 */
			Thread();

			//---------------------------------------------------------------------------------
			/**
			 * @brief	デストラクタ
			 */
			~Thread();

			//---------------------------------------------------------------------------------
			/**
			 * @brief	スレッドの開始
			 * @param	func		スレッドのワーカー関数
			 * @param	arg			スレッドに渡す情報
			 * @param	stackSize	スレッド側で利用できる最大スタックメモリサイズ
			 * @return	
			 */
			void Start(WorkerFunc func, void* arg, size_t stackSize = 4096);

			//---------------------------------------------------------------------------------
			/**
			 * @brief	スレッドの終了待機
			 * @return	
			 */
			void Wait();
		};
	}
}
