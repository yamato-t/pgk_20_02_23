﻿#include "time.h"
#include "debug.h"
#include <chrono>

namespace core {
	namespace util {
		using namespace std;

		//---------------------------------------------------------------------------------
		/**
		 * @brief	コンストラクタ（計測開始）
		 * @param	tag			識別タグ
		 */
		Time::Time(const char* tag) {
			if(!TIME_CONTAINER_VALID()) {
				TimeContainer::Build();
			}

			auto time		= chrono::system_clock::now().time_since_epoch();
			m_startTime		= chrono::duration_cast<chrono::microseconds>(time).count();

			m_tag		= string(tag);
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	デストラクタ（計測終了）
		 */
		Time::~Time() {
			auto time		= chrono::system_clock::now().time_since_epoch();
			auto endTime	= chrono::duration_cast<chrono::microseconds>(time).count();

			auto microsec = endTime - m_startTime;
			TIME_CONTAINER().Add(m_tag.c_str(), microsec);
		}


		//---------------------------------------------------------------------------------
		/**
		 * @brief	コンストラクタ
		 */
		TimeContainer::TimeContainer() {
			m_container.clear();
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	デストラクタ
		 */
		TimeContainer::~TimeContainer() {
			m_container.clear();
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	計測対象の追加
		 * @param	tag			識別タグ
		 * @param	microsec	加算するマイクロ秒
		 * @return
		 */
		void TimeContainer::Add(const std::string& tag, double microsec) {
			auto& it = m_container.find(tag);
			if(it == m_container.end()) {
				m_container.insert(std::make_pair(tag, microsec));
			} else {
				it->second += microsec;
			}
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	計測結果の表示
		 * @param	tag		表示する識別タグ（nullの場合はすべての識別タグを表示）
		 * @return
		 */
		void TimeContainer::Print(char* tag) {
			if(tag) {
				auto it = m_container.find(string(tag));
				if(it != m_container.end()) {
					trace("tag [ %s ] : millisec [ %f ]", it->first.c_str(), it->second / 1000.0f);
				}
			} else {
				for(auto& x : m_container) {
					trace("tag [ %s ] : millisec [ %f ]", x.first.c_str(), x.second / 1000.0f);
				}
			}
		}
	}
}
