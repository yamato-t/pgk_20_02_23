﻿#pragma once

#include "debug.h"

namespace core {

	namespace util {

		//---------------------------------------------------------------------------------
		/**
		 * @brief
		 * シングルトン化制御
		 *
		 * このクラスを継承したクラスをシングルトン化させる
		 */
		template <class T>
		class Singleton {

		public:
			typedef T* instance_ptr;

		public:
			//---------------------------------------------------------------------------------
			/**
			 * @brief	シングルトンインスタンスの作成
			 */
			static void Build() {
				if (s_instance != nullptr) {
					Release();
				}
				s_instance = CreateInstance();
			}

			//---------------------------------------------------------------------------------
			/**
			 * @brief	シングルトンインスタンスの取得
			 * @return	シングルトンインスタンス
			 */
			static T& Instance() {
				assert(s_instance, "インスタンスがありません");
				return *s_instance;
			}

			//---------------------------------------------------------------------------------
			/**
			 * @brief	シングルトンインスタンスの開放
			 */
			static void Release() {
				assert(s_instance, "インスタンスがありません");
				delete s_instance;
				s_instance = nullptr;
			}

			static bool IsValid() {
				return (s_instance != nullptr);
			}


		protected:
			//---------------------------------------------------------------------------------
			/**
			 * @brief	コンストラクタ
			 */
			Singleton() {}

		private:
			//---------------------------------------------------------------------------------
			/**
			 * @brief	シングルトンインスタンスの生成
			 * @return	シングルトンインスタンス
			 */
			inline static T* CreateInstance() { return new T();  }

		private:
			// コピーコンストラクタや代入を許可しない
			Singleton(const Singleton& r)				= delete;
			Singleton& operator=(const Singleton& r)	= delete;
			Singleton(Singleton&& r)					= delete;
			Singleton& operator=(Singleton && r)		= delete;

		private:
			static instance_ptr s_instance;		///< インスタンス
		};

		template <class T> typename Singleton<T>::instance_ptr Singleton<T>::s_instance;
	}
}

