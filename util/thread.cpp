#include "thread.h"
#include "debug.h"
#include <process.h>

namespace core {
	namespace util {

		using namespace std;

#if WINDOWS_THREAD
		//---------------------------------------------------------------------------------
		/**
		 * @brief	
		 * スレッド制御インプリメントクラス
		 */
		class Thread::cImpl {
		public:
			//---------------------------------------------------------------------------------
			/**
			 * @brief	コンストラクタ
			 */
			cImpl()
			: m_hThread(nullptr) {
				m_hThread = nullptr;
			}

			//---------------------------------------------------------------------------------
			/**
			 * @brief	デストラクタ
			 */
			~cImpl() {
				if (m_hThread) {
					trace("wait end thread");
					Wait();
				}
			}

			//---------------------------------------------------------------------------------
			/**
			 * @brief	スレッドの開始
			 * @param	func		スレッドのワーカー関数
			 * @param	arg			スレッドに渡す情報
			 * @param	stackSize	スレッド側で利用できる最大スタックメモリサイズ
			 * @return	
			 */
			void Start(WorkerFunc func, void* arg, size_t stackSize) {
				assert(!m_hThread, "already create thread");

				m_hThread = (HANDLE)_beginthreadex(nullptr,
					stackSize,
					func,
					arg,
					0/*CREATE_SUSPENDED*/,	// 即開始
					nullptr);
				assert(m_hThread, "fail create thread");
			}

			//---------------------------------------------------------------------------------
			/**
			 * @brief	スレッドの終了待機
			 * @return	
			 */
			void Wait() {
				WaitForSingleObject(m_hThread, INFINITE);
				m_hThread = nullptr;
			}
		private:
			HANDLE							m_hThread;	///< スレッドハンドル
		};
#else
		// --------------------------------------------------------------
		/**
		*	スレッドインプリメントクラス
		*	stdスレッド
		*/
		class Thread::cImpl {
		public:
			// --------------------------------------------------------------
			/**
			*
			*/
			cImpl() {
				m_pThread.reset();
			}

			// --------------------------------------------------------------
			/**
			*
			*/
			~cImpl() {
				if (m_pThread) {
					trace("wait end thread");
					m_pThread->join();
					m_pThread.reset();
				}
			}

			// --------------------------------------------------------------
			/**
			*
			*/
			void Start(WorkerFunc func, void* arg, size_t stackSize) {
				assert(!m_pThread, "always started thread");
				// スレッド開始
				m_pThread.reset(new std::thread(func, arg));
			}

			// --------------------------------------------------------------
			/**
			*
			*/
			void Wait() {

				if (m_pThread) {
					assert(m_pThread->joinable(), "always end thread");

					m_pThread->join();
					m_pThread.reset();
				}
			}
		private:
			std::shared_ptr<std::thread>	m_pThread;
		};
#endif

		//---------------------------------------------------------------------------------
		/**
		 * @brief	コンストラクタ
		 */
		Thread::Thread() {
			m_pImpl.reset(new cImpl());
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	デストラクタ
		 */
		Thread::~Thread() {
			m_pImpl.reset();
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	スレッドの開始
		 * @param	func		スレッドのワーカー関数
		 * @param	arg			スレッドに渡す情報
		 * @param	stackSize	スレッド側で利用できる最大スタックメモリサイズ
		 * @return	
		 */
		void Thread::Start(WorkerFunc func, void* arg, size_t stackSize) {
			m_pImpl->Start(func, arg, stackSize);
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	スレッドの終了待機
		 * @return	
		 */
		void Thread::Wait() {
			m_pImpl->Wait();
		}
	}
}