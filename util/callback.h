﻿#pragma once

namespace core {

	namespace util {

		//---------------------------------------------------------------------------------
		/**
		 * @brief
		 * コールバック
		 */
		template<typename Result, typename ...Arg>
		class CallBack {
		public:
			using Function = std::function<Result(Arg...)>;

			//---------------------------------------------------------------------------------
			/**
			 * @brief	コンストラクタ
			 */
			CallBack()
			{
				m_function = nullptr;
			}

			//---------------------------------------------------------------------------------
			/**
			 * @brief	コンストラクタ
			 */
			CallBack(const Function& f)
			{
				m_function = f;
			}

			//---------------------------------------------------------------------------------
			/**
			 * @brief	デストラクタ
			 */
			~CallBack() {}

			//---------------------------------------------------------------------------------
			/**
			 * @brief	代入
			 */
			void operator=(const Function& f)
			{
				m_function = f;
			}

			//---------------------------------------------------------------------------------
			/**
			 * @brief	実行
			 */
			Result operator()(Arg ...arg)
			{
				assert(m_function, "不正な関数です");
				return m_function(arg ...);
			}

		private:
			Function m_function;
		};
	}
}
