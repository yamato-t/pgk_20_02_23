﻿#pragma once

#include "../obj/drawObj.h"
#include "../texture/texture.h"

#include "../camera/cameraOrtho.h"

namespace core {

	namespace objScreen {

		//---------------------------------------------------------------------------------
		/**
		 * @brief
		 * スクリーン描画オブジェクトクラス
		 */
		class Screen	: public obj::DrawObj
						, public util::Singleton<Screen>{
		private:
			friend class util::Singleton<Screen>;

		public:
			//---------------------------------------------------------------------------------
			/**
			 * @brief	コンストラクタ
			 */
			Screen();

			//---------------------------------------------------------------------------------
			/**
			 * @brief	デストラクタ
			 */
			virtual ~Screen();

		public:
			//---------------------------------------------------------------------------------
			/**
			 * @brief	描画リセット
			 * @return
			 */
			void Reset();

			//---------------------------------------------------------------------------------
			/**
			 * @brief	スクリーン初期化
			 * @return
			 */
			virtual void Initialize();

			//---------------------------------------------------------------------------------
			/**
			 * @brief	スクリーンの描画
			 * @param	pContext	描画コンテキスト
			 * @param	pMaterial	外部指定のマテリアル
			 * @return
			 */
			virtual void Draw(ID3D11DeviceContext* pContext) {}

			//---------------------------------------------------------------------------------
			/**
			 * @brief	スクリーンの描画(テクスチャ指定)
			 * @param	pContext	描画コンテキスト
			 * @param	pTexture　	描画するテクスチャのポインタ
			 * @param	pRect　		描画矩形（xy：左上 zw：右下）
			 * @return
			 */
			void DrawTexture(ID3D11DeviceContext* pContext, texture::Texture* pTexture, vec4* pRect = nullptr);

			//---------------------------------------------------------------------------------
			/**
			 * @brief	スクリーンの描画(シェーダリソースビュー指定)
			 * @param	pContext	描画コンテキスト
			 * @param	pTexture　	描画するシェーダリソース
			 * @param	pRect　		描画矩形（xy：左上 zw：右下）
			 * @return
			 */
			void DrawShaderResource(ID3D11DeviceContext* pContext, IShaderResourceView* pShaderResouceView, vec4* pRect = nullptr);

		private:
			camera::CameraOrtho		m_cameraOrtho;		///< スクリーン投影(正射影)カメラ

			buffer::VertexBuffer	m_testVertexBuffer[5];		///< 複数の頂点バッファ（テスト）
			int						m_currentVertexBufferIndex;	///< 現在の頂点バッファインデックス

		};
	}
};
