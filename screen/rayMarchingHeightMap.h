﻿#pragma once

#include "../obj/drawObj.h"

namespace core {

	namespace objScreen {

		//---------------------------------------------------------------------------------
		/**
		 * @brief
		 * スクリーン空間レイマーチングハイトマップ描画オブジェクトクラス
		 */
		class RayMarchingHeightMap : public obj::DrawObj {

		public:
			//---------------------------------------------------------------------------------
			/**
			 * @brief	コンストラクタ
			 */
			RayMarchingHeightMap();

			//---------------------------------------------------------------------------------
			/**
			 * @brief	デストラクタ
			 */
			virtual ~RayMarchingHeightMap();

		public:
			//---------------------------------------------------------------------------------
			/**
			 * @brief	レイマーチングハイトマップ初期化
			 * @return
			 */
			virtual void Initialize();

			//---------------------------------------------------------------------------------
			/**
			 * @brief	レイマーチングハイトマップ描画
			 * @param	pContext	描画コンテキスト
			 * @param	pMaterial	外部指定のマテリアル
			 * @return
			 */
			virtual void Draw(ID3D11DeviceContext* pContext);

		private:
			texture::Texture m_heightMap;
		};
	}
};
