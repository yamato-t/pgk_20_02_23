#include "drawObjContainer.h"

namespace core {

	namespace obj {
		//---------------------------------------------------------------------------------
		/**
		 * @brief	コンストラクタ
		 */
		DrawObjContainer::DrawObjContainer() {
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	デストラクタ
		 */
		DrawObjContainer::~DrawObjContainer() {
			for(auto p : m_pDrawObj) {
				p.clear();
			}
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	描画オブジェクトの登録
		 * @param	layer	 描画レイヤー
		 * @param	pDrawObj 描画オブジェクト 
		 * @return	
		 */
		void DrawObjContainer::Register(layer layer, obj::DrawObj* pDrawObj) {
			m_pDrawObj[layer].push_back(pDrawObj);
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	描画オブジェクトの描画順ソート
		 * @param	layer	 描画レイヤー
		 * @return	
		 */
		void DrawObjContainer::Sort(layer layer) {

			// カメラまでの距離を見てクイックソートなどで並び替える
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	描画オブジェクトの取得
		 * @param	layer	 描画レイヤー
		 * @return	レイヤーに登録されている描画オブジェクト
		 */
		std::vector<obj::DrawObj*>& DrawObjContainer::GetDrawObj(layer layer) {
			return m_pDrawObj[layer];
		}

	}
}