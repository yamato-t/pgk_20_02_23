﻿#include "drawObj.h"

namespace core {

	namespace obj {


		//---------------------------------------------------------------------------------
		/**
		 * @brief	コンストラクタ
		 */
		DrawObj::DrawObj()
		: m_pIndexBuffers(nullptr)
		, m_materialNum(0) {
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	コンストラクタ
		 */
		DrawObj::~DrawObj() {

			// マテリアルを削除
			if(m_pMaterials) {
				for(int i = 0; i < m_materialNum; i++) { delete m_pMaterials[i]; }
				delete[] m_pMaterials;
				m_pMaterials = nullptr;
			}

			// インデックスバッファを削除
			if(m_pIndexBuffers) {
				delete[] m_pIndexBuffers;
				m_pIndexBuffers = nullptr;
			}
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	頂点バッファの取得
		 * @return	頂点バッファ
		 */
		buffer::VertexBuffer*	DrawObj::GetVertexBuffer() {
			return &m_vertexBuffer;
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	インデックスバッファの取得
		 * @param	no	インデックスバッファのコンテナ番号
		 * @return	インデックスバッファ
		 */
		buffer::IndexBuffer*	DrawObj::GetIndexBuffer(uint32_t no) {
			return &m_pIndexBuffers[no];
		}
	}
}
