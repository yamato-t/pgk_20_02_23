﻿#include "objBase.h"

namespace core {

	namespace obj {

		//---------------------------------------------------------------------------------
		/**
		 * @brief	コンストラクタ
		 */
		ObjBase::ObjBase()
		: m_update(true)
		, m_position(0,0,0)
		, m_rotation(0,0,0)
		, m_scale(1,1,1) {

		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	デストラクタ
		 */
		ObjBase::~ObjBase() {

		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	座標の設定
		 * @param	pos		座標
		 * @return
		 */
		void ObjBase::Position(const vec3& pos) {
			m_update |= (m_position != pos);
			m_position = pos;
		}


		//---------------------------------------------------------------------------------
		/**
		 * @brief	回転の設定
		 * @param	rot		角度（ラジアン）
		 * @return
		 */
		void ObjBase::Rotation(const vec3& rot) {
			m_update |= (m_rotation != rot);
			m_rotation = rot;
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	拡縮の設定
		 * @param	scale	係数
		 * @return
		 */
		void ObjBase::Scale(const vec3& scale) {
			m_update |= (m_scale != scale);
			m_scale = scale;
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	オブジェクトの更新
		 * @return
		 */
		void ObjBase::Update() {

			// トランスフォーム
			if(m_update) {
				matrix scale;
				math::MatrixScale(&scale, m_scale.x, m_scale.y, m_scale.z);

				matrix rotation;
				math::MatrixRotationZXY(&rotation, m_rotation.z, m_rotation.x, m_rotation.y);

				matrix position;
				math::MatrixTranslation(&position, m_position.x, m_position.y, m_position.z);

				matrix temp;

				math::MatrixMultiply(&temp,			&scale, &rotation);
				math::MatrixMultiply(&m_transform,	&temp,	&position);
			}
		}
	}
}
