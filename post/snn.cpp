﻿#include "snn.h"
#include "../render/postProcRender.h"


#include "../dx11/dx11.h"
#include "../buffer/buffer.h"
#include "../render/obj3dRender.h"
#include "../screen/screen.h"
#include "../texture/texture.h"

namespace core {

	namespace filter {

		using namespace core::dx11;

		//---------------------------------------------------------------------------------
		/**
		 * @brief	初期化
		 * @return
		 */
		void SNN::Initialize() {
			render::PostProcRender::Instance().Register(this);

			auto colorBuffer = render::Obj3DRender::Instance().ColorTarget();

			D3D11_TEXTURE2D_DESC textureDesc;
			colorBuffer.GetTexture()->Get()->GetDesc(&textureDesc);

			{

				// リソース作成
				m_resource.Create(textureDesc.Width, textureDesc.Height,
					DXGI_FORMAT_R8G8B8A8_UNORM, D3D11_BIND_UNORDERED_ACCESS);

				// バッファサイズ
				auto bufferSize = textureDesc.Width * textureDesc.Height;

				// アンオーダードアクセスビュー作成
				D3D11_UNORDERED_ACCESS_VIEW_DESC unorderedAccessDesc;
				memset(&unorderedAccessDesc, 0, sizeof(unorderedAccessDesc));
				unorderedAccessDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
				unorderedAccessDesc.ViewDimension = D3D11_UAV_DIMENSION_TEXTURE2D;
				unorderedAccessDesc.Buffer.FirstElement = 0;
				unorderedAccessDesc.Buffer.NumElements = bufferSize;

				if (FAILED(dx11Device::Instance().Device()->CreateUnorderedAccessView(
					m_resource.Get(),
					&unorderedAccessDesc,
					m_pUAV.GetPointerPointer()))) {
					assert(false, " アンオーダードアクセスビュー作成に失敗しました");
				}
			}

			// コンピュートシェーダ
			{

				// hlslファイル読み込み ブロブ作成
				ComPtr<ID3DBlob> pCompiledShader;
				ComPtr<ID3DBlob> pErrors;

				UINT flag = D3D10_SHADER_ENABLE_STRICTNESS | D3D10_SHADER_OPTIMIZATION_LEVEL0;

				// ブロブからシェーダー作成
				if (SUCCEEDED(D3DX11CompileFromFileW(L"resource/hlsl/computeSNN.hlsl",
					nullptr,
					nullptr,
					"CS_Main",
					"cs_5_0",
					flag,
					0,
					nullptr,
					pCompiledShader.GetPointerPointer(),
					pErrors.GetPointerPointer(),
					nullptr)))
				{
					if (FAILED(dx11Device::Instance().Device()->CreateComputeShader(
						pCompiledShader->GetBufferPointer(),
						pCompiledShader->GetBufferSize(),
						nullptr,
						m_pCS.GetPointerPointer()))) {
						assert(false, "コンピュートシェーダーの作成に失敗しました");
					}
				}
				else {
					char* p = (char*)pErrors->GetBufferPointer();
					assert(false, p);
				}
			}
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	SNN処理
		 * @param	pContext	描画コンテキスト
		 * @param	pMaterial	外部指定のマテリアル
		 * @return
		 */
		void SNN::Draw(ID3D11DeviceContext* pContext) {
			// シェーダ実行
			{
				// コンピュートシェーダ設定
				pContext->CSSetShader(m_pCS.GetPointer(), nullptr, 0);

				// シェーダーリソースビューは現在のカラーバッファを指定する
				auto colorBuffer	= render::Obj3DRender::Instance().ColorTarget();
				auto texture		= colorBuffer.GetTexture();
				D3D11_TEXTURE2D_DESC textureDesc;
				texture->Get()->GetDesc(&textureDesc);

				ID3D11ShaderResourceView* views[] = { texture->GetTextureView() };
				pContext->CSSetShaderResources(0, 1, views);

				// アンオーダードアクセスビューをコンピュートシェーダーに設定
				pContext->CSSetUnorderedAccessViews(0, 1, m_pUAV.GetPointerPointer(), nullptr);

				// コンピュートシェーダーを実行
				pContext->Dispatch(textureDesc.Width / 16, textureDesc.Height / 16, 1);
			}

			// 解除
			{
				pContext->CSSetShader(nullptr, nullptr, 0);

				IUnorderedAccessView* nullUAView[1] = { nullptr };
				pContext->CSSetUnorderedAccessViews(0, 1, nullUAView, nullptr);

				IShaderResourceView* nullSRV[1] = { nullptr };
				pContext->CSSetShaderResources(0, 1, nullSRV);
			}

			auto colorBuffer = render::Obj3DRender::Instance().ColorTarget();
			D3D11_TEXTURE2D_DESC textureDesc;
			colorBuffer.GetTexture()->Get()->GetDesc(&textureDesc);

			// 結果を表示
			vec4 rect(0, 0, textureDesc.Width, textureDesc.Height);
			objScreen::Screen::Instance().DrawShaderResource(pContext, m_resource.GetTextureView(), &rect);

		}
	}
}
