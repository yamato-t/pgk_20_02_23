﻿#pragma once

#include "buffer.h"
#include "../dx11/dx11.h"

namespace core {

	namespace buffer {

		//---------------------------------------------------------------------------------
		/**
		 * @brief	バッファの作成
		 * @param	bindFlags		バインドフラグ
		 * @param	stride			構造化バッファサイズ
		 * @param	count			構造化バッファ数
		 * @param	usage			バッファの利用方法種類の設定
		 * @param	cpuAccessFlag	CPUアクセス可能か否か
		 * @param	miscFlags		その他属性フラグ
		 * @param	pInit			初期データ
		 * @return
		 */
		void IBuffer::Create(uint32_t				bindFlags,
							uint32_t				stride,
							uint32_t				count,
							D3D11_USAGE				usage,
							uint32_t				cpuAccessFlag,
							uint32_t				miscFlags,
							D3D11_SUBRESOURCE_DATA* pInit) {
			// バッファ作成
			D3D11_BUFFER_DESC cb;
			cb.BindFlags			= bindFlags;
			cb.StructureByteStride  = stride;
			cb.ByteWidth			= stride * count;
			cb.Usage				= usage;
			cb.CPUAccessFlags		= cpuAccessFlag;
			cb.MiscFlags			= miscFlags;

			if (FAILED(dx11::dx11Device::Instance().Device()->CreateBuffer(&cb,
				pInit,
				m_pBuffer.GetPointerPointer()))) {
				assert(false, "バッファの作成に失敗しました");
			}
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	バッファの更新
		 * @param	pContext	描画コンテキスト
		 * @param	data　		更新データ
		 * @param	dataSize　	データサイズ
		 * @return
		 */
		void IBuffer::Update(ID3D11DeviceContext* pContext, void* data, uint32_t dataSize) {

			auto buffer = GetBuffer();

			if( buffer ) {
				D3D11_MAPPED_SUBRESOURCE resource;
				if( FAILED(pContext->Map(buffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &resource)) ) {
					assert(false, "バッファの更新に失敗しました");
				}
				memcpy(resource.pData, data, dataSize);
				pContext->Unmap(buffer, 0);
			}
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	バッファの取得
		 * @return	バッファ
		 */
		ID3D11Buffer* IBuffer::GetBuffer() {
			return m_pBuffer.GetPointer();
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	バッファの全体のサイズの取得
		 * @return	バッファ全体のサイズ
		 */
		const uint32_t IBuffer::Size() {
			return m_size;
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	バッファサイズの取得
		 * @return	バッファサイズ
		 */
		const uint32_t IBuffer::Stride() {
			return m_stride;
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	バッファ内の実データまでのオフセット取得
		 * @return	バッファ内の実データまでのオフセット
		 */
		const uint32_t IBuffer::Offset() {
			return m_offset;
		}

	}
}
