﻿#pragma once

#include "../dx11/dx11def.h"
#include "buffer.h"
#include <string>

namespace core {

	namespace buffer {

		//---------------------------------------------------------------------------------
		/**
		 * @brief
		 * 頂点バッファ
		 */
		class VertexBuffer : public IBuffer {

		public:
			//---------------------------------------------------------------------------------
			/**
			 * @brief
			 * 頂点バッファ項目のフォーマット
			 */
			enum format
			{
				pos,		///< 頂点座標
				color,		///< 頂点カラー
				uv,			///< 頂点UV座標
				uvw,		///< 頂点UVW座標
				normal,		///< 頂点法線
				tangent,	///< 頂点接線
				binormal,	///< 頂点従法線
				float4x4,	///< インスタンシング用変換行列
				max
			};

		public:
			//---------------------------------------------------------------------------------
			/**
			 * @brief	コンストラクタ
			 */
			VertexBuffer();

			//---------------------------------------------------------------------------------
			/**
			 * @brief	デストラクタ
			 */
			virtual ~VertexBuffer();

			//---------------------------------------------------------------------------------
			/**
			 * @brief	頂点バッファの作成
			 * @param	formatCount		頂点を構成するデータのフォーマットの数
			 * @param	formats			頂点を構成するデータのフォーマット配列のポインタ
			 * @param	vertexCount		頂点数
			 * @param	vertexBuffers	頂点を構成するデータ配列のポインタ
			 * @return
			 */
			void Create(uint32_t formatCount, uint32_t* formats, uint32_t vertexCount, void* vertexBuffers);

			//---------------------------------------------------------------------------------
			/**
			 * @brief	頂点を構成するデータのフォーマットを取得
			 * @return　フォーマットが入ったコンテナ
			 */
			const std::vector<format>& GetFormat() const;

			//---------------------------------------------------------------------------------
			/**
			 * @brief	フォーマットサイズの取得
			 * @return　フォーマットサイズ
			 */
			static uint32_t GetFormatSize(format f);

		private:
			std::vector<format>	m_formats;		///< 頂点を構成するデータのフォーマットが入ったコンテナ
		};
	}

}
