﻿#include "allocator.h"
#include "allocator.h"
#include "time.h"
#include "thread.h"
#include "framerate.h"
#include "callback.h"

#include "../dx11/dx11.h"
#include "../win/window.h"
#include "../input/input.h"
#include "../render/obj3dRender.h"
#include "../render/postProcRender.h"
#include "../render/obj2dRender.h"

#include "../app/scene/SceneComputeFilter.h"
#include "../app/scene/SceneRayMarchingHeightMap.h"
#include "../app/scene/sceneTexture3d.h"
#include "../app/scene/sceneManager.h"



#include "../app/area/mortonOrder.h"

#if 0

unsigned int __stdcall Worker(void* arg)
{
	int count = *(int*)arg;

	int sum = 0;

	for(int i = 0; i < count; i++)
	{
		sum++;
	}

	trace("%d", sum);

	return 0;
}

// --------------------------------------------------------------
/**
*
*/
int main(void)
{
	{
		TIME_CHECK_SCORP("main");

		Thread counter;
		int count = 20000;

		counter.Start(Worker, &count);

		{
			{
				TIME_CHECK_SCORP("allocate test");

				int* p = new int[100];

				uint64_t* x = new uint64_t;

				delete[] p;
				delete x;

				uint64_t* y = new uint64_t;
				delete y;
			}

			TIME_PRINT("allocate test");
		}

		counter.Wait();
	}

	TIME_PRINT("main");

	return 0;
}
#endif

//---------------------------------------------------------------------------------
/**
 * @brief	エントリー関数
 */
INT WINAPI WinMain(HINSTANCE hInstance, HINSTANCE, LPSTR, INT) {
	{
		using namespace core::util;
		using namespace core::windows;
		using namespace core::input;
		using namespace core::dx11;
		using namespace core::render;
		using namespace core::obj;
		using namespace app::scene;

		// メモリリークチェック
		_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
		{

			FrameRate::Build();
			FrameRate::Instance().SetFPS(60);

			Window::Build();
			Window::Instance().Create(hInstance);
			// ウィンドウ生成イベントシグナル設定を待つ
			Window::Instance().Wait();

			dx11Device::Build();
			dx11Device::Instance().Initialize();

			Input::Build();

			RenderManager::Build();
			RenderManager::Instance().StartWorker();

			Obj3DRender::Build();
			Obj3DRender::Instance().Initialize();

			PostProcRender::Build();
			PostProcRender::Instance().Initialize();

			Obj2DRender::Build();
			Obj2DRender::Instance().Initialize();

			DrawObjContainer::Build();

			SceneManager::Build();
			SceneManager::Instance().ChangeNewScene<SceneComputeFilter>();


			// アプリケーションループ
			while (!Window::Instance().IsEnd()) {

				// フレーム初期設定
				{
					Obj3DRender::Instance().Reset();
					Obj2DRender::Instance().Reset();
				}

				// 更新
				{
					SceneManager::Instance().Update();
					Obj3DRender::Instance().Update();
					Obj2DRender::Instance().Update();
				}

				// 描画
				{
					RenderManager::Instance().Begin();

					Obj3DRender::Instance().Draw();
					Obj2DRender::Instance().Draw();
					PostProcRender::Instance().Draw();

					RenderManager::Instance().End();
				}

				FrameRate::Instance().WaitFrame();
			}

			// ここでコマンド処理とGPU処理を待たなければならない
			RenderManager::Instance().WaitFinish();

			SceneManager::Release();

			DrawObjContainer::Release();

			PostProcRender::Release();
			Obj2DRender::Release();
			Obj3DRender::Release();
			RenderManager::Release();

			dx11Device::Release();
			Input::Release();

			Window::Release();

			FrameRate::Release();
		}
	}

	return 0;
}
