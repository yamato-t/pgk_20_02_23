﻿#include "shader.h"

#include <stdlib.h>
#include <locale.h>

namespace core {

	namespace shader {

		using namespace dx11;

		//---------------------------------------------------------------------------------
		/**
		 * @brief
		 * シェーダ制御インプリメントクラス
		 */
		class Shader::cImpl {

		public:
			ComPtr<ID3D11VertexShader>		m_pVertexShader;	///< 頂点シェーダ制御
			ComPtr<ID3D11PixelShader>		m_pPixelShader;		///< ピクセルシェーダ制御
			ComPtr<ID3D11InputLayout>		m_pVertexLayout;	///< 頂点レイアウト

		public:
			//---------------------------------------------------------------------------------
			/**
			 * @brief	コンストラクタ
			 */
			cImpl() {

			}

			//---------------------------------------------------------------------------------
			/**
			 * @brief	デストラクタ
			 */
			~cImpl() {
			}

			//---------------------------------------------------------------------------------
			/**
			 * @brief	シェーダの生成
			 * @param	strFilePath		シェーダファイルのパス
			 * @param	pLayout			頂点レイアウト
			 * @param	elementNum		頂点レイアウトの項目数
			 * @return
			 */
			void Create(const std::string& strFilePath, D3D11_INPUT_ELEMENT_DESC* pLayout, uint32_t elementNum) {
				// hlslファイル読み込み ブロブ作成
				ComPtr<ID3DBlob> pCompiledShader;
				ComPtr<ID3DBlob> pErrors;

				// ブロブからシェーダー作成
				if (SUCCEEDED(D3DX11CompileFromFile(strFilePath.c_str(),
					nullptr,
					nullptr,
					"VS",
					"vs_5_0",
					0,
					0,
					nullptr,
					pCompiledShader.GetPointerPointer(),
					pErrors.GetPointerPointer(),
					nullptr)))
				{

					if (FAILED(dx11Device::Instance().Device()->CreateVertexShader(pCompiledShader->GetBufferPointer(),
						pCompiledShader->GetBufferSize(),
						nullptr,
						m_pVertexShader.GetPointerPointer()))) {
						assert(false, "頂点シェーダの作成に失敗しました");
					}

					//頂点インプットレイアウトを作成
					if (FAILED(dx11Device::Instance().Device()->CreateInputLayout(pLayout,
						elementNum,
						pCompiledShader->GetBufferPointer(),
						pCompiledShader->GetBufferSize(),
						m_pVertexLayout.GetPointerPointer()))) {
						assert(false, "頂点レイアウトの作成に失敗しました");
					}
				}
				else {
					char* p = (char*)pErrors->GetBufferPointer();
					assert(false, p);
				}

				//ブロブからピクセルシェーダー作成
				if (SUCCEEDED(D3DX11CompileFromFile(strFilePath.c_str(),
					nullptr,
					nullptr,
					"PS",
					"ps_5_0",
					0,
					0,
					nullptr,
					pCompiledShader.GetPointerPointer(),
					pErrors.GetPointerPointer(),
					nullptr)))
				{

					if (FAILED(dx11Device::Instance().Device()->CreatePixelShader(pCompiledShader->GetBufferPointer(),
						pCompiledShader->GetBufferSize(),
						nullptr,
						m_pPixelShader.GetPointerPointer()))) {
						assert(false, "ピクセルシェーダーの作成に失敗しました");
					}
				} else {
					char* p = (char*)pErrors->GetBufferPointer();
				}
			}
		};

		//---------------------------------------------------------------------------------
		/**
		 * @brief	コンストラクタ
		 */
		Shader::Shader() {
			m_pImpl.reset(new Shader::cImpl());
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	デストラクタ
		 */
		Shader::~Shader() {
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	シェーダの生成
		 * @param	path			シェーダファイルのパス
		 * @param	formats	頂点バッファフォーマット
		 * @return
		 */
		void Shader::Create(const std::string& path, const std::vector<buffer::VertexBuffer::format>& formats) {

			//頂点インプットレイアウトを定義
			D3D11_INPUT_ELEMENT_DESC* pLayout = new D3D11_INPUT_ELEMENT_DESC[formats.size()];
			uint32_t currentOffset = 0;
			for(uint32_t i = 0; i < formats.size(); i++) {

				D3D11_INPUT_ELEMENT_DESC l;

				if(formats[i] == buffer::VertexBuffer::format::pos) {
					l.SemanticName			= "IN_POSITION";
					l.Format				= DXGI_FORMAT_R32G32B32_FLOAT;
				} else if(formats[i] == buffer::VertexBuffer::format::color) {
					l.SemanticName			= "IN_COLOR";
					l.Format				= DXGI_FORMAT_R32G32B32A32_FLOAT;
				} else if(formats[i] == buffer::VertexBuffer::format::uv) {
					l.SemanticName			= "IN_UV";
					l.Format				= DXGI_FORMAT_R32G32_FLOAT;
				} else if (formats[i] == buffer::VertexBuffer::format::uvw) {
					l.SemanticName			= "IN_UVW";
					l.Format				= DXGI_FORMAT_R32G32B32_FLOAT;
				} else if(formats[i] == buffer::VertexBuffer::format::normal) {
					l.SemanticName			= "IN_NORMAL";
					l.Format				= DXGI_FORMAT_R32G32B32_FLOAT;
				}

				l.SemanticIndex			= 0;
				l.InputSlot				= 0;
				l.AlignedByteOffset		= currentOffset;
				l.InputSlotClass		= D3D11_INPUT_PER_VERTEX_DATA;
				l.InstanceDataStepRate	= 0;

				pLayout[i] = l;

				currentOffset += buffer::VertexBuffer::GetFormatSize(formats[i]);
			}

			m_pImpl->Create(path, pLayout, (uint32_t)formats.size());

			delete pLayout;
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	シェーダの生成
		 * @param	path			シェーダファイルのパス
		 * @param	formats			頂点バッファフォーマット
		 * @param	instanceFormats	インスタンシング用追加頂点バッファのフォーマット
		 * @return
		 */
		void Shader::Create(const std::string& path, const std::vector<buffer::VertexBuffer::format>& formats, const std::vector<buffer::VertexBuffer::format>& instanceFormats) {

			//頂点インプットレイアウトを定義
			D3D11_INPUT_ELEMENT_DESC* pLayout = new D3D11_INPUT_ELEMENT_DESC[formats.size() + 4/*instanceFormats.size()*/];
			uint32_t currentOffset = 0;

			for(uint32_t i = 0; i < formats.size(); i++) {
				D3D11_INPUT_ELEMENT_DESC l;

				if(formats[i] == buffer::VertexBuffer::format::pos) {
					l.SemanticName			= "IN_POSITION";
					l.Format				= DXGI_FORMAT_R32G32B32_FLOAT;
				} else if(formats[i] == buffer::VertexBuffer::format::color) {
					l.SemanticName			= "IN_COLOR";
					l.Format				= DXGI_FORMAT_R32G32B32A32_FLOAT;
				} else if(formats[i] == buffer::VertexBuffer::format::uv) {
					l.SemanticName			= "IN_UV";
					l.Format				= DXGI_FORMAT_R32G32_FLOAT;
				} else if (formats[i] == buffer::VertexBuffer::format::uvw) {
					l.SemanticName			= "IN_UVW";
					l.Format				= DXGI_FORMAT_R32G32B32_FLOAT;
				} else if(formats[i] == buffer::VertexBuffer::format::normal) {
					l.SemanticName			= "IN_NORMAL";
					l.Format				= DXGI_FORMAT_R32G32B32_FLOAT;
				}

				l.SemanticIndex			= 0;
				l.InputSlot				= 0;
				l.AlignedByteOffset		= currentOffset;
				l.InputSlotClass		= D3D11_INPUT_PER_VERTEX_DATA;
				l.InstanceDataStepRate	= 0;

				pLayout[i] = l;

				currentOffset += buffer::VertexBuffer::GetFormatSize(formats[i]);
			}


			// インスタンス用追加レイアウトを定義 DXGI_FORMAT_R32G32B32A32_FLOAT x 4の行列を入力する
			currentOffset = 0;
			const uint32_t rowNum = 4;
			for(uint32_t i = 0; i < rowNum; i++) {
				D3D11_INPUT_ELEMENT_DESC l;
				l.SemanticName			= "MATRIX";
				l.SemanticIndex			= i;
				l.Format				= DXGI_FORMAT_R32G32B32A32_FLOAT;
				l.InputSlot				= 1;
				l.AlignedByteOffset		= currentOffset;
				l.InputSlotClass		= D3D11_INPUT_PER_INSTANCE_DATA;
				l.InstanceDataStepRate	= 1;
				pLayout[i + formats.size()] = l;

				currentOffset += (buffer::VertexBuffer::GetFormatSize(buffer::VertexBuffer::format::float4x4) / rowNum);
			}

			m_pImpl->Create(path, pLayout, (uint32_t)(formats.size() + rowNum));

			delete pLayout;
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	頂点シェーダ取得
		 * @return	頂点シェーダ制御のポインタ
		 */
		ID3D11VertexShader*	Shader::VertexShader() {
			return m_pImpl->m_pVertexShader.GetPointer();
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	ピクセルシェーダ取得
		 * @return	ピクセルシェーダ制御のポインタ
		 */
		ID3D11PixelShader*	Shader::PixelShader() {
			return m_pImpl->m_pPixelShader.GetPointer();
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	頂点レイアウト取得
		 * @return	設定された頂点バッファを元に作成した頂点レイアウト
		 */
		ID3D11InputLayout*	Shader::VertexLayout() {
			return m_pImpl->m_pVertexLayout.GetPointer();
		}
	};

};
