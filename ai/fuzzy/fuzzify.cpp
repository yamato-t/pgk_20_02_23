#include "fuzzify.h"


// ---------------------------------------------------------------------
/*
*	値の制限
*/
template<typename T> T clamp(T value, T min, T max) {
	return (value < min ? min : max < value ? max : value);
}

// ---------------------------------------------------------------------
/*
*	メンバーシップ　＿／￣ or ￣＼＿　斜辺関数　
*/
float Hypotenuse::Fuzzy() {
	float result = 0;

	auto value = m_crispSet.Value();

	if (m_crispSet[1] - m_crispSet[0] < 0) {
		result = 1.0f - (value - m_crispSet[1]) / (m_crispSet[0] - m_crispSet[1]);
	} else {
		result = (value - m_crispSet[0]) / (m_crispSet[1] - m_crispSet[0]);
	}

	return clamp(result, 0.0f, 1.0f);
}

// ---------------------------------------------------------------------
/*
*	メンバーシップ　＿／＼＿　トライアングル関数
*/
float Tri::Fuzzy() {
	float result = 0;

	auto value = m_crispSet.Value();

	if (m_crispSet[0] < value && value <= m_crispSet[1]) { 
		// 上昇
		result = (value - m_crispSet[0]) / (m_crispSet[1] - m_crispSet[0]);
	} else if (m_crispSet[1] < value && value <= m_crispSet[2]) {
		// 下降
		result = 1.0f - ((value - m_crispSet[1]) / (m_crispSet[2] - m_crispSet[1]));
	}

	return clamp(result, 0.0f, 1.0f);
}

// ---------------------------------------------------------------------
/*
*	メンバーシップ　＿／￣＼＿　台形関数
*/
float Trapezoid::Fuzzy() {
	float result = 0;

	auto value = m_crispSet.Value();

	if (m_crispSet[0] < value && value <= m_crispSet[1]) {
		// 上昇
		result = (value - m_crispSet[0]) / (m_crispSet[1] - m_crispSet[0]);
	} else if (m_crispSet[2] < value && value <= m_crispSet[3]) {
		// 下降
		result = 1.0f - ((value - m_crispSet[2]) / (m_crispSet[3] - m_crispSet[2]));
	} else if (m_crispSet[1] < value && value <= m_crispSet[2]) {
		// 上限
		result = 1.0f;
	}

	return clamp(result, 0.0f, 1.0f);
}


// ---------------------------------------------------------------------
/*
*	スレッドワーカー
*/
static unsigned int ThreadWorker(void* Param) {
	auto p = (Fuzzify::FuzzyParam*)Param;

	p->pInstance->Worker(p->pInputFuzzySet, p->membershipSet);

	delete p;

	return 0;
}

// ---------------------------------------------------------------------
/*
*	入力ファジー集合を作成
*/
void Fuzzify::CreateSet(FuzzySet* pInputFuzzySet, const MembershipSet& membershipSet) {
	auto param = new FuzzyParam;

	param->pInstance		= this;
	param->pInputFuzzySet	= pInputFuzzySet;
	param->membershipSet	= membershipSet;

	m_thread.Start(ThreadWorker, param);
}

// ------------------------------------------------------------------
/*
*	スレッドワーカー
*/
void Fuzzify::Worker(FuzzySet* pInputFuzzySet, const MembershipSet& membershipSet) {
	assert(pInputFuzzySet->size() == membershipSet.size(), "不正な設定になっています");

	int index = 0;
	for (auto membership : membershipSet)
	{
		(*pInputFuzzySet)[index++].Value(membership->Fuzzy());
	}
}

