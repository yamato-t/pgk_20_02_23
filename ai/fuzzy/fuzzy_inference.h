#pragma once

#include "fuzzify.h"
#include "thread.h"


// 入力ファジー集合を利用して、特定の推論を元に出力ファジー集合を作成する
// 入力ファジー集合は一つではなく、複数の入力ファジー集合を利用する場合が多い（気温に関する入力ファジー集合＆人口密度に関するファジー集合等）

// 出力ファジー集合は基本的に一つと考える方がわかりやすい（上記の例から、エアコンの操作に関する出力ファジー集合が導き出される等）

// 特定の形はなく、製作者の意図によって推論内容が変化する

// ---------------------------------------------------------------------
/*
*	ファジー推論定義
*/
enum INFERENCE {
	AIR_CONTROL,	// エアコン操作の為のファジー推論
	CHANGE_ROOM,	// 部屋を変えるべきか否かの為のファジー推論
	MAX,
};


// ---------------------------------------------------------------------
/*
*	ファジー推論
*/
class FuzzyInference {
public:
	struct InferenceParam {
		FuzzyInference* pInstance;
		FuzzySet*		pOutputFuzzySet;
		FuzzySets		inputfuzzySets;
		INFERENCE		rule;
	};

public:
	// ---------------------------------------------------------------------
	/*
	*	コンストラクタ
	*/
	FuzzyInference(FuzzySet* pOutputFuzzySet, const FuzzySets& inputfuzzySets, INFERENCE rule);

	// ---------------------------------------------------------------------
	/*
	*	デストラクタ
	*/
	~FuzzyInference();

	// ---------------------------------------------------------------------
	/**
	*	スレッドワーカー
	*/
	void Worker(FuzzySet* pOutputFuzzySet, const FuzzySets& inputfuzzySets, INFERENCE rule);

private:
	// ---------------------------------------------------------------------
	/**
	* 関数テーブル設定用メタ関数
	*/
	template<int> void SetFunction();

	// ---------------------------------------------------------------------
	/*
	*	入力ファジー集合を利用したファジー推論
	*/
	template<INFERENCE> void Inference(FuzzySet* pOutputFuzzySet, const FuzzySets& inputfuzzySets);

private:
	// ---------------------------------------------------------------------
	/**
	* 関数テーブル
	*/
	void (FuzzyInference::*m_pFunc[INFERENCE::MAX])(FuzzySet* pOutputFuzzySet, const FuzzySets& inputfuzzySets);

	// スレッド制御
	core::util::Thread	m_thread;
};