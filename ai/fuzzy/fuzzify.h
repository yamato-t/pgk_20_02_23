#pragma once

// 入力した閾値の集合（クリスプ集合）をメンバーシップ関数によってファジー化（曖昧な表現を指す内容）する

#include "thread.h"

// ---------------------------------------------------------------------
/*
*	入力クリスプ集合
*/
template <class T, int num> class CrispSet {

public:
	// ------------------------------------------------------------------
	/*
	*	コンストラクタ
	*/
	CrispSet() {
		m_value = 0;
	}

	// ------------------------------------------------------------------
	/*
	*	デストラクタ
	*/
	~CrispSet()
	{
	}

	// ------------------------------------------------------------------
	/*
	*	アクセサ
	*/
	T& operator[](int index)			{ assert((index < num), "不正なインデックスです");  return m_crisps[index]; }

	void		Value(const T& value)	{ m_value = value; }
	T&			Value()					{ return m_value; }

private:
	T			m_crisps[num];	// クリスプ集合
	T			m_value;		// 入力値
};


// ---------------------------------------------------------------------
/*
*	ファジー
*/
class Fuzzy {
public:
	Fuzzy() {}
	Fuzzy(float value) { m_value = value; }
	~Fuzzy(){}

	// ------------------------------------------------------------------
	/*
	*	値の設定　取得
	*/
	const float Value()	const			{ return m_value; };
	void		Value(const float value){ m_value = value; };


private:
	float			m_value;	// ファジー化された値

};

// 単体ファジー集合
typedef std::vector<Fuzzy>		FuzzySet;

// ファジー集合群
typedef std::vector<FuzzySet>	FuzzySets;


// ---------------------------------------------------------------------
/*
*	メンバーシップベース
*/
class MembershipFunctionBase
{
public:
	MembershipFunctionBase() {}
	virtual ~MembershipFunctionBase() {}

public:
	// ------------------------------------------------------------------
	/*
	*	ファジー化変換
	*/
	virtual float Fuzzy() = 0;
};

// ---------------------------------------------------------------------
/*
*	メンバーシップ
*/
template <class T, int ThresholdNum> 
class MembershipFunction : public MembershipFunctionBase
{
public:
	// ------------------------------------------------------------------
	/*
	*	コンストラクタ
	*/
	MembershipFunction() {}

	// ------------------------------------------------------------------
	/*
	*	デストラクタ
	*/
	virtual ~MembershipFunction() {}

	// ------------------------------------------------------------------
	/*
	*	クリスプ集合の取得
	*/
	CrispSet<T, ThresholdNum>& GetCrispSet() { return m_crispSet;  }

protected:
	CrispSet<T, ThresholdNum> m_crispSet;	// クリスプ集合
};

// メンバーシップの集合
typedef std::vector<MembershipFunctionBase*> MembershipSet;

// メンバーシップ集合群
typedef std::vector<MembershipSet>			MembershipSets;

// 可変長テンプレートのほうがいいかもしれない

// ---------------------------------------------------------------------
/*
*	メンバーシップ　斜辺関数
*/
class Hypotenuse : public MembershipFunction<float, 2>
{
public:
	Hypotenuse() {}
	virtual ~Hypotenuse() {}

	// ------------------------------------------------------------------
	/*
	*	ファジー化変換
	*/
	virtual float Fuzzy();
};

// ---------------------------------------------------------------------
/*
*	メンバーシップ　トライアングル関数
*/
class Tri : public MembershipFunction<float, 3>
{
public:
	Tri() {}
	virtual ~Tri() {}

	// ------------------------------------------------------------------
	/*
	*	ファジー化変換
	*/
	virtual float Fuzzy();
};

// ---------------------------------------------------------------------
/*
*	メンバーシップ　台形関数
*/
class Trapezoid : public MembershipFunction<float, 4>
{
public:
	Trapezoid() {}
	virtual ~Trapezoid() {}

	// ------------------------------------------------------------------
	/*
	*	ファジー化変換
	*/
	virtual float Fuzzy();
};




// ---------------------------------------------------------------------
/*
*	ファジー化
*/
class Fuzzify
{
public:
	struct FuzzyParam
	{
		Fuzzify*			pInstance;
		FuzzySet*			pInputFuzzySet;
		MembershipSet		membershipSet;
	};

public:
	// ---------------------------------------------------------------------
	/*
	*	コンストラクタ
	*/
	Fuzzify()
	{
	}

	// ---------------------------------------------------------------------
	/*
	*	デストラクタ
	*/
	~Fuzzify()
	{
		m_thread.Wait();
	}

public:
	// ------------------------------------------------------------------
	/*
	*	入力ファジー集合を作成
	*/
	void CreateSet(FuzzySet* pInputFuzzySet, const MembershipSet& membershipSet);

	// ------------------------------------------------------------------
	/*
	*	スレッドワーカー
	*/
	void Worker(FuzzySet* pInputFuzzySet, const MembershipSet& membershipSet);


private:
	core::util::Thread m_thread;
};
