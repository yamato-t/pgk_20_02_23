#include "fuzzy_inference.h"

// ---------------------------------------------------------------------
/*
*	ファジールール（論理演算）
*/
float And(float fuzzy1, float fuzzy2) {
	// fuzzy1 であり fuzzy2
	return fmin(fuzzy1, fuzzy2);
}
float Or(float fuzzy1, float fuzzy2) {
	// fuzzy1 あるいは fuzzy2
	return fmax(fuzzy1, fuzzy2);
}
float Not(float fuzzy1) {
	// fuzzy1 でない
	return (1.0f - fuzzy1);
}



// ---------------------------------------------------------------------
/*
*	スレッドワーカー
*/
static unsigned int ThreadWorker(void* Param) {
	auto p = (FuzzyInference::InferenceParam*)Param;

	p->pInstance->Worker(p->pOutputFuzzySet, p->inputfuzzySets, p->rule);

	delete p;

	return 0;
}

// ---------------------------------------------------------------------
/*
*	コンストラクタ
*/
FuzzyInference::FuzzyInference(FuzzySet* pOutputFuzzySet, const FuzzySets& inputfuzzySets, INFERENCE rule) {
	InferenceParam* param	= new InferenceParam;

	param->pInstance		= this;
	param->pOutputFuzzySet	= pOutputFuzzySet;
	param->inputfuzzySets	= inputfuzzySets;
	param->rule				= rule;

	//即開始
	m_thread.Start(&ThreadWorker, param);
}

// ---------------------------------------------------------------------
/*
*	デストラクタ
*/
FuzzyInference::~FuzzyInference() {
	m_thread.Wait();
}

// ---------------------------------------------------------------------
/**
*	スレッドワーカー
*/
void FuzzyInference::Worker(FuzzySet* pOutputFuzzySet, const FuzzySets& inputfuzzySets, INFERENCE rule) {
	// 関数設定
	SetFunction<INFERENCE::MAX>();

	// 推論処理開始
	(this->*m_pFunc[rule])(pOutputFuzzySet, inputfuzzySets);
}

// ---------------------------------------------------------------------
/*
*	テンプレート関数設定
*/
template<> void FuzzyInference::SetFunction<0>() {
	return;
}
template<int Count> void FuzzyInference::SetFunction() {
	const int iEnum = (Count - 1);
	m_pFunc[iEnum] = &FuzzyInference::Inference<static_cast<INFERENCE>(iEnum)>;

	SetFunction<iEnum>();
}


// ---------------------------------------------------------------------
/*
*	エアコン操作のファジー推論
*/
template<> void FuzzyInference::Inference<INFERENCE::AIR_CONTROL>(FuzzySet* pOutputFuzzySet, const FuzzySets& inputFuzzySets) {
	auto inputFuzzySet = inputFuzzySets[0];

	// 丁度良い
	float best = inputFuzzySet[1].Value();

	// 丁度良くない
	float notBest = Not(inputFuzzySet[1].Value());

	// 寒い
	float cold = inputFuzzySet[0].Value();

	// 涼しい
	float bestCold = And(best ,inputFuzzySet[0].Value());

	// 暑い
	float hot = inputFuzzySet[2].Value();

	// 暖かい
	float bestHot = And(best, inputFuzzySet[2].Value());

	// 丁度良くなく、寒い　→　暖かくしたい
	float wantHot = And(notBest, cold);

	// 丁度良くなく、暑い　→　涼しくしたい
	float wantCold = And(notBest, hot);

	// 暖かくなく（寒い度合が高い）、暖かくしたい訳でもない（寒いと言い切れない）→　暖かくする事を進める
	float recommendHot = And(Not(bestHot), Not(wantHot));

	// 涼しくなく（暑い度合が高い）、涼しくしたい訳ではない（暑いと言い切れない）→　涼しくする事を進める
	float recommendCold = And(Not(bestCold), Not(wantCold));

	auto outputFuzzySet = *pOutputFuzzySet;

	outputFuzzySet.push_back(wantHot);	
	outputFuzzySet.push_back(wantCold);
	outputFuzzySet.push_back(best);
	outputFuzzySet.push_back(recommendHot);
	outputFuzzySet.push_back(recommendCold);
}

// ---------------------------------------------------------------------
/*
*	人口のファジー推論
*/
template<> void FuzzyInference::Inference<INFERENCE::CHANGE_ROOM>(FuzzySet* pOutputFuzzySet, const FuzzySets& inputFuzzySets) {
}





